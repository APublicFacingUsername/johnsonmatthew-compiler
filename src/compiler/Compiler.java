/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package compiler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import parser.Parser;
import parser.SymbolTable;
import parser.syntaxtree.ProgramNode;
import scanner.Scanner;

/**
 * This is the main class of the compiler project. It's function is to take in a
 * series of command line arguments and to translate those commands into a set
 * of instructions. This process includes both the parsing of the filepath for
 * the file to be compiled as well as the parsing of the arguments provided
 * along side the filepath. The filepath is stored as a string, the arguments
 * are flagged as either true or false along the way. The main then calls a
 * helper function to instantiate the other classes of the project and to pass
 * the appropriate arguments to them.
 *
 * @author Matt
 */
public class Compiler {

    /**
     * boolean flags to let us know what to do
     */
    private static boolean produceTree = false;
    private static boolean produceSymbolTable = false;
    private static boolean produceAssembly = false;

    /**
     * Main method that breaks down the command line arguments and flags certain
     * class booleans accordingly, letting us know later what we should do with
     * the input file as well as the outputs from our compiler.
     *
     * @param args Array of strings representing the command line arguments.
     */
    public static void main(String args[]) {
        for (String arg : args) {
            switch (arg) {
                case "-h":
                    helpMenu();
                    break;
                case "/?":
                    helpMenu();
                    break;
                case "-tr":
                    produceTree = true;
                    break;
                case "-ta":
                    produceSymbolTable = true;
                    break;
                case "-asm":
                    produceAssembly = true;
                default:
                    break;
            }
        }
        if (args.length > 0) {
            setupAndRun(args[args.length - 1]);
        } else if (args.length == 0){
            helpMenu();
            System.out.println("no options provided, specify filename including path.");
        }
    }

    /**
     * Helper method that actually instantiates classes from our compiler and
     * calls the methods of our compiler.
     *
     * @param filepath
     */
    private static void setupAndRun(String filepath) {

        //Split up the filepath based on its delimiters. Intent is to get file name.
        String fileName = filepath;
        if (filepath.contains("\\")) {
            //Holy mother of all escape characters, Batman!
            String[] pathSegments = filepath.split("\\\\");
            fileName = pathSegments[pathSegments.length - 1];
        } else if (filepath.contains("/")) {
            String[] pathSegments = filepath.split("/");
            fileName = pathSegments[pathSegments.length - 1];
        }
        if (fileName.endsWith(".pas")) {
            fileName = fileName.split("\\.")[0];
        }

        //Create a folder for outputs if it doesn't exist in the current classpath.
        try {
            if (!Files.exists(Paths.get("Outputs"))) {
                Files.createDirectory(Paths.get("Outputs"));
            }
        } catch (java.io.IOException ex) {
            System.out.println("Error in creating output directory");
        }

        //Run program and create outputs
        try {
            BufferedReader inputReader = new BufferedReader(new FileReader(filepath));
            Scanner inputScanner = new Scanner(inputReader);
            Parser instance = new Parser(inputScanner);
            ProgramNode outputSyntaxTree = instance.program();
            SymbolTable outputSymbolTable = instance.getSymbolTable();
            semantics.SemanticAnalyzer.createAssignments(outputSyntaxTree, outputSymbolTable);
            semantics.SemanticAnalyzer.consistentAssignments(outputSyntaxTree, outputSymbolTable);
            //If specified by user, make a symbol table file and write the symbol table data to it.
            if (produceSymbolTable) {
                try {
                    if (!Files.exists(Paths.get("Outputs/" + fileName + ".table"))) {
                        Files.createFile(Paths.get("Outputs/" + fileName + ".table"));
                    }
                    System.out.println("Writing symbol table to: Outputs/" + fileName + ".table");
                    Files.write(Paths.get("Outputs/" + fileName + ".table"), outputSymbolTable.indentedToString().getBytes());
                } catch (java.io.IOException ex) {
                    System.out.println("Error writing to file Outputs\\" + fileName + ".table");
                }
            }
            //If specified by the user, make a syntax tree file and write the symbol table data to it.
            if (produceTree) {
                try {
                    if (!Files.exists(Paths.get("Outputs/" + fileName + ".tree"))) {
                        Files.createFile(Paths.get("Outputs/" + fileName + ".tree"));
                    }
                    System.out.println("Writing syntax tree to: Outputs/" + fileName + ".tree");
                    Files.write(Paths.get("Outputs/" + fileName + ".tree"), outputSyntaxTree.indentedToString(0, outputSymbolTable).getBytes());
                } catch (java.io.IOException ex) {
                    System.out.println("Error writing to file Outputs\\" + fileName + ".tree");
                }
            }
            if (produceAssembly) {
                try {
                    codegen.CodeGenerator codegenerator = new codegen.CodeGenerator();
                    codegenerator.generateAsm(outputSyntaxTree, outputSymbolTable);
                    if (!Files.exists(Paths.get("Outputs/" + fileName + ".asm"))) {
                        Files.createFile(Paths.get("Outputs/" + fileName + ".asm"));
                    }
                    System.out.println("Writing assembly to: Outputs/" + fileName + ".asm");
                    Files.write(Paths.get("Outputs/" + fileName + ".asm"), codegenerator.getAsm().getBytes());
                } catch (java.io.IOException ex) {
                    System.out.println("Error writing to file Outputs\\" + fileName + ".asm");
                }
            }
        } catch (scanner.InvalidTokenException ex) {
            System.out.println(ex.toString());
        } catch (parser.UnexpectedTokenException ex) {
            System.out.println(ex.toString());
        } catch (semantics.InvalidAssignmentException ex) {
            System.out.println(ex.toString());
        } catch (java.io.FileNotFoundException ex) {
            System.out.println("Incorrect usage, file '" + filepath + "' not found.");
        }
    }

    /**
     * A helper method that outputs details of how command line calls to this
     * program should be structured.
     */
    private static void helpMenu() {
        System.out.println("The correct usage is: 'java -jar JohnsonMatthew-Compiler.jar <options> <filepath>'\n"
                + "Options should not be concatenated.\n"
                + "The options are:\n -h or /?\thelp information, details of options and correct argument order\n"
                + " -ta\t\tproduce a copy of the symbol table as output\n"
                + " -tr\t\tproduce a copy of the syntax tree as output\n"
                + " -asm\t\tproduce a copy of MIPs assmebly as output\n");
    }
}
