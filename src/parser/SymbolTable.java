/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import parser.syntaxtree.*;

/**
 * A class that allows us to track satellite data that goes with a given Lexeme.
 * The structure of the symbol table includes multiple scopes. The global scope
 * of the symbol table is the scope in which our "program" node exists. The
 * local scope of the symbol table is which ever scope the programmer has set it
 * to. It is initialized to the global scope. Scopes stack on top of the global
 * scope. Multiple tables may be stacked on top of any given table, inclusive of
 * the global scope. When a symbol is searched for in the global scope, the
 * symbol table will first look in the local scope, then recursively search down
 * through the stack of symbol tables. This global search will not touch tables
 * that exist adjacent to the current local scope or any adjacent to its
 * parents, IE if program has children scopes function1 and function2 the global
 * search called when function1 is in focus will not search through function2.
 *
 * @author Matt
 */
public class SymbolTable {

    private final tableScope scopeMain;
    private tableScope scopeLocal;
    HashMap<String, tableScope> symbolTables;
    private boolean isLocked;

    /**
     * Constructor that takes the name of the global scope and creates a symbol
     * table (which exists as a set of tables, one for each scope).
     *
     * @param programName
     */
    public SymbolTable(String programName) {
        symbolTables = new HashMap<String, tableScope>();
        scopeMain = new tableScope(programName, programName);
        scopeLocal = scopeMain;
        symbolTables.put(programName, scopeMain);
        this.isLocked = false;
    }

    /**
     * This is a method that locks out the programmer from adding anything new.
     * This method is present in order to preserve the nature of the symbol
     * table after it has been created by the parser. The table shouldn't need
     * to be edited after parsing. In the case that it does, the programmer must
     * do so inside the parser package and refrain from using this method.
     *
     * @return True if the table wasn't locked and now is, false if it was
     * already locked.
     */
    public boolean lockTables() {
        if (!isLocked) {
            isLocked = true;
            return true;
        }
        return false;
    }

    /**
     * Method that allows us to add a new HashMap to our internal stack
     * structure. This is done to represent the changing of scope in the running
     * MiniPascal program.
     *
     * @param scopeName
     * @return true if the set was completed, false if it failed.
     */
    public boolean setTableScope(String scopeName) {
        if (symbolTables.containsKey(scopeName)) {
            scopeLocal = symbolTables.get(scopeName);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the table scope that is presently assigned. This scope is the
     * function, program, or process in which the table is currently focused.
     *
     * @return the current scope of the table as string
     */
    public String getTableScope() {
        return this.scopeLocal.getName();
    }

    /**
     * Get the parent scope of the scope that is presently assigned. This is the
     * function in which the current function resides. If it is the top level
     * program, returns the program name.
     *
     * @return name of the parent scope.
     */
    public String getParentScope() {
        return this.scopeLocal.getParent();
    }

    /**
     * Get all of the scopes that are children to the scope that is presently
     * assigned.
     *
     * @return An arraylist of the names of the children of the present scope.
     */
    public ArrayList<String> getChildren() {
        return this.scopeLocal.getChildren();
    }

    /**
     * Method that allows us to remove HashMaps from our internal stack
     * structure. This is done to represent the changing of scope in the running
     * MiniPascal program.
     */
    void addTableScope(String scopeName) {
        if (!isLocked) {
            scopeLocal.addChild(scopeName);
            scopeLocal = new tableScope(scopeName, scopeLocal.getName());
            symbolTables.put(scopeName, scopeLocal);
        }
    }

    /**
     * Adds a variable to the ID table and provides the correct annotation.
     *
     * @param inputString The ID being added.
     * @param inputType The type of ID being added.
     */
    void insertID(String inputString, Type inputType) {
        if (!isLocked) {
            scopeLocal.getSymbolsObByRef().put(inputString, new Info(inputString, inputType));
        }
    }

    /**
     * Sets an object reference for the given ID. This is to be used when the
     * node for an array or procedure or function is necessary for the
     * acquisition of the satellite data associated with these types of IDs. IE,
     * the arguments for a procedure call are stored in the procedure node and
     * only acquired after the ID has been entered into the symbol table. Once
     * those satellite data are acquired, they are then added to the symbol
     * table by the object reference to the procedure node.
     *
     * Please note: this method is intended only to be invoked immediately after
     * the ID has been declared and inserted into the table. It assumes that the
     * ID exists in the local scope and will not attempt to modify the object
     * reference of an ID that is only contained in the global scope.
     *
     * @param inputString The ID that we are going to set the object reference
     * for.
     * @param objRef The object we would like to associate with the ID.
     */
    void setObjectReference(String inputString, Object objRef) {
        if (scopeLocal.getSymbolsObByRef().containsKey(inputString) && !isLocked) {
            Info infoToEdit = scopeLocal.getSymbolsObByRef().get(inputString);
            infoToEdit.setObjRef(objRef);
        }
    }

    /**
     * Gets the object reference for the given ID. Attempts to first get the
     * object reference from the local scope. Should this fail, it then attempts
     * to recursively search through the global scope for the given ID and
     * returns whatever object reference is associated with it (if there is no
     * object reference it returns null).
     *
     * @param inputString the ID that we are searching for.
     * @return An object reference that contains satellite data associated with
     * the ID.
     */
    public Object getObjectReference(String inputString) {
        return recursiveObjectSearch(scopeLocal, inputString);
    }

    /**
     * Helper method that recursively searches through the current global scope
     */
    private Object recursiveObjectSearch(tableScope i, String h) {
        if (i.getSymbolsObByRef().containsKey(h)) {
            return i.getSymbolsObByRef().get(h).getObjRef();
        } else if (i.getParent() != i.getName()) {
            return recursiveObjectSearch(symbolTables.get(i.getParent()), h);
        }
        return null;
    }

    /**
     * Check if ID is contained in the current local scope.
     *
     * @param inputString The ID we are looking for.
     * @param inputType The type of ID we are looking for.
     * @return {@code True} if the ID is present in the local scope,
     * {@code False} if it is not.
     */
    public boolean isLocalID(String inputString, Type inputType) {
        if (scopeLocal.getSymbolsObByRef().containsKey(inputString) && scopeLocal.getSymbolsObByRef().get(inputString).getType() == inputType) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if ID is contained in the global scope. The global scope is defined
     * as being the current local scope and all tables it is descended from.
     *
     * @param inputString The ID we are looking for.
     * @param inputType The type of ID we are looking for.
     * @return {@code True} if the ID is present in the global scope,
     * {@code False} if it is not.
     */
    public boolean isGlobalID(String inputString, Type inputType) {
        return recursiveSymbolSearch(scopeLocal, inputString, inputType);
    }

    /**
     * A helper method that is used to recursively search table scopes down to
     * the root scope, or true global scope.
     *
     * @param i The table scope in which we start our search
     * @param h The id name that we are searching for
     * @param g The type of id that we are searching for
     * @return
     */
    private boolean recursiveSymbolSearch(tableScope i, String h, Type g) {
        if (i.getSymbolsObByRef().containsKey(h) && i.getSymbolsObByRef().get(h).getType() == g) {
            return true;
        } else if (i.getParent() != i.getName()) {
            return recursiveSymbolSearch(symbolTables.get(i.getParent()), h, g);
        }
        return false;
    }

    /**
     * Get the type of the given symbol if it exists in the current local and
     * global scope.
     *
     * @param inputString The symbol we are checking the type of
     * @return Type of the symbol if it exists, null if it doesn't.
     */
    public Type getType(String inputString) {
        return recursiveTypeSearch(this.scopeLocal, inputString);
    }

    /**
     * A helper method to search for the type of a given id in the global scope
     */
    private Type recursiveTypeSearch(tableScope i, String h) {
        if (i.getSymbolsObByRef().containsKey(h)) {
            return i.getSymbolsObByRef().get(h).getType();
        } else if (i.getParent() != i.getName()) {
            return recursiveTypeSearch(symbolTables.get(i.getParent()), h);
        }
        return null;
    }

    /**
     * This is a class that we use to specify the parent and children scopes for
     * a given scope. That is the only practical function of this class.
     */
    private class tableScope {

        String scopeName;
        String parent;
        ArrayList<String> children;
        HashMap<String, Info> symbols;

        tableScope(String inputString, String parent) {
            this.scopeName = inputString;
            this.parent = parent;
            children = new ArrayList<String>();
            symbols = new HashMap<String, Info>();
        }

        HashMap<String, Info> getSymbolsObByRef() {
            return this.symbols;
        }

        String getName() {
            return this.scopeName;
        }

        String getParent() {
            return this.parent;
        }

        void addChild(String child) {
            children.add(child);
        }

        ArrayList<String> getChildren() {
            return this.children;
        }

        /**
         * Overridden function that is public by necessity of the method it
         * overrides. Returns this table scope as represented by a string.
         *
         * @return the string representation of this table scope.
         */
        @Override
        public String toString() {
            String returnString = "Scope: " + scopeName + "\n";
            for (Entry entry : symbols.entrySet()) {
                Info currentID = (Info) entry.getValue();
                returnString += currentID.toString() + "\n";
            }
            return returnString;
        }

        /**
         * Returns this table scope as represented by a string with
         * indentations.
         *
         * @param level number of indentations to make
         * @return the table scope as a string with indentations
         */
        public String indentedToString(int level) {
            String returnString = this.indentation(level) + "Scope: " + scopeName + "\n";
            for (Entry entry : symbols.entrySet()) {
                Info currentID = (Info) entry.getValue();
                returnString += this.indentation(level + 1) + currentID.toString() + "\n";
            }
            return returnString;
        }

        /**
         * Creates an indentation String for the indentedToString.
         *
         * @param level The amount of indentation.
         * @return A String displaying the given amount of indentation.
         */
        private String indentation(int level) {
            String answer = "";
            if (level > 0) {
                answer = "|-- ";
            }
            for (int indent = 1; indent < level; indent++) {
                answer += "--- ";
            }
            return (answer);
        }
    }

    /**
     * This class is used so as to allow us extensibility in our symbol table
     * datum.
     */
    private class Info {

        final private String lex;
        final private Type type;
        private Object objRef;

        Info(String inputString, Type type) {
            this.lex = inputString;
            this.type = type;
            objRef = null;
        }

        void setObjRef(Object objRef) {
            this.objRef = objRef;
        }

        Object getObjRef() {
            return this.objRef;
        }

        String getLex() {
            return this.lex;
        }

        Type getType() {
            return this.type;
        }

        /**
         * Returns this information as a string with indentations
         *
         * @return this information as a string with indentations
         */
        @Override
        public String toString() {
            String tabString = "\t";
            if (this.type == Type.REAL || this.type == Type.INTEGER) {
                tabString += "\t\t";
            } else if (this.type == Type.REAL_ARRAY || this.type == Type.ID_FUNCTION) {
                tabString += "\t";
            }
            return type.toString() + tabString + lex;
        }
    }

    /**
     * Returns this table as a string
     *
     * @return this table as a string
     */
    @Override
    public String toString() {
        String returnString = "";
        for (Entry entry : symbolTables.entrySet()) {
            returnString += entry.getValue().toString();
        }
        return returnString;
    }

    /**
     * Returns a string representation of this symbol table with indentations to
     * indicate the nesting of multiple table scopes.
     *
     * @return a string representing this symbol table.
     */
    public String indentedToString() {
        return recursiveIndentedToString(scopeMain, 0);
    }

    private String recursiveIndentedToString(tableScope s, int level) {
        String returnString = s.indentedToString(level);
        for (String child : s.getChildren()) {
            returnString += recursiveIndentedToString(symbolTables.get(child), level + 1);
        }
        return returnString;
    }
}
