/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser;

import java.io.IOException;
import java.util.ArrayList;
import parser.syntaxtree.*;
import scanner.Scanner;
import scanner.TokenType;
import scanner.Token;
import scanner.InvalidTokenException;
import semantics.SemanticAnalyzer;

/**
 * This is a class that allows us to recognize a correctly formatted mini pascal
 * file and create a syntax tree from that mini pascal file. It takes in a
 * series of tokens from a scanner object, then determines what to do next based
 * on the next token in line. This is a process that is realized by recursive
 * descent, with each production rule in the grammar having a corresponding
 * method in this class, though many of them are package private.
 *
 * @author Matt
 */
public class Parser {

    /**
     * different objects that need to be accessed across many methods
     */
    /**
     * the DFA of our compiler, reads in tokens from text/pascal file
     */
    private Scanner ourScanner;
    /**
     * the next token that our scanner has found
     */
    private Token lookahead;
    /**
     * the symbol table that we enter identifiers into
     */
    private SymbolTable symbolTable;

    /**
     * The constructor for our parser. Takes the scanner we are reading tokens
     * from as an argument.
     *
     * @param inputScanner A Java scanner object that represents the DFA of our
     * compiler.
     * @throws InvalidTokenException
     */
    public Parser(Scanner inputScanner) throws InvalidTokenException {
        ourScanner = inputScanner;
        try {
            lookahead = ourScanner.nextToken();
        } catch (IOException ex) {
            System.out.println("IO Error occured when trying to read from the file\n" + ex.toString());
        }
    }

    /**
     * Package private constructor that allows us to test on inputs with
     * variables that have not been declared. Do not use this constructor
     * outside of testing conditions where you need to test a function deeper in
     * the method call stack than declarations(). The test case this constructor
     * handles is where we want to test a subfunction that contains IDs as part
     * of its production rules without those IDs having been declared. An
     * example would be the statement() function.
     *
     * @param inputScanner The scanner
     * @param testTable Table populated with any IDs that are in the unit test.
     */
    Parser(Scanner inputScanner, SymbolTable testTable) throws InvalidTokenException {
        ourScanner = inputScanner;
        try {
            lookahead = ourScanner.nextToken();
        } catch (IOException ex) {
            System.out.println("IO Error occured when trying to read from the file\n" + ex.toString());
        }
        symbolTable = testTable;
    }

    /**
     * Returns the symbol table that the parser has generated.
     *
     * @return A symbol table containing the IDs found in the parsed text.
     */
    public SymbolTable getSymbolTable() {
        return this.symbolTable;
    }

    /**
     * Function that matches the lookahead Token with an expected tokenType and
     * removes the lookahead from the input stream of our scanner, allowing us
     * to move on to the next token.
     *
     * @param expected The TokenType that is expected.
     * @throws InvalidTokenException When invalid (not included in the grammar)
     * token is found.
     * @throws UnexpectedTokenException When unexpected token is found.
     */
    void match(TokenType expected) throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            if (this.lookahead.getType() == expected) {
                try {
                    lookahead = ourScanner.nextToken();
                } catch (IOException ex) {
                    System.out.println("IO Error occured when trying to read from the file\n" + ex.toString());
                }
            } else {
                throw new UnexpectedTokenException("Expected a token of the type " + expected + " found instead " + this.lookahead.getType(), ourScanner.getLine() + 1, lookahead);
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Method that represents the starting symbol for our grammar. This method
     * will call every other method in the object Parser when appropriate. Each
     * of these methods represent one of the productions as found in our grammar
     * and are named for the left hand component of a given production.
     *
     * @return ProgramNode A ProgramNode object from package parser.syntaxtree
     * that represents the parsed program.
     * @throws InvalidTokenException When invalid (not included in the grammar)
     * token is found.
     * @throws UnexpectedTokenException When a valid but unexpected token is
     * found.
     */
    public ProgramNode program() throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            match(TokenType.KW_program);
            String lexTemp = lookahead.getLex();
            match(TokenType.id);
            //Create symbol table whose base scope is the name found as ID
            symbolTable = new SymbolTable(lexTemp);
            //Create a program node with the name found as ID
            ProgramNode programNode = new ProgramNode(lexTemp);
            match(TokenType.SYM_semicolon);
            //Assign attributes of the program node
            programNode.setVariables(declarations(new DeclarationsNode()));
            programNode.setFunctions(subprogramDeclarations(new SubProgramDeclarationsNode()));
            programNode.setMain(compoundStatement());
            match(TokenType.SYM_period);
            return programNode;
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for identifier_list as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     * @return {@code ArrayList<String>} an array list of string representations of the
     * identifiers we have parsed.
     */
    ArrayList<String> identifierList(ArrayList<String> idents) throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            //add the identifier to the list
            String lexTemp = lookahead.getLex();
            match(TokenType.id);
            idents.add(lexTemp);
            //dig for more identifiers if there's a comma ahead
            if (lookahead.getType() == TokenType.SYM_comma) {
                match(TokenType.SYM_comma);
                identifierList(idents);
            }
            return idents;
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for declarations as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     * @return DeclarationsNode an object representing the declarations we have
     * parsed.
     */
    DeclarationsNode declarations(DeclarationsNode decs) throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            if (lookahead.getType() == TokenType.KW_var) {
                match(TokenType.KW_var);
                //get the list of identifiers we are adding
                ArrayList<String> idents = identifierList(new ArrayList<String>());
                //get the type of the identifiers
                match(TokenType.SYM_colon);
                ArrayList<Object> identsTuple = type();
                match(TokenType.SYM_semicolon);
                //place the type into variable
                Type identsType = (Type) identsTuple.get(0);
                String lowerBound;
                String upperBound;
                //if we got array information, add it. otherwise add just the type information.
                if (identsTuple.size() > 1) {
                    lowerBound = (String) identsTuple.get(1);
                    upperBound = (String) identsTuple.get(2);
                    for (String s : idents) {
                        ArrayVariableNode arrayVar = new ArrayVariableNode(s, lowerBound, upperBound, identsType);
                        decs.addVariable(arrayVar);
                        symbolTable.insertID(s, identsType);
                        symbolTable.setObjectReference(s, arrayVar);
                    }
                } else {
                    for (String s : idents) {
                        RegularVariableNode var = new RegularVariableNode(s, symbolTable);
                        decs.addVariable(var);
                        symbolTable.insertID(s, identsType);
                    }
                }
                //try for another list of declarations
                declarations(decs);
                //return the list of declarations
                return decs;
            } else {
                //Lambda option, return what has been generated so far
                return decs;
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for type as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     * @return {@code ArrayList<Object>} A tuple of length 1 or 3. First element is the
     * {@code parser.Type} that was found. The next two values are integers
     * representing the bounds of an array, if an array was found.
     */
    ArrayList<Object> type() throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            ArrayList<Object> returns = new ArrayList<>();
            //If we have an array, establish what bounds are specified for the array.
            if (lookahead.getType() == TokenType.KW_array) {
                match(TokenType.KW_array);
                match(TokenType.SYM_lbracket);
                String lowerBound = lookahead.getLex();
                match(TokenType.num);
                match(TokenType.SYM_colon);
                String upperBound = lookahead.getLex();
                match(TokenType.num);
                match(TokenType.SYM_rbracket);
                match(TokenType.KW_of);
                Type type = standardType();
                if (type == Type.REAL) {
                    returns.add(Type.REAL_ARRAY);
                    returns.add(lowerBound);
                    returns.add(upperBound);
                    return returns;
                } else {
                    returns.add(Type.INTEGER_ARRAY);
                    returns.add(lowerBound);
                    returns.add(upperBound);
                    return returns;
                }
                //Otherwise just get the standard type (INTEGER or REAL).
            } else {
                returns.add(standardType());
                return returns;
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for standard_type as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     * @return Type instance of the enum specifying the type of id
     */
    Type standardType() throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            if (lookahead.getType() == TokenType.KW_integer) {
                match(TokenType.KW_integer);
                return Type.INTEGER;
            } else {
                match(TokenType.KW_real);
                return Type.REAL;
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for subprogram_declarations as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     * @return SubProgramDeclarationsNode an object representing the subprogram
     * declarations we have parsed.
     */
    SubProgramDeclarationsNode subprogramDeclarations(SubProgramDeclarationsNode subprograms) throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            //If we have a subprogram declaration, add it to the list of declarations
            if (lookahead.getType() == TokenType.KW_function || lookahead.getType() == TokenType.KW_procedure) {
                SubProgramNode subprogram = subprogramDeclaration();
                symbolTable.setObjectReference(subprogram.getName(), subprogram);
                subprograms.addSubProgramDeclaration(subprogram);
                match(TokenType.SYM_semicolon);
                //Try for another subprogram declaration
                subprogramDeclarations(subprograms);
                return subprograms;
            } else {
                //lambda option, return the list of subprograms found so far.
                return subprograms;
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for subprogram_declaration as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    SubProgramNode subprogramDeclaration() throws InvalidTokenException, UnexpectedTokenException {
        //Get the subprogram header
        SubProgramNode subprogram = subprogramHead();
        //Get the declarations for the subprogram. First create list from the arguments of the subprogram,
        //then complete the list with variables explicitly declared in the subprogram's declarations.
        DeclarationsNode decs = new DeclarationsNode();
        ArgumentsNode argsNode = subprogram.getArgs();
        ArrayList<VariableNode> argsList = argsNode.getArgs();
        for (VariableNode argVar : argsList) {
            decs.addVariable(argVar);
        }
        subprogram.setDeclarations(declarations(decs));
        subprogram.setSubProgramDeclarations(subprogramDeclarations(new SubProgramDeclarationsNode()));
        //Get the compound statement for this subprogram
        subprogram.setCompoundStatement(compoundStatement());
        //If we are parsing a FunctionNode, check if it has a return variable.
        if (subprogram instanceof FunctionNode) {
            FunctionNode subFunc = (FunctionNode) subprogram;
            for (StatementNode statement : subprogram.getCompoundStatement().getStatements()) {
                if (statement instanceof AssignmentStatementNode) {
                    AssignmentStatementNode assignment = (AssignmentStatementNode) statement;
                    if (assignment.getLValue() instanceof ReturnVariableNode) {
                        //Assign the return variable its proper type, assign the function its return expression.
                        ReturnVariableNode returnVar = (ReturnVariableNode) assignment.getLValue();
                        returnVar.setType(subFunc.getReturnType());
                        subFunc.setReturnExpr(assignment.getExpression());
                    }
                }
            }
        }
        symbolTable.setTableScope(symbolTable.getParentScope());
        return subprogram;
    }

    /**
     * Production rule for subprogram_head as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    SubProgramNode subprogramHead() throws InvalidTokenException, UnexpectedTokenException {
        String lexTemp;
        if (lookahead != null) {
            if (lookahead.getType() == TokenType.KW_function) {
                //If we have a function, add the ID to the symbol table as a function
                match(TokenType.KW_function);
                lexTemp = lookahead.getLex();
                match(TokenType.id);
                symbolTable.insertID(lexTemp, Type.ID_FUNCTION);
                symbolTable.addTableScope(lexTemp);
                //Create a FunctionNode and point the symbol table's object reference for this ID to the FunctionNode
                //This allows us to capture satellite data for the function
                FunctionNode functionNode = new FunctionNode(lexTemp);
                symbolTable.setObjectReference(lexTemp, functionNode);
                functionNode.setArguments(arguments());
                match(TokenType.SYM_colon);
                //Get the return type for the function.
                ArrayList<Object> typeTuple = type();
                Type returnType = (Type) typeTuple.get(0);
                if (typeTuple.size() > 1) {
                    if (returnType == Type.INTEGER_ARRAY) {
                        functionNode.setReturnType(Type.INTEGER_ARRAY, new int[]{Integer.parseInt(typeTuple.get(1).toString()), Integer.parseInt(typeTuple.get(2).toString())});
                    } else if (returnType == Type.REAL_ARRAY) {
                        functionNode.setReturnType(Type.REAL_ARRAY, new int[]{Integer.parseInt(typeTuple.get(1).toString()), Integer.parseInt(typeTuple.get(2).toString())});
                    } else {
                        throw new UnexpectedTokenException("Expected a type of array, found instead " + typeTuple.get(0).toString(), ourScanner.getLine() + 1);
                    }
                }
                //Set the return type for the function.
                functionNode.setReturnType(returnType);
                match(TokenType.SYM_semicolon);
                return functionNode;
            } else {
                //If we have a procedure, add the ID to the symbol table as a procedure
                match(TokenType.KW_procedure);
                lexTemp = lookahead.getLex();
                match(TokenType.id);
                symbolTable.insertID(lexTemp, Type.ID_PROCEDURE);
                symbolTable.addTableScope(lexTemp);
                //Create a procedure node and point the symbol table's object reference for this ID to the FunctionNode
                //This allows us to capture satellite data for the procedure
                ProcedureNode procedureNode = new ProcedureNode(lexTemp);
                symbolTable.setObjectReference(lexTemp, procedureNode);
                procedureNode.setArguments(arguments());
                match(TokenType.SYM_semicolon);
                return procedureNode;
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for arguments as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    ArgumentsNode arguments() throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            if (lookahead.getType() == TokenType.SYM_lparens) {
                match(TokenType.SYM_lparens);
                ArgumentsNode args = parameterList(new ArgumentsNode());
                match(TokenType.SYM_rparens);
                return args;
            } else {
                //lambda option
                return new ArgumentsNode();
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for parameter_list as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    ArgumentsNode parameterList(ArgumentsNode args) throws InvalidTokenException, UnexpectedTokenException {
        //Get the identifiers listed as parameters.
        ArrayList<String> idents = identifierList(new ArrayList<String>());
        match(TokenType.SYM_colon);
        //Get the type of the parameters.
        ArrayList<Object> identsTuple = type();
        //If the type is array, add the type as array with its bounds
        Type identsType = (Type) identsTuple.get(0);
        if (identsTuple.size() > 1) {
            String lowerBound = (String) identsTuple.get(1);
            String upperBound = (String) identsTuple.get(2);
            for (String s : idents) {
                ArrayVariableNode arrayVar = new ArrayVariableNode(s, lowerBound, upperBound, identsType);
                args.addVariable(arrayVar);
                symbolTable.insertID(s, identsType);
                symbolTable.setObjectReference(s, arrayVar);
            }
            //Otherwise add the standard type (INTEGER or REAL)
        } else {
            for (String s : idents) {
                RegularVariableNode var = new RegularVariableNode(s, symbolTable);
                args.addVariable(var);
                symbolTable.insertID(s, identsType);
            }
        }
        //If we have a semicolon, try for another set of parameters, presumably of different type.
        if (lookahead != null) {
            if (lookahead.getType() == TokenType.SYM_semicolon) {
                match(TokenType.SYM_semicolon);
                parameterList(args);
            }
            return args;
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for compound_statement as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    CompoundStatementNode compoundStatement() throws InvalidTokenException, UnexpectedTokenException {
        match(TokenType.KW_begin);
        CompoundStatementNode returnNode = new CompoundStatementNode(optionalStatements());
        match(TokenType.KW_end);
        return returnNode;
    }

    /**
     * Production rule for optional_statements as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    ArrayList<StatementNode> optionalStatements() throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            //Make sure we have a statement by checking the next token matches the first set of the non-terminal
            if (lookahead.getType() == TokenType.id || lookahead.getType() == TokenType.KW_begin
                    || lookahead.getType() == TokenType.KW_if || lookahead.getType() == TokenType.KW_while
                    || lookahead.getType() == TokenType.KW_readint || lookahead.getType() == TokenType.KW_readfloat
                    || lookahead.getType() == TokenType.KW_write) {
                return statementList(new ArrayList<StatementNode>());
            } else {
                //lambda option, return an empty arraylist
                return new ArrayList<StatementNode>();
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for statement_list as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    ArrayList<StatementNode> statementList(ArrayList<StatementNode> statements) throws InvalidTokenException, UnexpectedTokenException {
        //Add the next statement
        statements.add(statement());
        if (lookahead != null) {
            //Try for another statement
            if (lookahead.getType() == TokenType.SYM_semicolon) {
                match(TokenType.SYM_semicolon);
                statementList(statements);
            }
            return statements;
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for statement as in the grammar. Also contains the whole
     * of the production rules for "variable" and "procedure_statement".
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    StatementNode statement() throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            switch (lookahead.getType()) {
                //Case for "variable assignop expression" production body component as in the grammar.
                //Also the case for "procedure_statement" production body component.
                //Also the case for returning values from a function.
                case id:
                    //"variable assignop expression"
                    if (symbolTable.isLocalID(lookahead.getLex(), Type.INTEGER) || symbolTable.isLocalID(lookahead.getLex(), Type.REAL)
                            || symbolTable.isLocalID(lookahead.getLex(), Type.INTEGER_ARRAY) || symbolTable.isLocalID(lookahead.getLex(), Type.REAL_ARRAY)) {
                        VariableNode var = variable();
                        match(TokenType.assignop);
                        ExpressionNode expr = expression();
                        return new AssignmentStatementNode(var, expr);
                        //"procedure_statement" or return value of function.
                    } else if (symbolTable.isLocalID(lookahead.getLex(), Type.ID_PROCEDURE) || symbolTable.isGlobalID(lookahead.getLex(), Type.ID_PROCEDURE)
                            || symbolTable.isLocalID(lookahead.getLex(), Type.ID_FUNCTION) || symbolTable.isGlobalID(lookahead.getLex(), Type.ID_FUNCTION)) {
                        //If the id matches the current scope we are in, this is an assignment of a return value.
                        String nextString = lookahead.getLex();
                        if (nextString.equals(symbolTable.getTableScope()) && symbolTable.getType(nextString) == Type.ID_FUNCTION) {
                            match(TokenType.id);
                            match(TokenType.assignop);
                            ExpressionNode expr = expression();
                            ReturnVariableNode returnVar = new ReturnVariableNode(nextString);
                            return new AssignmentStatementNode(returnVar, expr);
                            //Otherwise it is a normal procedure statement
                        } else {
                            return procedureStatement();
                        }
                    }
                    break;
                //Case for production body component "compound_statement".
                case KW_begin:
                    match(TokenType.KW_begin);
                    CompoundStatementNode returnCompS = new CompoundStatementNode(optionalStatements());
                    match(TokenType.KW_end);
                    return returnCompS;
                //Case for production body component "if expression then statement else statement"
                case KW_if:
                    match(TokenType.KW_if);
                    //Get the evalautation expression and ensure it can be found as comparison operation or a negation of a comparison operation.
                    ExpressionNode exprIf = expression();
                    if (!(exprIf instanceof OperationNode || exprIf instanceof NegationNode)) {
                        throw new UnexpectedTokenException("Invalid expression for if statement evaluation, must be a relation operation or its negation: " + exprIf.toString(), ourScanner.getLine() + 1);
                    }
                    //If it is an operation node, check that it is a relation operation node.
                    if (exprIf instanceof OperationNode) {
                        OperationNode op = (OperationNode) exprIf;
                        if (!(op.getOperation().contains("=") || op.getOperation().contains("<>") || op.getOperation().contains("<=")
                                || op.getOperation().contains(">=") || op.getOperation().contains("<") || op.getOperation().contains(">"))) {
                            throw new UnexpectedTokenException("Invalid operation type, must be a comparison operation: " + op.toString(), ourScanner.getLine() + 1);
                        }
                    }
                    //Get the statement for "true" evalutations
                    match(TokenType.KW_then);
                    StatementNode thenStatement = statement();
                    //Get the statement for "false" evaluations
                    match(TokenType.KW_else);
                    StatementNode elseStatement = statement();
                    //Create and return the if statement
                    IfStatementNode returnIf = new IfStatementNode();
                    returnIf.setTest(exprIf);
                    returnIf.setThenStatement(thenStatement);
                    returnIf.setElseStatement(elseStatement);
                    return returnIf;
                //Case for production body component "while expression do statement"
                case KW_while:
                    match(TokenType.KW_while);
                    //Get the evalautation expression and ensure it can be found as comparison operation or a negation of a comparison operation.
                    ExpressionNode exprWhile = expression();
                    if (!(exprWhile instanceof OperationNode || exprWhile instanceof NegationNode)) {
                        throw new UnexpectedTokenException("Invalid expression for while statement evaluation, must be a relation operation or its negation: " + exprWhile.toString(), ourScanner.getLine() + 1);
                    }
                    //If it is an operation node, check that it is a relation operation node.
                    if (exprWhile instanceof OperationNode) {
                        OperationNode op = (OperationNode) exprWhile;
                        if (!(op.getOperation().contains("=") || op.getOperation().contains("<>") || op.getOperation().contains("<=")
                                || op.getOperation().contains(">=") || op.getOperation().contains("<") || op.getOperation().contains(">"))) {
                            throw new UnexpectedTokenException("Invalid operation type, must be a comparison operation: " + op.toString(), ourScanner.getLine() + 1);
                        }
                    }
                    match(TokenType.KW_do);
                    //Get the statement for "true" evaluations and return a new while statement node
                    StatementNode whileStatement = statement();
                    WhileStatementNode returnWhile = new WhileStatementNode(exprWhile, whileStatement);
                    return returnWhile;
                //Case for production body component "write(expression)"
                case KW_write:
                    match(TokenType.KW_write);
                    match(TokenType.SYM_lparens);
                    ExpressionNode exprWrite = expression();
                    match(TokenType.SYM_rparens);
                    WriteNode returnWrite = new WriteNode(exprWrite);
                    return returnWrite;
                //Case for production body component "read(id)"
                case KW_readfloat:
                    match(TokenType.KW_readfloat);
                case KW_readint:
                    if (lookahead.getType() == TokenType.KW_readint) {
                        match(TokenType.KW_readint);
                    }
                    match(TokenType.SYM_lparens);
                    String idToAssign = lookahead.getLex();
                    Type readType = symbolTable.getType(idToAssign);
                    match(TokenType.id);
                    //Ensure the read command reads in either an array index value or a singular variable value.
                    if (lookahead.getType() == TokenType.SYM_lbracket && (symbolTable.getType(idToAssign) == Type.REAL_ARRAY || symbolTable.getType(idToAssign) == Type.INTEGER_ARRAY)) {
                        match(TokenType.SYM_lbracket);
                        ExpressionNode expr = expression();
                        ArrayIndexNode varArr = new ArrayIndexNode((ArrayVariableNode) symbolTable.getObjectReference(idToAssign), expr, symbolTable);
                        match(TokenType.SYM_rbracket);
                        match(TokenType.SYM_rparens);
                        ReadNode returnRead = new ReadNode(varArr, null);
                        returnRead.setType(readType);
                        return returnRead;
                    } else {
                        match(TokenType.SYM_rparens);
                        if (readType == Type.INTEGER || readType == Type.REAL) {
                            RegularVariableNode varReg = new RegularVariableNode(idToAssign, symbolTable);
                            ReadNode returnRead = new ReadNode(varReg, null);
                            returnRead.setType(readType);
                            return returnRead;
                        } else {
                            throw new UnexpectedTokenException("Expected an integer or real variable, instead found id " + idToAssign + " of the type " + readType.toString(), ourScanner.getLine() + 1, lookahead);
                        }
                    }
                //No production body component corresponding to lambda, so throw error if anything else.
                default:
                    throw new UnexpectedTokenException("Expected a statement and did not find one.", ourScanner.getLine() + 1, lookahead);
            }
            throw new UnexpectedTokenException("Expected a statement and did not find one.", ourScanner.getLine() + 1, lookahead);
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for variable as in the grammar. Returns the variable
     * object that was declared earlier, if it exists in the symbol table. If it
     * does not, throws an error.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    VariableNode variable() throws InvalidTokenException, UnexpectedTokenException {
        String variableID = lookahead.getLex();
        //If the variable can be found in the local scope as a regular variable, match it.
        if (symbolTable.isLocalID(lookahead.getLex(), Type.INTEGER) || symbolTable.isLocalID(lookahead.getLex(), Type.REAL)
                || symbolTable.isLocalID(lookahead.getLex(), Type.INTEGER_ARRAY) || symbolTable.isLocalID(lookahead.getLex(), Type.REAL_ARRAY)) {
            match(TokenType.id);
        } else {
            throw new UnexpectedTokenException("Encountered undeclared variable " + lookahead.getLex(), ourScanner.getLine() + 1);
        }
        if (lookahead != null) {
            //If we find a bracket, we are looking for an array index node.
            if (lookahead.getType() == TokenType.SYM_lbracket
                    && (symbolTable.isLocalID(variableID, Type.INTEGER_ARRAY) || symbolTable.isLocalID(variableID, Type.REAL_ARRAY))) {
                match(TokenType.SYM_lbracket);
                ExpressionNode index = expression();
                match(TokenType.SYM_rbracket);
                Object refNode = symbolTable.getObjectReference(variableID);
                if (refNode instanceof ArrayVariableNode) {
                    return new ArrayIndexNode((ArrayVariableNode) refNode, index, symbolTable);
                } else {
                    throw new UnexpectedTokenException("Expected array reference, found reference to object \"" + refNode.toString(), ourScanner.getLine() + 1);
                }
                //Otherwise we have either a regular variable or a full length array.
            } else if (symbolTable.isLocalID(variableID, Type.INTEGER) || symbolTable.isLocalID(variableID, Type.REAL)) {
                return new RegularVariableNode(variableID, symbolTable);
            } else if (symbolTable.isLocalID(variableID, Type.INTEGER_ARRAY) || symbolTable.isLocalID(variableID, Type.REAL_ARRAY)) {
                Object objRef = symbolTable.getObjectReference(variableID);
                if (objRef instanceof ArrayVariableNode) {
                    return (ArrayVariableNode) objRef;
                } else {
                    throw new UnexpectedTokenException("Expected array variable, instead found id \"" + variableID + "\" of the type: " + symbolTable.getType(variableID), ourScanner.getLine() + 1);
                }
            } else {
                throw new UnexpectedTokenException("Expected variable or array index, instead found id \"" + variableID + "\" of the type: " + symbolTable.getType(variableID), ourScanner.getLine() + 1);
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for procedure_statement as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    CallNodeAsStatement procedureStatement() throws InvalidTokenException, UnexpectedTokenException {
        String procID = lookahead.getLex();
        match(TokenType.id);
        if (lookahead != null) {
            if (symbolTable.isGlobalID(procID, Type.ID_PROCEDURE) || symbolTable.isGlobalID(procID, Type.ID_FUNCTION)) {
                //If it has an argument list, gather that argument list and create a procedure call with those arguments
                if (lookahead.getType() == TokenType.SYM_lparens) {
                    match(TokenType.SYM_lparens);
                    if (symbolTable.getType(procID) == Type.ID_FUNCTION) {
                        CallNodeAsStatement funcCall = new CallNodeAsStatement(procID, symbolTable);
                        ArrayList<ExpressionNode> argsFunc = new ArrayList<ExpressionNode>();
                        expressionList(argsFunc);
                        funcCall.setArgs(argsFunc);
                        match(TokenType.SYM_rparens);
                        return funcCall;
                    } else if (symbolTable.getType(procID) == Type.ID_PROCEDURE) {
                        CallNodeAsStatement procCall = new CallNodeAsStatement(procID, symbolTable);
                        ArrayList<ExpressionNode> argsProc = new ArrayList<ExpressionNode>();
                        expressionList(argsProc);
                        procCall.setArgs(argsProc);
                        match(TokenType.SYM_rparens);
                        return procCall;
                    } else {
                        throw new UnexpectedTokenException("Encountered reference to undeclared function or procedure: " + procID, ourScanner.getLine() + 1);
                    }
                    //Otherwise create a procedure call without any arguments
                } else {
                    if (symbolTable.getType(procID) == Type.ID_PROCEDURE) {
                        CallNodeAsStatement procCall = new CallNodeAsStatement(procID, symbolTable);
                        return procCall;
                    } else if (symbolTable.getType(procID) == Type.ID_FUNCTION) {
                        CallNodeAsStatement funcCall = new CallNodeAsStatement(procID, symbolTable);
                        return funcCall;
                    } else {
                        throw new UnexpectedTokenException("Encountered reference to undeclared function or procedure: " + procID, ourScanner.getLine() + 1);
                    }
                }
            } else {
                throw new UnexpectedTokenException("Expected function or procedure id, instead found id \"" + procID + "\" of type " + symbolTable.getType(procID).toString());
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for expression_list as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    void expressionList(ArrayList<ExpressionNode> args) throws InvalidTokenException, UnexpectedTokenException {
        args.add(expression(true));
        if (lookahead != null) {
            if (lookahead.getType() == TokenType.SYM_comma) {
                match(TokenType.SYM_comma);
                expressionList(args);
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for expression as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    ExpressionNode expression(boolean... fromExpList) throws InvalidTokenException, UnexpectedTokenException {
        ExpressionNode simpleExpression = simpleExpression(fromExpList);
        if (lookahead != null) {
            if (lookahead.getType() == TokenType.relop) {
                ExpressionNode left = simpleExpression;
                String relop = lookahead.getLex();
                match(TokenType.relop);
                ExpressionNode right = simpleExpression(fromExpList);
                OperationNode operationNode = new OperationNode(TokenType.relop, relop);
                operationNode.setLeft(left);
                operationNode.setRight(right);
                if (lookahead.getType() == TokenType.relop) {
                    throw new UnexpectedTokenException("Relation operators can only be used once per expression\n", ourScanner.getLine() + 1);
                }
                return operationNode;
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
        return simpleExpression;
    }

    /**
     * Production rule for simple_expression as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    ExpressionNode simpleExpression(boolean... fromExpList) throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            if (lookahead.getLex().equalsIgnoreCase("+") || lookahead.getLex().equalsIgnoreCase("-")) {
                String addop = lookahead.getLex();
                match(TokenType.addop);
                OperationNode adjustSign = new OperationNode(TokenType.addop, addop);
                adjustSign.setLeft(new ValueNode("0"));
                adjustSign.setRight(term(fromExpList));
                OperationNode simplePart = simplePart(fromExpList);
                if (simplePart == null) {
                    return adjustSign;
                } else {
                    simplePart.setLeft(adjustSign);
                    return simplePart;
                }
            } else {
                ExpressionNode term = term(fromExpList);
                OperationNode simplePart = simplePart(fromExpList);
                if (simplePart == null) {
                    return term;
                } else {
                    simplePart.setLeft(term);
                    return simplePart;
                }
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for simple_part as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    OperationNode simplePart(boolean... fromExpList) throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            if (lookahead.getType() == TokenType.addop) {
                String addop = lookahead.getLex();
                match(TokenType.addop);
                ExpressionNode term = term(fromExpList);
                OperationNode nextSimplePart = simplePart(fromExpList);
                if (nextSimplePart == null) {
                    OperationNode simplePart = new OperationNode(TokenType.addop, addop);
                    simplePart.setRight(term);
                    return simplePart;
                } else {
                    OperationNode simplePart = new OperationNode(TokenType.addop, addop);
                    nextSimplePart.setLeft(term);
                    simplePart.setRight(nextSimplePart);
                    return simplePart;
                }
            } else {
                //lambda option
                return null;
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for term as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    ExpressionNode term(boolean... fromExpList) throws InvalidTokenException, UnexpectedTokenException {
        ExpressionNode factor = factor(fromExpList);
        OperationNode termPart = termPart(fromExpList);
        if (termPart == null) {
            return factor;
        } else {
            termPart.setLeft(factor);
            return termPart;
        }
    }

    /**
     * Production rule for term_part as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    OperationNode termPart(boolean... fromExpList) throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            if (lookahead.getType() == TokenType.mulop) {
                String mulop = lookahead.getLex();
                match(TokenType.mulop);
                ExpressionNode factor = factor(fromExpList);
                OperationNode nextTermPart = termPart(fromExpList);
                if (nextTermPart == null) {
                    OperationNode termPart = new OperationNode(TokenType.mulop, mulop);
                    termPart.setRight(factor);
                    return termPart;
                } else {
                    OperationNode termPart = new OperationNode(TokenType.mulop, mulop);
                    nextTermPart.setLeft(factor);
                    termPart.setRight(nextTermPart);
                    return termPart;
                }
            } else {
                //lambda option
                return null;
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }

    /**
     * Production rule for factor as in the grammar.
     *
     * @throws InvalidTokenException
     * @throws UnexpectedTokenException
     */
    ExpressionNode factor(boolean... fromExpList) throws InvalidTokenException, UnexpectedTokenException {
        if (lookahead != null) {
            switch (lookahead.getType()) {
                case id:
                    String identifier = lookahead.getLex();
                    match(TokenType.id);
                    if (lookahead != null) {
                        //Case for array references
                        if (lookahead.getType() == TokenType.SYM_lbracket) {
                            if (symbolTable.getType(identifier) != Type.REAL_ARRAY && symbolTable.getType(identifier) != Type.INTEGER_ARRAY
                                    || !(symbolTable.isLocalID(identifier, Type.INTEGER_ARRAY)) && !(symbolTable.isLocalID(identifier, Type.REAL_ARRAY))) {
                                throw new UnexpectedTokenException("Encountered reference to undeclared array: " + identifier, ourScanner.getLine() + 1);
                            }
                            match(TokenType.SYM_lbracket);
                            ExpressionNode index = expression();
                            match(TokenType.SYM_rbracket);
                            return new ArrayIndexNode((ArrayVariableNode) symbolTable.getObjectReference(identifier), index, symbolTable);
                            //Case for function references (no procedures as they don't have return values)
                        } else if (lookahead.getType() == TokenType.SYM_lparens) {
                            if (symbolTable.getType(identifier) == Type.ID_PROCEDURE) {
                                throw new UnexpectedTokenException("Encountered reference to procedure " + identifier + " in an expression\nOnly function calls can be expressions as procedures lack return types");
                            } else if (symbolTable.getType(identifier) != Type.ID_FUNCTION) {
                                throw new UnexpectedTokenException("Encountered reference to undeclared function: " + identifier, ourScanner.getLine() + 1);
                            }
                            CallNodeAsExpression funcCall = new CallNodeAsExpression(identifier, symbolTable);
                            ArrayList<ExpressionNode> argsFunc = new ArrayList<ExpressionNode>();
                            match(TokenType.SYM_lparens);
                            expressionList(argsFunc);
                            match(TokenType.SYM_rparens);
                            funcCall.setArgs(argsFunc);
                            return funcCall;
                            //Case for arrays as part of expression lists, and for regular variables
                        } else {
                            //Whole arrays, not array indecies, but only in the context of expression lists
                            //It's ugly but I lack the time to refactor it into something sensible and it works now.
                            if (!(symbolTable.isLocalID(identifier, Type.INTEGER) || symbolTable.isLocalID(identifier, Type.REAL)) && fromExpList == null) {
                                throw new UnexpectedTokenException("Encountered reference to undeclared variable: " + identifier, ourScanner.getLine() + 1);
                            } else if (fromExpList != null && (symbolTable.isLocalID(identifier, Type.INTEGER_ARRAY) || symbolTable.isLocalID(identifier, Type.REAL_ARRAY))) {
                                if (symbolTable.getObjectReference(identifier) instanceof ArrayVariableNode) {
                                    ArrayVariableNode arrVar = (ArrayVariableNode) symbolTable.getObjectReference(identifier);
                                    //manually create deep copy...
                                    ArrayVariableNode ardVar = new ArrayVariableNode(arrVar.getName(), Integer.toString(arrVar.getLowerBound()), Integer.toString(arrVar.getUpperBound()), arrVar.getType());
                                    return ardVar;
                                }
                            } else if (symbolTable.isLocalID(identifier, Type.INTEGER) || symbolTable.isLocalID(identifier, Type.REAL)) {
                                //If its not a whole array, create a regular variable
                                RegularVariableNode regVariableNode = new RegularVariableNode(identifier, symbolTable.getType(identifier));
                                return regVariableNode;
                            } else {
                                throw new UnexpectedTokenException("Encountered reference to undeclared variable: " + identifier, ourScanner.getLine() + 1);
                            }
                        }
                    } else {
                        throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
                    }
                case num:
                    String value = lookahead.getLex();
                    match(TokenType.num);
                    return new ValueNode(value);
                case SYM_lparens:
                    match(TokenType.SYM_lparens);
                    ExpressionNode expressionNode = expression();
                    match(TokenType.SYM_rparens);
                    return expressionNode;
                case KW_not:
                    match(TokenType.KW_not);
                    ExpressionNode notExpr = factor();
                    //Ensure a not node negates either another not node or an operation node containing a relation operation
                    if (!(notExpr instanceof NegationNode || notExpr instanceof OperationNode)) {
                        throw new UnexpectedTokenException("Invalid quantity to be negated, must be comparison operation or its negation: " + notExpr.toString(), ourScanner.getLine() + 1);
                    }
                    if (notExpr instanceof OperationNode) {
                        OperationNode op = (OperationNode) notExpr;
                        if (!(op.getOperation().contains("=") || op.getOperation().contains("<>") || op.getOperation().contains("<=")
                                || op.getOperation().contains(">=") || op.getOperation().contains("<") || op.getOperation().contains(">"))) {
                            throw new UnexpectedTokenException("Invalid operation type, must be a comparison operation: " + op.toString(), ourScanner.getLine() + 1);
                        }
                    }
                    return new NegationNode(notExpr);
                default:
                    throw new UnexpectedTokenException("\nError:", ourScanner.getLine() + 1, lookahead);
            }
        } else {
            throw new UnexpectedTokenException("Encountered EOF marker sooner than expected.", ourScanner.getLine() + 1);
        }
    }
    /**
     * Error method that allowed us to print stack traces. Deprecated method
     * retained for the option of returning to it in the future. If you see this
     * in JavaDoc you may remove it from the JavaDoc by either deleting the last
     * lines of Parser.java or by setting JavaDoc generation to not include
     * private methods.
     *
     * @throws UnexpectedTokenException
     */
//    private void throwError() throws UnexpectedTokenException{
//    StackTraceElement[] arrStackTrace = Thread.currentThread().getStackTrace();
//    String strStackTrace = new String();
//    for (StackTraceElement el : arrStackTrace) {
//        strStackTrace += el.toString() + "\n";
//    }
//    System.out.println(strStackTrace);
//    throw new UnexpectedTokenException("\nError:", ourScanner.getLine() + 1, lookahead);
//    }
}
