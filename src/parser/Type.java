/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser;

/**
 * An enum that enumerates the different type of variables that are possible.
 *
 * @author Matt
 */
public enum Type {
    REAL, INTEGER, REAL_ARRAY, INTEGER_ARRAY, ID_PROGRAM, ID_PROCEDURE, ID_FUNCTION;
}
