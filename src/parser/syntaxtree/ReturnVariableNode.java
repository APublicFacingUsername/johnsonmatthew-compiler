/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser.syntaxtree;

import parser.SymbolTable;
import parser.Type;

/**
 * Represents a variable that is being returned from a function call. Should
 * only be instantiated with a name that corresponds to the function that was
 * called.
 *
 * @author Matt
 */
public class ReturnVariableNode extends VariableNode {

    private String name;

    /**
     * Constructor that takes the name of the variable.
     *
     * @param name the variable name, should be the same as the function being
     * returned from.
     */
    public ReturnVariableNode(String name) {
        this.name = name;
    }

    /**
     * Gets the name of the variable.
     *
     * @return the name of the variable.
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Returns the name of the variable.
     *
     * @return the name of the variable.
     */
    @Override
    public String toString() {
        return this.name;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param symbols the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable symbols) {
        return this.indentation(level) + "ReturnVariable: \"" + this.name + "\" ofType: " + this.type + "\n";
    }

}
