/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser.syntaxtree;

import parser.SymbolTable;
import parser.Type;

/**
 * Representation of a not statement in the MiniPascal Grammar.
 *
 * @author Matt
 */
public class NegationNode extends ExpressionNode {

    private ExpressionNode expr;

    /**
     * Constructor that takes an expression node representing the quantity being
     * negated. This expression node should be an instance of a relop operation
     * node or another negation node.
     *
     * @param expr the quantity being negated.
     */
    public NegationNode(ExpressionNode expr) {
        this.expr = expr;
    }

    /**
     * Gets the expression that represents the quantity being negated.
     *
     * @return
     */
    public ExpressionNode getExpr() {
        return this.expr;
    }

    /**
     * Returns a string representation of the negation, both the negation and
     * the quantity being negated.
     *
     * @return string representation of the neagtion.
     */
    @Override
    public String toString() {
        return "not (" + this.expr.toString() + ")";
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param symbols the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable symbols) {
        String answer = "";
        answer += this.indentation(level) + "Negation \n";
        if (this.expr instanceof NegationNode) {
            NegationNode negation = (NegationNode) expr;
            answer += negation.indentedToString(level + 1, symbols);
        } else if (this.expr instanceof OperationNode) {
            OperationNode op = (OperationNode) expr;
            answer += op.indentedToString(level + 1, symbols);
        } else {
            answer += indentation(level + 1) + "Not a negation or relation: " + expr.toString();
        }
        return answer;
    }
}
