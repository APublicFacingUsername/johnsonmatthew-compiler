package parser.syntaxtree;

import parser.SymbolTable;

/**
 * Represents an if statement in Pascal. An if statement includes a boolean
 * expression, and two statements.
 *
 * @author Erik Steinmetz
 */
public class IfStatementNode extends StatementNode {

    private ExpressionNode test;
    private StatementNode thenStatement;
    private StatementNode elseStatement;

    /**
     * lacks a specific constructor, so defaults to the most immediate parent
     * object that possesses a constructor, in this case this should be the
     * generic Object class of java.
     */
    /**
     * Get the evaluation expression that determines which branch the if
     * statement should take. This expression should be either a relop operation
     * node or a negation node.
     *
     * @return the evaluation expression of the if statement.
     */
    public ExpressionNode getTest() {
        return test;
    }

    /**
     * Sets the evaluation expression that determines which branch the if
     * statement should take. This expression should be either a relop operation
     * node or a negation node. This assignment is done directly and does not
     * produce a copy of the expression passed in to the method.
     *
     * @param test the evaluation expression of the if statement.
     */
    public void setTest(ExpressionNode test) {
        this.test = test;
    }

    /**
     * Get the statement to branch to if the evaluation passes.
     *
     * @return statement to branch to if the evaluation passes.
     */
    public StatementNode getThenStatement() {
        return thenStatement;
    }

    /**
     * Set the statement to branch to if the evaluation passes.
     *
     * @param thenStatement the statement to branch to if the evaluation fails.
     */
    public void setThenStatement(StatementNode thenStatement) {
        this.thenStatement = thenStatement;
    }

    /**
     * Get the statement to branch to if the evaluation fails.
     *
     * @return the statment to branch to if the evaluation fails
     */
    public StatementNode getElseStatement() {
        return elseStatement;
    }

    /**
     * Set the statement to branch to if the evaluation fails.
     *
     * @param elseStatement the statement to branch to if the evaluation fails.
     */
    public void setElseStatement(StatementNode elseStatement) {
        this.elseStatement = elseStatement;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param table the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable table) {
        String answer = "";
        answer += this.indentation(level) + "If\n";
        answer += this.test.indentedToString(level + 1, table);
        answer += this.indentation(level) + "Then\n";
        answer += this.thenStatement.indentedToString(level + 1, table);
        answer += this.indentation(level) + "Else\n";
        answer += this.elseStatement.indentedToString(level + 1, table);
        return answer;
    }

}
