package parser.syntaxtree;

import java.util.ArrayList;
import parser.SymbolTable;

/**
 * Represents a compound statement in Pascal. A compound statement is a block of
 * zero or more statements to be run sequentially.
 *
 * @author ErikSteinmetz
 */
public class CompoundStatementNode extends StatementNode {

    /**
     * internal representation of the statements, the object for which this is a
     * wrapper class
     */
    private ArrayList<StatementNode> statements;

    /**
     * Constructor that initializes an arraylist to store the statements of this
     * compound statement in.
     */
    public CompoundStatementNode() {
        this.statements = new ArrayList<StatementNode>();
    }

    /**
     * Constructor that takes an arraylist and directly assigns it to the
     * internal representation of the statements in this compound statement.
     *
     * @param statements the statements to
     */
    public CompoundStatementNode(ArrayList<StatementNode> statements) {
        this.statements = statements;
    }

    /**
     * Adds a statement to the internal arraylist that represents the statements
     * in this compound statement.
     *
     * @param state the statement to add to this compound statement.
     */
    public void addStatement(StatementNode state) {
        this.statements.add(state);
    }

    /**
     * Gets the arraylist representation of the statements in this compound
     * statement.
     *
     * @return
     */
    public ArrayList<StatementNode> getStatements() {
        return this.statements;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param table the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    public String indentedToString(int level, SymbolTable table) {
        String answer = this.indentation(level);
        answer += "Compound Statement\n";
        for (StatementNode state : statements) {
            answer += state.indentedToString(level + 1, table);
        }
        return answer;
    }
}
