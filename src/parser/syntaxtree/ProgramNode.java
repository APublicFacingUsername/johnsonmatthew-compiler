package parser.syntaxtree;

import parser.SymbolTable;

/**
 * Represents a Pascal Program
 *
 * @author Erik Steinmetz
 */
public class ProgramNode extends SyntaxTreeNode {

    private String name;
    private DeclarationsNode variables;
    private SubProgramDeclarationsNode functions;
    private CompoundStatementNode main;

    /**
     * Constructor that takes the name of the program
     *
     * @param aName the name of the program
     */
    public ProgramNode(String aName) {
        this.name = aName;
    }

    /**
     * Gets the name of the program
     *
     * @return the name of the program
     */
    public String getName() {
        return this.name;
    }

    /**
     * gets the variables present in the program as declarations node
     *
     * @return the variables present in the program.
     */
    public DeclarationsNode getVariables() {
        return variables;
    }

    /**
     * set the variables of the program. Does not make a copy of the
     * declarations node passed in but rather does a direct assignment of the
     * declarations passed in.
     *
     * @param variables the declarations that we want assigned.
     */
    public void setVariables(DeclarationsNode variables) {
        this.variables = variables;
    }

    /**
     * get the functions of the program.
     *
     * @return the functions of the program as
     * {@code SubProgramDeclarationsNode}
     */
    public SubProgramDeclarationsNode getFunctions() {
        return functions;
    }

    /**
     * Sets the functions of the program. This does not make a copy of the
     * SubProgramDeclarationsNode passed in but rather performs a direct
     * assignment of the declarations passed in.
     *
     * @param functions the functions to be assigned as
     * {@code SubProgramDeclarationsNode}
     */
    public void setFunctions(SubProgramDeclarationsNode functions) {
        this.functions = functions;
    }

    /**
     * Gets the main set of statements for the program, the statements that are
     * actually run.
     *
     * @return the compound statement that constitutes the main function of the
     * program.
     */
    public CompoundStatementNode getMain() {
        return main;
    }

    /**
     * Sets the main set of statements for the program, the statements that are
     * actually run. This does not perform a copy of the CompoundStatementNode
     * passed in, but rather does a direct assignment of the passed in node to
     * the internal representation of the main function.
     *
     * @param main
     */
    public void setMain(CompoundStatementNode main) {
        this.main = main;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param table the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable table) {
        String answer = this.indentation(level);
        answer += "Program: " + name + "\n";
        answer += variables.indentedToString(level + 1, table);
        answer += functions.indentedToString(level + 1, table);
        answer += main.indentedToString(level + 1, table);
        return answer;
    }
}
