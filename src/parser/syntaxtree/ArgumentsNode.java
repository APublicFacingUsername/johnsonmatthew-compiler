/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser.syntaxtree;

import java.util.ArrayList;
import parser.SymbolTable;

/**
 * Object that represents the arguments for a given function. Is used only by
 * the {@code SubProgramNode} classes, the call nodes utilize the ArrayList
 * object of type VariableNode without this wrapper class.
 *
 * @author Matt
 */
public class ArgumentsNode extends SyntaxTreeNode {

    /**
     * arrayList representation of the variables.
     */
    private ArrayList<VariableNode> args;

    /**
     * Constructor that initializes an empty arraylist of the type variablenode
     */
    public ArgumentsNode() {
        this.args = new ArrayList<VariableNode>();
    }

    /**
     * Add a variable to the declaration node.
     *
     * @param aVariable A VariableNode representation of the variable.
     */
    public void addVariable(VariableNode aVariable) {
        args.add(aVariable);
    }

    /**
     * Gets an arraylist representation of the variables within this declaration
     * node
     *
     * @return ArrayList List of variable nodes in this declaration.
     */
    public ArrayList<VariableNode> getArgs() {
        return this.args;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param table the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable table) {
        String answer = this.indentation(level);
        answer += "Arguments\n";
        for (VariableNode variable : args) {
            answer += variable.indentedToString(level + 1, table);
        }
        return answer;
    }
}
