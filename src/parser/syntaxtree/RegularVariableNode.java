/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser.syntaxtree;

import java.util.Objects;
import parser.SymbolTable;
import parser.Type;

/**
 * A regular variable node, present to distinguish simple variables from
 * variables that are present either as return values, arrays, or array
 * indicies.
 *
 * @author Matt
 */
public class RegularVariableNode extends VariableNode {

    private String name;

    /**
     * Creates a RegularVariableNode with the given name.
     *
     * @param name The attribute for this node.
     */
    public RegularVariableNode(String name, SymbolTable table) {
        this.name = name;
        this.type = table.getType(name);
    }

    /**
     * Creates a RegularVariableNode with the given name and type.
     *
     * @param name the attribute
     * @param type the type
     */
    public RegularVariableNode(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    /**
     * Returns the name of the RegularVariable of this node.
     *
     * @return String The name of this RegularVariableNode.
     */
    @Override
    public String getName() {
        return (this.name);
    }

    /**
     * Returns the name of the variable as the description of this node.
     *
     * @return The attribute String of this node.
     */
    @Override
    public String toString() {
        return (name);
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param table the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable table) {
        String answer = this.indentation(level);
        if (this.intToReal) {
            answer += "Variable: \"" + this.name + "\" ofContextualType: " + this.type.toString() + "\"\n";
        } else if (this.type != null) {
            answer += "Variable: \"" + this.name + "\" ofType: " + this.type.toString() + "\n";
        } else {
            answer += "Variable: \"" + this.name + "\" ofType: " + table.getType(this.name) + "\n";
        }
        return answer;
    }
}
