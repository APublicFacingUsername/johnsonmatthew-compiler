/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser.syntaxtree;

import parser.SymbolTable;
import parser.Type;

/**
 *
 * @author Matt
 */
public class FunctionNode extends SubProgramNode {

    private String name;
    private Type returnType;
    private int returnBounds[];
    private ArgumentsNode args;
    private DeclarationsNode variables;
    private SubProgramDeclarationsNode functions;
    private CompoundStatementNode body;
    private ExpressionNode returnExpr;

    /**
     * Constructor that takes the name of the function to be represented by this
     * node.
     *
     * @param name the name of the function to be represented by this node.
     */
    public FunctionNode(String name) {
        this.name = name;
    }

    /**
     * Set the return type of this function.
     *
     * @param returnType The type of value that is returned from this function
     * @param bounds Optional argument to specify the bounds of an array that
     * might be returned from this argument.
     */
    public void setReturnType(Type returnType, int... bounds) {
        this.returnType = returnType;
        if (bounds != null && (returnType == Type.INTEGER_ARRAY || returnType == Type.REAL_ARRAY)) {
            if (bounds.length == 2) {
                returnBounds = new int[]{bounds[0], bounds[1]};
            }
        }
    }

    /**
     * Set the expression that represents the return value of this function.
     *
     * @param returnExpr The expression that represents the return value of this
     * function.
     */
    public void setReturnExpr(ExpressionNode returnExpr) {
        this.returnExpr = returnExpr;
    }

    /**
     * Set the arguments for the subprogram
     *
     * @param args arguments as an argument node
     */
    @Override
    public void setArguments(ArgumentsNode args) {
        this.args = args;
    }

    /**
     * Sets the declarations for the subprogram
     *
     * @param variables declarations as declarations node
     */
    @Override
    public void setDeclarations(DeclarationsNode variables) {
        this.variables = variables;
    }

    /**
     * sets the sub-subprogram declarations for the subprogram
     *
     * @param functions a list of sub-subprograms as
     * {@code SubProgramDeclarationsNode}
     */
    @Override
    public void setSubProgramDeclarations(SubProgramDeclarationsNode functions) {
        this.functions = functions;
    }

    /**
     * sets the compound statement for the subprogram
     *
     * @param body the "main" of the subprogram, the code actually executed as
     * compound statement node.
     */
    @Override
    public void setCompoundStatement(CompoundStatementNode body) {
        this.body = body;
    }

    /**
     * gets the name of the subprogram
     *
     * @return the name of the subprogram
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Get the return type of this function.
     *
     * @return the return type of this function.
     */
    public Type getReturnType() {
        return this.returnType;
    }

    /**
     * Return a pair of integer bounds as an array of integers. These bounds
     * represent the upper and lower bounds of an array that may or may not be
     * returned from this function based on how the function node has been
     * modified.
     *
     * @return Pair of integers representing the bounds of the return array.
     */
    public int[] getReturnBounds() {
        return this.returnBounds;
    }

    /**
     * Get the return expression for this function.
     *
     * @return the expression being returned from this function.
     */
    public ExpressionNode getReturnExpr() {
        return this.returnExpr;
    }

    /**
     * Gets the arguments of the subprogram
     *
     * @return the arguments of the subprogram as arguments node
     */
    @Override
    public ArgumentsNode getArgs() {
        return this.args;
    }

    /**
     * Gets the declarations of the subprogram
     *
     * @return the declarations of the subprogram as declarations node
     */
    @Override
    public DeclarationsNode getDeclarations() {
        return this.variables;
    }

    /**
     * Gets the sub-subprogram declarations of the subprogram
     *
     * @return the sub-subprogram declarations of the subprogram as
     * {@code SubProgramDeclarationsNode}
     */
    @Override
    public SubProgramDeclarationsNode getSubProgramDeclarations() {
        return this.functions;
    }

    /**
     * Gets the compound statement of the subprogram, the statements that
     * actually get executed.
     *
     * @return the compound statement of the subprogram.
     */
    @Override
    public CompoundStatementNode getCompoundStatement() {
        return this.body;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param table the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable table) {
        String answer = this.indentation(level);
        answer += "Function: " + name + "\n";
        answer += args.indentedToString(level + 1, table);
        String rType = "";
        if (returnType == Type.INTEGER_ARRAY || returnType == Type.REAL_ARRAY) {
            rType += returnType + "[" + returnBounds[0] + ":" + returnBounds[1] + "]";
        } else {
            rType += returnType;
        }
        answer += this.indentation(level + 1) + "Returns: " + rType + "\n";
        answer += this.indentation(level + 1) + "ReturnExpression \n" + this.returnExpr.indentedToString(level + 2, table);
        answer += variables.indentedToString(level + 1, table);
        answer += functions.indentedToString(level + 1, table);
        answer += body.indentedToString(level + 1, table);
        return answer;
    }
}
