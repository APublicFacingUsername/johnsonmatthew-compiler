package parser.syntaxtree;

import java.util.ArrayList;
import parser.SymbolTable;

/**
 * Represents a collection of subprogram declarations
 *
 * @author Erik Steinmetz
 */
public class SubProgramDeclarationsNode extends SyntaxTreeNode {

    private ArrayList<SubProgramNode> procs = new ArrayList<SubProgramNode>();

    /**
     * Adds a subprogram to the set of subprograms
     *
     * @param aSubProgram the subprogram to be added
     */
    public void addSubProgramDeclaration(SubProgramNode aSubProgram) {
        procs.add(aSubProgram);
    }

    /**
     * Returns the set of subprograms this node represents
     *
     * @return An ArrayList of SubPrograms.
     */
    public ArrayList<SubProgramNode> getSubProgams() {
        return this.procs;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param table the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    public String indentedToString(int level, SymbolTable table) {
        String answer = this.indentation(level);
        answer += "SubProgramDeclarations\n";
        for (SubProgramNode subProg : procs) {
            table.setTableScope(subProg.getName());
            answer += subProg.indentedToString(level + 1, table);
            table.setTableScope(table.getParentScope());
        }
        return answer;
    }
}
