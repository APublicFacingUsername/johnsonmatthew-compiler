/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser.syntaxtree;

import java.util.Objects;
import parser.SymbolTable;
import parser.Type;

/**
 * This is a class to represent an Array Variable in its entirety. This is done
 * by specifying the name of the array along with its lower and upper bounds.
 *
 * @author Matt
 */
public class ArrayVariableNode extends VariableNode {

    private final int lowerBound;
    private final int upperBound;
    private String name;

    /**
     * Constructor that takes the given name, the upper and lower bounds of the
     * array, and the array type and puts this information into the according
     * elements of this object. The bounds are represented internally as the
     * primitive ints of the java programming language.
     *
     * @param name The name of the array
     * @param lowerBound The lower bound of the array, an integer represented as
     * a string
     * @param upperBound The upper bound of the array, an integer represented as
     * a string
     * @param arrayType The type of the array.
     */
    public ArrayVariableNode(String name, String lowerBound, String upperBound, Type arrayType) {
        this.name = name;
        this.lowerBound = Integer.parseInt(lowerBound);
        this.upperBound = Integer.parseInt(upperBound);
        if (arrayType == Type.INTEGER_ARRAY || arrayType == Type.INTEGER) {
            this.type = Type.INTEGER_ARRAY;
        } else if (arrayType == Type.REAL_ARRAY || arrayType == Type.REAL) {
            this.type = Type.REAL_ARRAY;
        }
    }

    /**
     * Gets the lower bound of the array, as integer.
     *
     * @return the lower bound of the array.
     */
    public int getLowerBound() {
        return this.lowerBound;
    }

    /**
     * Gets the upper bound of the array, as integer.
     *
     * @return the upper bound of the array.
     */
    public int getUpperBound() {
        return this.upperBound;
    }

    /**
     * Gets the name of the array
     *
     * @return the name of the array.
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Returns the name of the variable as the description of this node.
     *
     * @return The attribute String of this node.
     */
    @Override
    public String toString() {
        return (name);
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param symbols the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable symbols) {
        String answer = this.indentation(level);
        answer += "Array: \"" + this.name + "[" + lowerBound + ":" + upperBound + "]\""
                + " ofType: " + this.type.toString() + "\n";
        return answer;
    }
}
