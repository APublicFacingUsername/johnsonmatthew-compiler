package parser.syntaxtree;

import parser.SymbolTable;

/**
 * Represents a single assignment statement. The internal structure of this
 * class is a variable node that is being assigned to and an expression node
 * that represents the value that we are assigning to the variable.
 *
 * @author Erik Steinmetz
 */
public class AssignmentStatementNode extends StatementNode {

    protected VariableNode lvalue;
    protected ExpressionNode expression;

    /**
     * Constructor that takes a variable node that we are assigning to and an
     * expression that we are assigning and ties them to the corresponding
     * internal values.
     *
     * @param lvalue The variable being assigned to
     * @param expression The expression being assigned
     */
    public AssignmentStatementNode(VariableNode lvalue, ExpressionNode expression) {
        this.lvalue = lvalue;
        this.expression = expression;
    }

    /**
     * Gets the variable that we are assigning to
     *
     * @return the variable being assigned to
     */
    public VariableNode getLValue() {
        return lvalue;
    }

    /**
     * Sets the variable that we are assigning to
     *
     * @param lvalue the variable being assigned to
     */
    public void setLValue(VariableNode lvalue) {
        this.lvalue = lvalue;
    }

    /**
     * Gets the expression that we are assigning
     *
     * @return the expression being assigned
     */
    public ExpressionNode getExpression() {
        return expression;
    }

    /**
     * Sets the expression that we are assigning
     *
     * @param expression the expression being assigned
     */
    public void setExpression(ExpressionNode expression) {
        this.expression = expression;
    }

    /**
     * Returns a string representation of this assignment, including the
     * variable, the MiniPascal assignop sign, and the expression being assigned
     * in full.
     *
     * @return String representation of the assignment.
     */
    @Override
    public String toString() {
        String answer = "";
        answer += this.lvalue.toString();
        answer += " := ";
        answer += this.expression.toString();
        return answer;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param table the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable table) {
        String answer = this.indentation(level);
        answer += "Assignment\n";
        answer += this.lvalue.indentedToString(level + 1, table);
        answer += this.expression.indentedToString(level + 1, table);
        return answer;
    }
}
