package parser.syntaxtree;

/**
 * Represents a single statement in Pascal.
 *
 * @author Erik Steinmetz
 */
public abstract class StatementNode extends SyntaxTreeNode {
    /**
     * No classes to document, hoorah!
     */
}
