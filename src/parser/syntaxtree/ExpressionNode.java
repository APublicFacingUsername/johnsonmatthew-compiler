package parser.syntaxtree;

import parser.Type;

/**
 * General representation of any expression.
 *
 * @author erik
 */
public abstract class ExpressionNode extends SyntaxTreeNode {

    protected Type type;
    protected boolean intToReal = false;
    protected String register;

    /**
     * get the register allocated to this expression
     *
     * @return the register allocated to this expression
     */
    public String getRegister() {
        return this.register;
    }

    /**
     * set the register allocated to this expression
     *
     * @param register the register allocated to this expression
     */
    public void setRegister(String register) {
        this.register = register;
    }

    /**
     * Check if the expression has been casted from integer to real
     *
     * @return true if the expression has been casted from integer to real,
     * false otherwise
     */
    public boolean intToReal() {
        return this.intToReal;
    }

    /**
     * Get the type that is assigned to this expression
     *
     * @return the type assigned to his expression
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Set the type that is assigned to this expression
     *
     * @param type the type to set to this expression.
     */
    public void setType(Type type) {
        if ((this.type == Type.INTEGER && type == Type.REAL) || (this.type == Type.INTEGER_ARRAY && type == Type.REAL_ARRAY)) {
            this.intToReal = true;
        }
        this.type = type;
    }
}
