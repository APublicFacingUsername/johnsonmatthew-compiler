/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.syntaxtree;

import parser.SymbolTable;

/**
 * Class to represent a while statement in the Mini Pascal language. Uses an
 * expression node to represent the evaluation that determines if the loop runs
 * and a statement node to represent the operation(s) performed each time the
 * construct loops.
 *
 * @author omega
 */
public class WhileStatementNode extends StatementNode {

    ExpressionNode expression;
    StatementNode statement;

    /**
     * Constructor that takes an ExpressionNode and a StatementNode and binds
     * them together in this object.
     *
     * @param expression The expression that is used to determine if the loop
     * runs
     * @param statement The statement that represents the operations of the loop
     */
    public WhileStatementNode(ExpressionNode expression, StatementNode statement) {
        this.expression = expression;
        this.statement = statement;
    }

    /**
     * Gets the expression node that represents the determinant of the loop.
     *
     * @return An expression node representing the loop determinant.
     */
    public ExpressionNode getExpression() {
        return this.expression;
    }

    /**
     * Gets the statement node that represents the operations contained in the
     * loop.
     *
     * @return A statement node representing the operations of the loop.
     */
    public StatementNode getStatement() {
        return this.statement;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param symbolTable the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable symbolTable) {
        String returnString = this.indentation(level) + "While Statement\n";
        returnString += expression.indentedToString(level + 1, symbolTable);
        returnString += statement.indentedToString(level + 1, symbolTable);
        return returnString;
    }
}
