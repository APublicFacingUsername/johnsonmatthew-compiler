/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser.syntaxtree;

import parser.SymbolTable;
import parser.Type;

/**
 * Represents a value or number in an expression.
 *
 * @author Erik Steinmetz
 */
public class ValueNode extends ExpressionNode {

    /**
     * The attribute associated with this node.
     */
    private String attribute;

    /**
     * Creates a ValueNode with the given attribute.
     *
     * @param attr The attribute for this value node.
     */
    public ValueNode(String attr) {
        this.attribute = attr;
        if (attr.contains(".") || attr.contains("E")) {
            this.type = Type.REAL;
        } else {
            this.type = Type.INTEGER;
        }
    }

    /**
     * Returns the attribute of this node.
     *
     * @return The attribute of this ValueNode.
     */
    public String getAttribute() {
        return (this.attribute);
    }

    /**
     * Returns the attribute as the description of this node.
     *
     * @return The attribute String of this node.
     */
    @Override
    public String toString() {
        return (attribute);
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param table the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable table) {
        String answer = this.indentation(level);
        if (intToReal) {
            answer += "Value: \"" + this.attribute + "\" ofContextualType: " + this.type.toString() + "\n";
        } else {
            answer += "Value: \"" + this.attribute + "\" ofType: " + this.type.toString() + "\n";
        }
        return answer;
    }

    /**
     * Compares this object to another, returning true if the other is also a
     * value node and has the same attribute as this node. (I never used this
     * one, if memory serves :)
     *
     * @param o The object to compare to
     * @return True if the object has same attribute, false if not.
     */
    @Override
    public boolean equals(Object o) {
        boolean answer = false;
        if (o instanceof ValueNode) {
            ValueNode other = (ValueNode) o;
            if (this.attribute.equals(other.attribute)) {
                answer = true;
            }
        }
        return answer;
    }
}
