package parser.syntaxtree;

import parser.SymbolTable;
import parser.Type;
import scanner.TokenType;

/**
 * Represents any operation in an expression.
 *
 * @author Erik Steinmetz
 */
public class OperationNode extends ExpressionNode {

    /**
     * The left operator of this operation.
     */
    private ExpressionNode left;
    private String leftRegister;

    /**
     * The right operator of this operation.
     */
    private ExpressionNode right;
    private String rightRegister;

    /**
     * The type of operation.
     */
    private TokenType opType;

    /**
     * The specific operation
     */
    private String operation;

    private String storeRegister;

    /**
     * Creates an operation node given an operation token.
     *
     * @param opType The TokenType of the operation (mulop, addop, relop)
     * @param operation The operation as a lexeme
     */
    public OperationNode(TokenType opType, String operation) {
        this.opType = opType;
        this.operation = operation;
    }

    /**
     * Gets the register assigned to the right hand operand
     *
     * @return register assigned to the right hand operand
     */
    public String getRightReg() {
        return this.rightRegister;
    }

    /**
     * Gets the register assigned to the left hand operand
     *
     * @return register assigned to the left hand operand
     */
    public String getLeftReg() {
        return this.leftRegister;
    }

    /**
     * get the register assigned to the result of this operation.
     *
     * @return the register assigned to the result of this operation
     */
    public String getStoreReg() {
        return this.storeRegister;
    }

    /**
     * Get the left hand operand as expression
     *
     * @return the left hand expression
     */
    public ExpressionNode getLeft() {
        return (this.left);
    }

    /**
     * Get the right hand operand as expression
     *
     * @return the right hand expression
     */
    public ExpressionNode getRight() {
        return (this.right);
    }

    /**
     * Get the general type of operation being performed as its token type
     *
     * @return the operation being performed
     */
    public TokenType getOpType() {
        return (this.opType);
    }

    /**
     * Get the specific operation being performed as string.
     *
     * @return the specific operation being performed
     */
    public String getOperation() {
        return (this.operation);
    }

    /**
     * Sets the left hand operand by direct assignment.
     *
     * @param node the expression to be placed on left hand of operation.
     */
    public void setLeft(ExpressionNode node) {
        this.left = node;
    }

    /**
     * Sets the right hand operand by direct assignment.
     *
     * @param node the expression to be placed on the right of operation.
     */
    public void setRight(ExpressionNode node) {
        this.right = node;
    }

    /**
     * Sets the general operation type of the operation as tokentype
     *
     * @param opType the general type of the operation.
     */
    public void setOpType(TokenType opType) {
        this.opType = opType;
    }

    /**
     * Sets the specific operation as String
     *
     * @param operation the specific operation as String
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }

    /**
     * Sets the register allocated to the right hand operand
     *
     * @param rightRegister String representation of the register allocated
     */
    public void setRightReg(String rightRegister) {
        this.rightRegister = rightRegister;
    }

    /**
     * Sets the register allocated to the left hand operand
     *
     * @param leftRegister String representation of the register allocated
     */
    public void setLeftReg(String leftRegister) {
        this.leftRegister = leftRegister;
    }

    /**
     * Sets the register allocated to the result of this operation
     *
     * @param storeRegister the register allocated to the result of this
     * operation.
     */
    public void setStoreReg(String storeRegister) {
        this.storeRegister = storeRegister;
        this.register = storeRegister;
    }

    /**
     * Returns the operation token as a String.
     *
     * @return The String version of the operation token.
     */
    @Override
    public String toString() {
        return this.left.toString() + " " + this.operation + " " + this.right.toString();
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param table the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable table) {
        String answer = this.indentation(level);
        answer += "Operation: \"" + this.operation + "\"";
        if (this.intToReal) {
            answer += " ofContextualType: " + this.type.toString() + "\n";
        } else if (this.type != null) {
            answer += " ofType: " + this.type.toString() + "\n";
        } else {
            answer += "\n";
        }
        answer += this.indentation(level + 1) + "LVal:\n";
        answer += left.indentedToString(level + 1, table);
        answer += this.indentation(level + 1) + "RVal:\n";
        answer += right.indentedToString(level + 1, table);
        return (answer);
    }

    /**
     * Compares this operation with another object. If the other is also an
     * operation and the left and right hand operands match as well as the
     * operation itself, returns true.
     *
     * @param o The object to compare to.
     * @return True if the object match as operations, false if otherwise
     */
    @Override
    public boolean equals(Object o) {
        boolean answer = false;
        if (o instanceof OperationNode) {
            OperationNode other = (OperationNode) o;
            if ((this.operation.equals(other.operation))
                    && this.left.equals(other.left)
                    && this.right.equals(other.right)) {
                answer = true;
            }
        }
        return answer;
    }
}
