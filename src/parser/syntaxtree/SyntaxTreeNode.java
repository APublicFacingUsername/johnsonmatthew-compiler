package parser.syntaxtree;

import parser.SymbolTable;

/**
 * The base class for all nodes in a syntax tree.
 *
 * @author Erik Steinmetz
 */
public abstract class SyntaxTreeNode {

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param symbols the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    public abstract String indentedToString(int level, SymbolTable symbols);

    /**
     * Creates an indentation String for the indentedToString.
     *
     * @param level The amount of indentation.
     * @return A String displaying the given amount of indentation.
     */
    protected String indentation(int level) {
        String answer = "";
        if (level > 0) {
            answer = "|-- ";
        }
        for (int indent = 1; indent < level; indent++) {
            answer += "--- ";
        }
        return (answer);
    }

}
