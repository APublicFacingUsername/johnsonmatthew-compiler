package parser.syntaxtree;

import java.util.ArrayList;
import parser.SymbolTable;

/**
 * Represents a set of declarations in a Pascal program.
 *
 * @author Erik Steinmetz
 */
public class DeclarationsNode extends SyntaxTreeNode {

    /**
     * arrayList representation of the variables.
     */
    private ArrayList<VariableNode> vars;

    /**
     * Constructor that initializes an the internal arraylist of this function.
     */
    public DeclarationsNode() {
        this.vars = new ArrayList<VariableNode>();
    }

    /**
     * Add a variable to the declaration node.
     *
     * @param aVariable A VariableNode representation of the variable.
     */
    public void addVariable(VariableNode aVariable) {
        vars.add(aVariable);
    }

    /**
     * Set the variables of this declaration node by a direct reference to the
     * ArrayList that is passed into this method. Does not create copy of the
     * arraylist, assigns by direct reference.
     *
     * @param vars the arraylist to assign to this declarations node.
     */
    public void setVars(ArrayList<VariableNode> vars) {
        this.vars = vars;
    }

    /**
     * Gets an arraylist representation of the variables within this declaration
     * node
     *
     * @return ArrayList List of variable nodes in this declaration.
     */
    public ArrayList<VariableNode> getVars() {
        return this.vars;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param table the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    public String indentedToString(int level, SymbolTable table) {
        String answer = this.indentation(level);
        answer += "Declarations\n";
        for (VariableNode variable : vars) {
            answer += variable.indentedToString(level + 1, table);
        }
        return answer;
    }
}
