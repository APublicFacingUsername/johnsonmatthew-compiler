/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser.syntaxtree;

import parser.Type;

/**
 * Abstract class to represent a sub program. Not meant to be instantiated.
 *
 * @author Matt
 */
public abstract class SubProgramNode extends SyntaxTreeNode {

    /**
     * Set the arguments for the subprogram
     *
     * @param args arguments as an argument node
     */
    public abstract void setArguments(ArgumentsNode args);

    /**
     * Sets the declarations for the subprogram
     *
     * @param variables declarations as declarations node
     */
    public abstract void setDeclarations(DeclarationsNode variables);

    /**
     * sets the sub-subprogram declarations for the subprogram
     *
     * @param functions a list of sub-subprograms as
     * {@code SubProgramDeclarationsNode}
     */
    public abstract void setSubProgramDeclarations(SubProgramDeclarationsNode functions);

    /**
     * sets the compound statement for the subprogram
     *
     * @param body the "main" of the subprogram, the code actually executed as
     * compound statement node.
     */
    public abstract void setCompoundStatement(CompoundStatementNode body);

    /**
     * gets the name of the subprogram
     *
     * @return the name of the subprogram
     */
    public abstract String getName();

    /**
     * Gets the arguments of the subprogram
     *
     * @return the arguments of the subprogram as arguments node
     */
    public abstract ArgumentsNode getArgs();

    /**
     * Gets the declarations of the subprogram
     *
     * @return the declarations of the subprogram as declarations node
     */
    public abstract DeclarationsNode getDeclarations();

    /**
     * Gets the sub-subprogram declarations of the subprogram
     *
     * @return the sub-subprogram declarations of the subprogram as
     * {@code SubProgramDeclarationsNode}
     */
    public abstract SubProgramDeclarationsNode getSubProgramDeclarations();

    /**
     * Gets the compound statement of the subprogram, the statements that
     * actually get executed.
     *
     * @return the compound statement of the subprogram.
     */
    public abstract CompoundStatementNode getCompoundStatement();
}
