/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser.syntaxtree;

import parser.SymbolTable;

/**
 * A representation of a write operation. This operation is to write to the
 * console a specified value.
 *
 * @author Matt
 */
public class WriteNode extends StatementNode {

    private ExpressionNode exprToWrite;

    /**
     * Constructor that takes the expression to be written.
     *
     * @param exprToWrite the expression to write.
     */
    public WriteNode(ExpressionNode exprToWrite) {
        this.exprToWrite = exprToWrite;
    }

    /**
     * Gets the expression that is to be written.
     *
     * @return the expression to write as ExpressionNode
     */
    public ExpressionNode getExprToWrite() {
        return this.exprToWrite;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param symbols the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable symbols) {
        String answer = "";
        answer += this.indentation(level) + "Write Command \n";
        answer += this.indentation(level + 1) + "Expression: " + this.exprToWrite.toString() + "\n";
        return answer;
    }

}
