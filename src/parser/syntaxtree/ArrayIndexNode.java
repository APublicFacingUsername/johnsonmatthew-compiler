/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser.syntaxtree;

import java.util.Objects;
import parser.SymbolTable;
import parser.Type;

/**
 * This is a class to represent the individual value of a given element of an
 * array. This is accomplished by specifying the array object that represents
 * the larger array in which this index exists, specifying the offset from the
 * beginning of that array, and giving the index a name containing both the
 * array name and the index value.
 *
 * @author Matt
 */
public class ArrayIndexNode extends VariableNode {

    private ArrayVariableNode array;
    private ExpressionNode offset;
    private String name;

    /**
     * Constructor that takes the source array, the offset or index as an
     * expression node, and the SymbolTable that corresponds to the syntax tree
     * this node is located in. Assumes that the symbol table has its scope set
     * appropriately to where ever this index is found.
     *
     * @param array The array in which this index exists
     * @param index The offset or index of the value that we are specifying
     * @param symbols The symbol table that corresponds to this node's syntax
     * tree.
     */
    public ArrayIndexNode(ArrayVariableNode array, ExpressionNode index, SymbolTable symbols) {
        this.name = array.toString() + "[" + index.toString() + "]";
        this.array = array;
        this.offset = index;
        index.setType(Type.INTEGER);
        if (array.getType() == Type.INTEGER_ARRAY) {
            this.type = Type.INTEGER;
        } else if (array.getType() == Type.REAL_ARRAY) {
            this.type = Type.REAL;
        }
    }

    /**
     * Gets the array, as an ArrayVariableNode, in which this index resides.
     *
     * @return The array that contains this index.
     */
    public ArrayVariableNode getArrayRef() {
        return this.array;
    }

    /**
     * Gets the offset of this array as an expression node.
     *
     * @return The offset or index of this array, as expression node.
     */
    public ExpressionNode getOffset() {
        return this.offset;
    }

    /**
     * Returns the name of this array, including its index.
     *
     * @return The name and the index of the array.
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Returns the name of this array, including its index.
     *
     * @return The name and index of the array.
     */
    @Override
    public String toString() {
        return this.name;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param symbols the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable symbols) {
        String answer = this.indentation(level);
        if (this.intToReal) {
            answer += "ArrayIndex: \"" + this.name + "\" ofContextualType: \"" + this.type.toString() + "\"\n";
        } else {
            answer += "ArrayIndex: \"" + this.name + "\" ofType: \"" + this.type.toString() + "\"\n";
        }
        return answer;
    }
}
