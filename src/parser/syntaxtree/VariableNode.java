package parser.syntaxtree;

/**
 * Represents a variable in the syntax tree. This class is abstract and not
 * meant to be instantiated.
 *
 * @author MJohnson
 */
public abstract class VariableNode extends ExpressionNode {

    /**
     * Gets the name of the variable.
     *
     * @return the name of the variable.
     */
    public abstract String getName();
}
