/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser.syntaxtree;

import parser.SymbolTable;
import parser.Type;

/**
 * This is a node that represents a read operation in the MiniPascal grammar.
 * Its function is to record what variable a read operation ought be assigned
 * to, along with the type of variable that is being read.
 *
 * @author Matt
 */
public class ReadNode extends AssignmentStatementNode {

    private Type type;

    /**
     * Basic constructor for a read node.
     *
     * @param lvalue The variable that we are assigning to
     * @param expression An expression that is never used as the expression is
     * found post-compile.
     */
    public ReadNode(VariableNode lvalue, ExpressionNode expression) {
        super(lvalue, expression);
    }

    /**
     * Get and set the type of variable that this node corresponds to
     */
    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return this.type;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param symbols the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable symbols) {
        String answer = "";
        answer += this.indentation(level) + "Read Command " + "\n";
        answer += this.indentation(level + 1) + this.lvalue.toString() + ":=" + "Hard Determinism" + "\n";
        return answer;
    }
}
