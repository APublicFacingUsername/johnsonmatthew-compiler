/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser.syntaxtree;

import java.util.ArrayList;
import parser.SymbolTable;
import parser.Type;

/**
 * A representation of a call node that exists as an expression, perhaps as part
 * of a series of operations. This node may only represent what are specified as
 * functions in the MiniPascal language, as procedures lack the return type
 * necessary to be present in an expression.
 *
 * @author Matt
 */
public class CallNodeAsExpression extends ExpressionNode {

    private String functionName;
    private FunctionNode function;
    private ArrayList<ExpressionNode> args;

    /**
     * Constructor that takes the name of the function and the symbol table with
     * its scope set accordingly. Initializes a number of internal
     * representations, including the name, the function node that is being
     * referred to, the return type of the function, and an empty set of
     * arguments.
     *
     * @param functionName The name of the function being called
     * @param table The symbol table with its scope set to the caller's scope.
     */
    public CallNodeAsExpression(String functionName, SymbolTable table) {
        this.functionName = functionName;
        this.function = (FunctionNode) table.getObjectReference(functionName);
        this.type = function.getReturnType();
        this.args = new ArrayList<ExpressionNode>();
    }

    /**
     * Gets the arraylist representation of the arguments contained in this call
     * node.
     *
     * @return the arguments as expressions.
     */
    public ArrayList<ExpressionNode> getArgs() {
        return this.args;
    }

    /**
     * Sets the arraylist representation of the arguments to the arraylist that
     * is provided. Does not perform a copy of the array but rather gives a
     * direct reference to the arraylist passed in.
     *
     * @param args The arraylist representation of the arguments
     */
    public void setArgs(ArrayList<ExpressionNode> args) {
        this.args = args;
    }

    /**
     * Gets the {@code FunctionNode} that this call node plans to attempt to
     * jump to.
     *
     * @return The function referenced.
     */
    public FunctionNode getProcOrFunc() {
        return this.function;
    }

    /**
     * Gets the name of the function that is referenced.
     *
     * @return name of the function referenced.
     */
    public String getName() {
        return this.functionName;
    }

    /**
     * Returns the name of the function that is referenced.
     *
     * @return name of the function referenced.
     */
    @Override
    public String toString() {
        return this.functionName;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param symbols the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable symbols) {
        String answer = "";
        answer += this.indentation(level) + "Function call \n";
        answer += this.indentation(level + 1) + "Function name: " + this.functionName + "\n";
        if (this.intToReal) {
            answer += this.indentation(level + 1) + "Contextual type: " + this.type.toString() + "\n";
        } else if (this.type != null) {
            answer += this.indentation(level + 1) + "Type: " + this.type + "\n";
        }
        String sArgs = "";
        for (ExpressionNode expr : args) {
            sArgs += expr.indentedToString(level + 1, symbols);
        }
        answer += this.indentation(level + 1) + "With Args: \n" + sArgs;
        return answer;
    }
}
