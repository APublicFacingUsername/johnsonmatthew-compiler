/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser.syntaxtree;

import java.util.ArrayList;
import parser.SymbolTable;

/**
 * A representation of a call node as it exists as a statement. These nodes are
 * intended to stand alone from any variety of operations and not to have the
 * ability to be placed as part of an assignment statement.
 *
 * @author Matt
 */
public class CallNodeAsStatement extends StatementNode {

    private String functionName;
    private SubProgramNode pf;
    private ArrayList<ExpressionNode> args;

    /**
     * Creates a new instance of this object, taking the function name
     * referenced and the symbol table corresponding to this node's larger
     * syntax tree with that table's scope set to the scope of the calling
     * function.
     *
     * @param functionName Name of the function to be called
     * @param table Symbol table corresponding to this node's larger syntax tree
     */
    public CallNodeAsStatement(String functionName, SymbolTable table) {
        this.functionName = functionName;
        this.pf = (SubProgramNode) table.getObjectReference(functionName);
        this.args = new ArrayList<ExpressionNode>();
    }

    /**
     * Sets the arguments for this function. Does not perform a copy of the
     * ArrayList passed in, but rather does a direct assignment of the arraylist
     * to the arraylist internal to this class.
     *
     * @param args Arraylist representation of the arguments of this func or
     * proc call.
     */
    public void setArgs(ArrayList<ExpressionNode> args) {
        this.args = args;
    }

    /**
     * Gets the arguments for this function, represented as an arraylist of
     * expressions.
     *
     * @return The arguments for this func or proc call.
     */
    public ArrayList<ExpressionNode> getArgs() {
        return this.args;
    }

    /**
     * The name of the function or procedure that is being called.
     *
     * @return the name of the function or procedure being called.
     */
    public String getName() {
        return this.functionName;
    }

    /**
     * Return the name of the function or procedure that is being called.
     *
     * @return the name of the function or procedure being called.
     */
    @Override
    public String toString() {
        return this.functionName;
    }

    /**
     * Gets the procedure or function node that this call node corresponds to.
     *
     * @return The procedure or function node that this call node corresponds
     * to.
     */
    public SubProgramNode getProcOrFunc() {
        return this.pf;
    }

    /**
     * Returns a string representation of this node, indented by a specified
     * number of 4 character tab breaks. The function assumes that the symbol
     * table passed in will have its scope set to the appropriate value, ex if
     * this node exists as part of the main program the table would have its
     * scope set to the main program.
     *
     * @param level the number of indentations to make, total of level * 4
     * spaces of indentation.
     * @param symbols the symbol table that corresponds to the syntax tree in
     * which this node exists.
     * @return A string representation of the node, nicely formatted.
     */
    @Override
    public String indentedToString(int level, SymbolTable symbols) {
        String answer = "";
        answer += this.indentation(level) + "Procedure or Function call \n";
        answer += this.indentation(level + 1) + "Procedure name: " + this.pf.getName() + "\n";
        String sArgs = "";
        for (ExpressionNode expr : args) {
            sArgs += expr.indentedToString(level + 1, symbols);
        }
        answer += this.indentation(level + 1) + "With Args: \n" + sArgs;
        return answer;
    }
}
