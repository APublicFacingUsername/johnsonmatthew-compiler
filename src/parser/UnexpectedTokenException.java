/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser;

import scanner.Token;

/**
 * This is an exception that is raised when an unexpected token is encountered.
 *
 * @author Matt
 */
public class UnexpectedTokenException extends Exception {

    private String message;
    private int lineNumber;
    private Token errorSource;

    /**
     * Constructor that takes a string to be displayed to the user
     *
     * @param message Gives the user a description of what happened.
     */
    public UnexpectedTokenException(String message) {
        this.message = message;
    }

    /**
     * Constructor that takes a string and the line number the error was found
     * on, to be displayed to the user.
     *
     * @param message Gives the user a description of what happened.
     * @param lineNumber Gives the user the line number on which the error
     * occurred.
     */
    public UnexpectedTokenException(String message, int lineNumber) {
        this.message = message;
        this.lineNumber = lineNumber;
    }

    /**
     * Constructor that takes a string, the line number, and the token that was
     * the source of the error. These are all to be displayed to the user.
     *
     * @param message Gives the user a description of what happened.
     * @param lineNumber Gives the user the line number on which the error
     * occurred.
     * @param errorSource The token that caused the error.
     */
    public UnexpectedTokenException(String message, int lineNumber, Token errorSource) {
        this.message = message;
        this.lineNumber = lineNumber;
        this.errorSource = errorSource;
    }

    @Override
    /**
     * Returns a string representation of the exception
     *
     * @return A String representation of the exception
     */
    public String toString() {
        if (errorSource != null) {
            return "UnexpectedTokenException: The token that caused the error was " + errorSource.toString() + "\nException occured on line " + Integer.toString(lineNumber) + "\n" + this.message;
        } else if (lineNumber != 0) {
            return "UnexpectedTokenException: Exception occured on line " + Integer.toString(lineNumber) + "\n" + this.message;
        } else {
            return "UnexpectedTokenException: " + this.message;
        }
    }

    /**
     * Gets the source of the error
     *
     * @return the token that caused the error
     */
    public Token getErrorSource() {
        return this.errorSource;
    }
}
