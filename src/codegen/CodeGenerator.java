/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package codegen;

import parser.SymbolTable;
import parser.Type;
import parser.syntaxtree.*;
import scanner.InvalidTokenException;
import scanner.TokenType;

/**
 * This is the java class for creation of MIPs assembly from our previously
 * constructed syntax tree. The design of this class is recursive descent, with
 * the recursions taking place on the different extensions of the abstract
 * {@code parser.syntaxtree.SyntaxTreeNode} class. To use this class,
 * instantiate the class, then pass in a root node of type
 * {@code parser.syntaxtree.ProgramNode} along with a {@code parser.SymbolTable}
 * to the function {@code generateAsm}. This function will then recurse on the
 * contents of the Syntax Tree referenced by the root, creating assembly as it
 * recurses. Once {@code generateAsm} has been called it is possible to acquire
 * the assembly by making a call to the function {@code getAsm}. The assembly is
 * represented as a single string.
 *
 * @author Matt
 */
public class CodeGenerator {

    private int registerPointer = 0;
    private int activeVariables = 0;
    private String registers[] = {"$v0", "$v1", "$a0", "$a1", "$a2", "$a3", "$t0", "$t1", "$t2", "$t3", "$t4",
        "$t5", "$t6", "$t7", "$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7", "$t8", "$t9"};
    private int floatPointer = 0;
    private int activeFloats = 0;
    private String floatRegisters[] = {"$f0", "$f1", "$f2", "$f3", "$f4", "$f5", "$f6", "$f7", "$f8", "$f9", "$f10", "$f11", "$f12", "$f13", "$f14", "$f15",
        "$f16", "$f17", "$f18", "$f19", "$f20", "$f21", "$f22", "$f23", "$f24", "$f25", "$f26", "$f27", "$f28", "$f29", "$f30", "$f31"};

    private String dataSection = "\n.data\n";
    private String theAsm = "\n.text\n\nmain:\n#Call the program function\naddi $sp, $sp, -4\nsw $ra, 0($sp)\njal ";

    /**
     * Generates MIPs assembly from a syntax tree root node and the symbol table
     * corresponding to the syntax tree in full. Assembly is stored in the
     * instance of this class, {@code CodeGenerator}.
     *
     * @param root The root node of a parser.syntaxtree, as
     * parser.syntaxtree.ProgramNode.
     * @param table The symbol table that corresponds to the syntaxtree in full.
     */
    public void generateAsm(ProgramNode root, SymbolTable table) {
        theAsm += root.getName();
        theAsm += "\nlw $ra, 0($sp)";
        theAsm += "\naddi $sp, $sp, 4";
        theAsm += "\n#Return to caller (system).";
        theAsm += "\njr $ra";
        declarationsAsm(root.getVariables(), table);
        subProgramDecsAsm(root.getFunctions(), table);
        theAsm += "\n\n#Program " + root.getName();
        theAsm += "\n" + root.getName() + ": ";
        compoundStatementAsm(root.getMain());
        theAsm += "\njr $ra";
        theAsm += "\n#End of program " + root.getName();
    }

    /**
     * Acquires the MIPs assembly that has either been generated, or not.
     * Requires a call to {@code generateAsm} for the return string to not be
     * the empty string.
     *
     * @return The string representation of the assembly code generated.
     */
    public String getAsm() {
        return this.dataSection + this.theAsm;
    }

    /**
     * Goes through a declarations node, grabbing each declaration and adding a
     * corresponding statement to the .data section
     */
    private void declarationsAsm(DeclarationsNode decs, SymbolTable table, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }

        for (VariableNode var : decs.getVars()) {
            Type varType = table.getType(var.getName());
            if (varType == Type.INTEGER) {
                dataSection += "\n" + var.getName() + hashCode + ": .word 0";
            } else if (varType == Type.INTEGER_ARRAY) {
                ArrayVariableNode arr = (ArrayVariableNode) var;
                dataSection += "\n" + var.getName() + hashCode + ": .word 0, ";
                int arrLength = arr.getUpperBound() - arr.getLowerBound() - 1;
                for (int i = 0; i < arrLength; i++) {
                    if (i == arrLength - 1) {
                        dataSection += "0";
                    } else {
                        dataSection += "0, ";
                    }
                }
            } else if (varType == Type.REAL) {
                dataSection += "\n" + var.getName() + hashCode + ": .float 0.0";
            } else if (varType == Type.REAL_ARRAY) {
                ArrayVariableNode arr = (ArrayVariableNode) var;
                dataSection += "\n" + var.getName() + hashCode + ": .float 0.0, ";
                int arrLength = arr.getUpperBound() - arr.getLowerBound() - 1;
                for (int i = 0; i < arrLength; i++) {
                    if (i == arrLength - 1) {
                        dataSection += "0.0";
                    } else {
                        dataSection += "0.0, ";
                    }
                }
            }
        }
    }

    /**
     * Goes through the subprogram declarations and recurses on its
     * declarations, subsubprogram decs, and compoundstatement
     */
    private void subProgramDecsAsm(SubProgramDeclarationsNode subDecs, SymbolTable table) {
        for (SubProgramNode subProg : subDecs.getSubProgams()) {
            table.setTableScope(subProg.getName());
            theAsm += "\n\n#Function " + subProg.getName();
            theAsm += "\n" + subProg.getName() + subProg.hashCode() + ": ";
            String hashCode = Integer.toString(subProg.hashCode());
            declarationsAsm(subProg.getDeclarations(), table, hashCode);
            compoundStatementAsm(subProg.getCompoundStatement(), hashCode);
            theAsm += "\njr $ra";
            theAsm += "\n#End of function " + subProg.getName();
            subProgramDecsAsm(subProg.getSubProgramDeclarations(), table);
        }
    }

    /**
     * Takes a statement node, checks what kind it is, and passes it along
     * accordingly
     */
    private void statementAsm(StatementNode statement, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }

        if (statement instanceof ReadNode) {
            ReadNode read = (ReadNode) statement;
            readAsm(read, hashCode);
        } else if (statement instanceof AssignmentStatementNode) {
            AssignmentStatementNode assn = (AssignmentStatementNode) statement;
            assignmentNodeAsm(assn, hashCode);
        } else if (statement instanceof CallNodeAsStatement) {
            CallNodeAsStatement call = (CallNodeAsStatement) statement;
            callNodeStatementAsm(call, hashCode);
        } else if (statement instanceof CompoundStatementNode) {
            CompoundStatementNode comp = (CompoundStatementNode) statement;
            compoundStatementAsm(comp, hashCode);
        } else if (statement instanceof IfStatementNode) {
            IfStatementNode ifStatement = (IfStatementNode) statement;
            ifStatementAsm(ifStatement, hashCode);
        } else if (statement instanceof WhileStatementNode) {
            WhileStatementNode whileStatement = (WhileStatementNode) statement;
            whileStatementAsm(whileStatement, hashCode);
        } else if (statement instanceof WriteNode) {
            WriteNode write = (WriteNode) statement;
            writeAsm(write, hashCode);
        }
    }

    /**
     * pushes all current registers to stack, jumps and links to the function,
     * then pops registers back from stack
     */
    private void callNodeStatementAsm(CallNodeAsStatement callNode, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }
        storeAllAsm();
        int prevRegisterPointer = registerPointer;
        registerPointer = 1;
        int prevActiveVariables = activeVariables;
        activeVariables = 0;
        int prevFloatPointer = floatPointer;
        floatPointer = 1;
        int prevActiveFloats = activeFloats;
        activeFloats = 0;
        for (ExpressionNode ex : callNode.getArgs()) {
            expressionAsm(ex, hashCode);
        }
        theAsm += "\naddi $sp, $sp, -4";
        theAsm += "\nsw $ra, 0($sp)";
        theAsm += "\njal " + callNode.getName() + callNode.getProcOrFunc().hashCode();
        theAsm += "\nlw $ra, 0($sp)";
        theAsm += "\naddi $sp, $sp, 4";
        freeRegisters(activeVariables);
        freeFloatRegisters(activeFloats);
        restoreAllAsm();
        registerPointer = prevRegisterPointer;
        activeVariables = prevActiveVariables;
        floatPointer = prevFloatPointer;
        activeFloats = prevActiveFloats;
    }

    /**
     * Goes through the statements in the compoundstatement, passing them to the
     * statementAsm function
     */
    private void compoundStatementAsm(CompoundStatementNode comp, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }
        for (StatementNode statement : comp.getStatements()) {
            statementAsm(statement, hashCode);
        }
    }

    /**
     * Passes the evaluation statement to expressionAsm, and the then/else
     * blocks to the statementAsm method
     */
    private void ifStatementAsm(IfStatementNode ifStatement, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }

        theAsm += "\n#If Statement #" + ifStatement.hashCode();
        if (ifStatement.getTest() instanceof NegationNode) {
            NegationNode negation = (NegationNode) ifStatement.getTest();
            negationNodeAsm(negation, 0, hashCode);
        } else if (ifStatement.getTest() instanceof OperationNode) {
            OperationNode op = (OperationNode) ifStatement.getTest();
            operationAsm(op, hashCode);
        }
        theAsm += " ifStatement" + ifStatement.hashCode();
        theAsm += "\n#Else block";
        statementAsm(ifStatement.getElseStatement());
        theAsm += "j endIfStatement" + ifStatement.hashCode();
        theAsm += "\nifStatement" + ifStatement.hashCode() + ": ";
        theAsm += "\n#Then block";
        statementAsm(ifStatement.getThenStatement());
        theAsm += "\nendIfStatement" + ifStatement.hashCode() + ": \n";
    }

    /**
     * Passes the evaluation expression to expressionAsm, then inverts its truth
     * value.
     */
    /**
     * The function then passes the while block to statementAsm
     */
    private void whileStatementAsm(WhileStatementNode whileStatement, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }
        theAsm += "\n#While Statement #" + whileStatement.hashCode();
        theAsm += "\nwhileStatement" + whileStatement.hashCode() + ": ";
        if (whileStatement.getExpression() instanceof NegationNode) {
            NegationNode negation = (NegationNode) whileStatement.getExpression();
            negationNodeAsm(negation, 0, hashCode);
        } else if (whileStatement.getExpression() instanceof OperationNode) {
            OperationNode op = (OperationNode) whileStatement.getExpression();
            operationAsm(op, hashCode);
        }
        whileEvalFlipper();
        theAsm += " endWhileStatement" + whileStatement.hashCode();
        statementAsm(whileStatement.getStatement(), hashCode);
        theAsm += "\nj whileStatement" + whileStatement.hashCode();
        theAsm += "\nendWhileStatement" + whileStatement.hashCode() + ": ";
    }

    /**
     * Flips the branch command of the last line to its complement
     */
    private void whileEvalFlipper() {
        String[] strings = theAsm.split("\\n");
        theAsm = "";
        for (int i = 0; i < strings.length - 1; i++) {
            theAsm += "\n" + strings[i];
        }
        theAsm += "\n";
        String lastLine = strings[strings.length - 1];
        if (lastLine.contains("blt") || lastLine.contains("bgt") || lastLine.contains("ble")
                || lastLine.contains("bge") || lastLine.contains("beq") || lastLine.contains("bne")) {
            if (lastLine.contains("blt")) {
                theAsm += lastLine.replace("blt", "bge");
            } else if (lastLine.contains("bgt")) {
                theAsm += lastLine.replace("bgt", "ble");
            } else if (lastLine.contains("ble")) {
                theAsm += lastLine.replace("ble", "bgt");
            } else if (lastLine.contains("bge")) {
                theAsm += lastLine.replace("bge", "blt");
            } else if (lastLine.contains("beq")) {
                theAsm += lastLine.replace("beq", "bne");
            } else if (lastLine.contains("bne")) {
                theAsm += lastLine.replace("bne", "beq");
            }
        } else {
            if (lastLine.contains("bc1t")) {
                theAsm += "\n" + lastLine.replace("bc1t", "bc1f");
            } else if (lastLine.contains("bc1f")) {
                theAsm += "\n" + lastLine.replace("bc1f", "bc1t");
            }
        }
    }

    /**
     * Pushes the argument and return registers to stack, then sets them in
     * accord with the type of read command specified. Also prints out a nice
     * string to indicate the type of input expected.
     */
    private void readAsm(ReadNode read, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }
        VariableNode var = read.getLValue();
        if (read.getType() == Type.INTEGER) {
            var.setRegister(requestRegisters(1)[0]);
            dataSection += "\nreadAscii" + read.hashCode() + ": .asciiz \"Enter an integer: \"";
            dataSection += "\nreadInt" + read.hashCode() + ": .word 0";
            theAsm += "\naddi $sp, $sp, -8";
            theAsm += "\nsw $v0, 0($sp)";
            theAsm += "\nsw $a0, 0($sp)";
            theAsm += "\nli $v0, 4";
            theAsm += "\nla $a0, readAscii" + read.hashCode();
            theAsm += "\nsyscall";
            theAsm += "\nli $v0, 5";
            theAsm += "\nsyscall";
            theAsm += "\naddi " + var.getRegister() + ", $v0, 0";
            if (var.getRegister() != "$a0") {
                theAsm += "\nlw $a0, 0($sp)";
            }
            if (var.getRegister() != "$v0") {
                theAsm += "\nlw $v0, 4($sp)";
            }
            theAsm += "\naddi $sp, $sp, 8";
            theAsm += "\nsw " + var.getRegister() + ", " + var.getName() + hashCode;
        } else {
            var.setRegister(requestFloatRegisters(1)[0]);
            dataSection += "\nreadAscii" + read.hashCode() + ": .asciiz \"Enter a real: \"";
            dataSection += "\nreadReal" + read.hashCode() + ": .float 0.0";
            theAsm += "\naddi $sp, $sp, -8";
            theAsm += "\nsw $v0, 0($sp)";
            theAsm += "\nsw $a0, 0($sp)";
            theAsm += "\nli $v0, 4";
            theAsm += "\nla $a0, readAscii" + read.hashCode();
            theAsm += "\nsyscall";
            theAsm += "\nlw $a0, 0($sp)";
            theAsm += "\nli $v0, 6";
            if (var.getRegister() != "$f0") {
                theAsm += "\nsw $f0, 0($sp)";
                theAsm += "\nsyscall";
                theAsm += "\nmov.s " + var.getRegister() + ", $f0";
                theAsm += "\nlw $f0, 0($sp)";
                theAsm += "\naddi $sp, $sp, 4";
            } else {
                theAsm += "\naddi $sp, $sp, 4";
                theAsm += "\nsyscall";
            }
            theAsm += "\nlw $v0, 0($sp)";
            theAsm += "\naddi $sp, $sp, 4";
            theAsm += "\nswc1 " + var.getRegister() + ", " + var.getName() + hashCode;
        }
    }

    /**
     * Pushes the argument and return registers to stack and sets them in accord
     * with the write command.
     */
    /**
     * Also prints a nice string indicating that this is output, then prints a
     * newline
     */
    private void writeAsm(WriteNode write, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }
        expressionAsm(write.getExprToWrite(), progHash);
        if (write.getExprToWrite().getType() == Type.INTEGER) {
            dataSection += "\nwriteAscii" + write.hashCode() + ": .asciiz \"Output: \"";
            theAsm += "\naddi $sp, $sp, -12";
            theAsm += "\nsw $t0, 8($sp)";
            theAsm += "\naddi $t0, " + write.getExprToWrite().getRegister() + ", 0";
            theAsm += "\nsw $v0, 4($sp)";
            theAsm += "\nsw $a0, 0($sp)";
            theAsm += "\nli $v0, 4";
            theAsm += "\nla $a0, writeAscii" + write.hashCode();
            theAsm += "\nsyscall";
            theAsm += "\nli $v0, 1";
            theAsm += "\naddi $a0, $t0, 0";
            theAsm += "\nsyscall";
            theAsm += "\nlw $a0, 0($sp)";
            theAsm += "\nlw $v0, 4($sp)";
            theAsm += "\nlw $t0, 8($sp)";
            theAsm += "\naddi $sp, $sp, 12";
            theAsm += "\n";
        } else {
            dataSection += "\nwriteAscii" + write.hashCode() + ": .asciiz \"Output: \"";
            theAsm += "\naddi $sp, $sp, -8";
            theAsm += "\nswc1 $f12, 4($sp)";
            theAsm += "\nmov.s $f12, " + write.getExprToWrite().getRegister();
            theAsm += "\nsw $v0, 0($sp)";
            theAsm += "\nli $v0, 4";
            theAsm += "\nla $a0, writeAscii" + write.hashCode();
            theAsm += "\nsyscall";
            theAsm += "\nli $v0, 2";
            theAsm += "\nsyscall";
            theAsm += "\nlw $v0, 0($sp)";
            theAsm += "\nlwc1 $f12, 4($sp)";
            theAsm += "\naddi $sp, $sp, 8";
            theAsm += "\n";
        }
        theAsm += "\naddi $sp, $sp, -8";
        theAsm += "\nsw $v0, 4($sp)";
        theAsm += "\nsw $a0, 0($sp)";
        theAsm += "\nli $v0, 11";
        theAsm += "\nli $a0, 10";
        theAsm += "\nsyscall";
        theAsm += "\nlw $a0, 0($sp)";
        theAsm += "\nlw $v0, 4($sp)";
        theAsm += "\naddi $sp, $sp, 8";
    }

    /**
     * Calls the function that corresponds to the type of expression input
     */
    private void expressionAsm(ExpressionNode expr, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }

        if (expr instanceof ArrayIndexNode) {
            ArrayIndexNode arrInd = (ArrayIndexNode) expr;
            arrayIndexAsm(arrInd, hashCode);
        } else if (expr instanceof ArrayVariableNode) {
            ArrayVariableNode arrVar = (ArrayVariableNode) expr;
            arrayAsm(arrVar, hashCode);
        } else if (expr instanceof CallNodeAsExpression) {
            CallNodeAsExpression call = (CallNodeAsExpression) expr;
            callNodeExprAsm(call, hashCode);
        } else if (expr instanceof NegationNode) {
            NegationNode negation = (NegationNode) expr;
            negationNodeAsm(negation, 0, hashCode);
        } else if (expr instanceof OperationNode) {
            OperationNode op = (OperationNode) expr;
            operationAsm(op, hashCode);
        } else if (expr instanceof RegularVariableNode) {
            RegularVariableNode regVar = (RegularVariableNode) expr;
            varAsm(regVar, hashCode);
        } else if (expr instanceof ReturnVariableNode) {
            ReturnVariableNode retVar = (ReturnVariableNode) expr;
            varAsm(retVar, hashCode);
        } else if (expr instanceof ValueNode) {
            ValueNode val = (ValueNode) expr;
            valueAsm(val);
        }
    }

    /**
     * Pushes all registers to the stack, evaluates all arguments to either a
     * basic real or integer (removing operations, etc) then sets these
     * arguments to registers of index 1 and onwards, reserving index 0 for the
     * return value. Jumps and links, then stores the value into the register
     * allocated for the expression.
     */
    private void callNodeExprAsm(CallNodeAsExpression callNode, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }

        if (callNode.getType() == Type.REAL) {
            callNode.setRegister(requestFloatRegisters(1)[0]);
        } else {
            callNode.setRegister(requestRegisters(1)[0]);
        }

        storeAllAsm();
        int prevRegisterPointer = registerPointer;
        registerPointer = 1;
        int prevActiveVariables = activeVariables;
        activeVariables = 0;
        int prevFloatPointer = floatPointer;
        floatPointer = 1;
        int prevActiveFloats = activeFloats;
        activeFloats = 0;
        theAsm += "\n#Evaluate the expressions used as arguments for the function call.";
        for (ExpressionNode ex : callNode.getArgs()) {
            expressionAsm(ex, hashCode);
        }
        theAsm += "\n#Make the call!";
        theAsm += "\naddi $sp, $sp, -4";
        theAsm += "\nsw $ra, 0($sp)";
        theAsm += "\njal " + callNode.getName() + callNode.getProcOrFunc().hashCode();
        theAsm += "\nlw $ra, 0($sp)";
        theAsm += "\naddi $sp, $sp, 4";
        theAsm += "\n#Store our return value.";
        if (callNode.getType() == Type.REAL) {
            dataSection += "\n" + callNode.getName() + callNode.hashCode() + ": .float 0.0";
            theAsm += "\nswc1 $f0, " + callNode.getName() + callNode.hashCode();
        } else {
            dataSection += "\n" + callNode.getName() + callNode.hashCode() + ": .word 0";
            theAsm += "\nsw $v0, " + callNode.getName() + callNode.hashCode();
        }
        freeRegisters(activeVariables);
        freeFloatRegisters(activeFloats);
        restoreAllAsm();
        registerPointer = prevRegisterPointer;
        activeVariables = prevActiveVariables;
        floatPointer = prevFloatPointer;
        activeFloats = prevActiveFloats;
        theAsm += "\n#Load the return value.";
        if (callNode.getProcOrFunc().getReturnType() == Type.REAL_ARRAY || callNode.getProcOrFunc().getReturnType() == Type.INTEGER_ARRAY) {
            theAsm += "\nla " + callNode.getRegister() + ", " + callNode.getName() + callNode.hashCode();
        } else if (callNode.getType() == Type.REAL) {
            String addrReg = requestRegisters(1)[0];
            theAsm += "\nla " + addrReg + ", " + callNode.getName() + callNode.hashCode();
            theAsm += "\nlwc1 " + callNode.getRegister() + ", 0(" + addrReg + ")";
            freeRegisters(1);
        } else {
            theAsm += "\nla " + callNode.getRegister() + ", " + callNode.getName() + callNode.hashCode();
            theAsm += "\nlw " + callNode.getRegister() + ", 0(" + callNode.getRegister() + ")";
        }
    }

    /**
     * Helper function to push all register values to the stack
     */
    private void storeAllAsm() {
        theAsm += "\n\n#Push all registers onto the stack for our function call. Later implementations might do this with a loop.";
        for (int i = 0; i < 24; i++) {
            theAsm += "\naddi $sp, $sp, -4";
            theAsm += "\nsw " + registers[i] + ", 0($sp)";
        }
        for (int i = 0; i < 32; i++) {
            theAsm += "\naddi $sp, $sp, -4";
            theAsm += "\nswc1 " + floatRegisters[i] + ", 0($sp)";
        }
        theAsm += "\n";
    }

    /**
     * Helper function to pop all register values off the stack
     */
    private void restoreAllAsm() {
        theAsm += "\n\n#Pop all registers off from the stack for our function call. Later implementations might do this with a loop.";
        for (int i = 0; i < 24; i++) {
            theAsm += "\nlw " + registers[i] + ", 0($sp)";
            theAsm += "\naddi $sp, $sp, 4";
        }
        for (int i = 0; i < 32; i++) {
            theAsm += "\nlwc1 " + floatRegisters[i] + ", 0($sp)";
            theAsm += "\naddi $sp, $sp, 4";
        }
        theAsm += "\n";
    }

    /**
     * Digs through negation nodes to find the operation it is negating, then
     * flips it if an odd number of negations have been parsed
     */
    private void negationNodeAsm(NegationNode negationNode, int negationsObserved, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }
        //If the negated quantity is also a negation node, make recursive call
        if (negationNode.getExpr() instanceof NegationNode) {
            NegationNode nextNeg = (NegationNode) negationNode.getExpr();
            negationNodeAsm(nextNeg, negationsObserved++);
            //If the negated quantity is an operation node, pass it to the function for operation code gen
        } else if (negationNode.getExpr() instanceof OperationNode) {
            OperationNode op = (OperationNode) negationNode.getExpr();
            operationAsm(op, hashCode);
            //If the operation has been negated an odd number of times, change it.
            if (negationsObserved % 2 == 1) {
                String[] strings = theAsm.split("\\n");
                theAsm = "";
                for (int i = 0; i < strings.length - 2; i++) {
                    theAsm += "\n" + strings[i];
                }
                theAsm += "\n";
                String secondToLast = strings[strings.length - 2];
                String lastLine = strings[strings.length - 1];
                if (lastLine.contains("blt") || lastLine.contains("bgt") || lastLine.contains("ble")
                        || lastLine.contains("bge") || lastLine.contains("beq") || lastLine.contains("bne")) {
                    theAsm += secondToLast + "\n";
                    if (lastLine.contains("blt")) {
                        theAsm += lastLine.replace("blt", "bge");
                    } else if (lastLine.contains("bgt")) {
                        theAsm += lastLine.replace("bgt", "ble");
                    } else if (lastLine.contains("ble")) {
                        theAsm += lastLine.replace("ble", "bgt");
                    } else if (lastLine.contains("bge")) {
                        theAsm += lastLine.replace("bge", "blt");
                    } else if (lastLine.contains("beq")) {
                        theAsm += lastLine.replace("beq", "bne");
                    } else if (lastLine.contains("bne")) {
                        theAsm += lastLine.replace("bne", "beq");
                    }
                } else {
                    if (secondToLast.contains("c.lt.s") && lastLine.contains("bc1t")) {
                        theAsm += secondToLast;
                        theAsm += "\n" + lastLine.replace("bc1t", "bc1f");
                    } else if (secondToLast.contains("c.lt.s") && lastLine.contains("bc1f")) {
                        theAsm += secondToLast;
                        theAsm += "\n" + lastLine.replace("bc1f", "bc1t");
                    } else if (secondToLast.contains("c.le.s") && lastLine.contains("bc1t")) {
                        theAsm += secondToLast;
                        theAsm += "\n" + lastLine.replace("bc1t", "bc1f");
                    } else if (secondToLast.contains("c.le.s") && lastLine.contains("bc1f")) {
                        theAsm += secondToLast;
                        theAsm += "\n" + lastLine.replace("bc1f", "bc1t");
                    } else if (secondToLast.contains("c.eq.s") && lastLine.contains("bc1t")) {
                        theAsm += secondToLast;
                        theAsm += "\n" + secondToLast.replace("bc1t", "bc1f");
                    } else if (secondToLast.contains("c.eq.s") && lastLine.contains("bc1f")) {
                        theAsm += secondToLast;
                        theAsm += "\n" + secondToLast.replace("bc1f", "bc1t");
                    }
                }
            }
        }
    }

    private void assignmentNodeAsm(AssignmentStatementNode assnNode, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }
        VariableNode var = assnNode.getLValue();
        if (var instanceof ReturnVariableNode) {
            theAsm += "\n#Return Value: " + assnNode.toString();
        } else {
            theAsm += "\n#Assignment Statement: " + assnNode.toString();
        }
        expressionAsm(assnNode.getExpression(), hashCode);
        if (var instanceof ReturnVariableNode) {
            if (assnNode.getExpression() instanceof ArrayVariableNode) {
                //Same as elsewhere, this functionality is called on account of rain.
            } else if (assnNode.getLValue().getType() == Type.INTEGER) {
                theAsm += "\naddi $v0, " + assnNode.getExpression().getRegister() + ", 0";
                freeRegisters(1);
            } else {
                theAsm += "\nmov.s $f0, " + assnNode.getExpression().getRegister();
                freeFloatRegisters(1);
            }
        } else {
            if (assnNode.getLValue().getType() == Type.INTEGER) {
                if (assnNode.getLValue() instanceof ArrayIndexNode) {
                    ArrayIndexNode arrayIndex = (ArrayIndexNode) assnNode.getLValue();
                    String storeAddr = requestRegisters(1)[0];
                    theAsm += "\nla " + storeAddr + ", " + arrayIndex.getArrayRef().getName() + hashCode;
                    expressionAsm(arrayIndex.getOffset(), hashCode);
                    String val4 = requestRegisters(1)[0];
                    theAsm += "\nli " + val4 + ", 4";
                    theAsm += "\nmult " + arrayIndex.getOffset().getRegister() + ", " + val4;
                    freeRegisters(1);
                    theAsm += "\nmflo " + arrayIndex.getOffset().getRegister();
                    theAsm += "\nadd " + storeAddr + ", " + storeAddr + ", " + arrayIndex.getOffset().getRegister();
                    theAsm += "\nsw " + assnNode.getExpression().getRegister() + ", (" + storeAddr + ")";
                } else {
                    theAsm += "\nsw " + assnNode.getExpression().getRegister() + ", " + var.getName() + hashCode;
                    freeRegisters(1);
                }
            } else if (assnNode.getLValue().getType() == Type.REAL) {
                if (assnNode.getLValue() instanceof ArrayIndexNode) {
                    ArrayIndexNode arrayIndex = (ArrayIndexNode) assnNode.getLValue();
                    String storeAddr = requestRegisters(1)[0];
                    theAsm += "\nla " + storeAddr + ", " + arrayIndex.getArrayRef().getName() + hashCode;
                    expressionAsm(arrayIndex.getOffset(), hashCode);
                    String val4 = requestRegisters(1)[0];
                    theAsm += "\nli " + val4 + ", 4";
                    theAsm += "\nmult " + arrayIndex.getOffset().getRegister() + ", " + val4;
                    freeRegisters(1);
                    theAsm += "\nmflo " + arrayIndex.getOffset().getRegister();
                    theAsm += "\nadd " + storeAddr + ", " + storeAddr + ", " + arrayIndex.getOffset().getRegister();
                    theAsm += "\nswc1 " + assnNode.getExpression().getRegister() + ", 0(" + storeAddr + ")";
                } else {
                    theAsm += "\nswc1 " + assnNode.getExpression().getRegister() + ", " + var.getName() + hashCode;
                    freeFloatRegisters(1);
                }
            } else if (assnNode.getLValue() instanceof ArrayVariableNode) {
                //Something more that could be done, but is absolutely no where in the specifications.
                //Fuck it, I'm gonna call the game on account'a rain.
            }
        }
        theAsm += "\n";
    }

    /**
     * Passes off the operands to their corresponding assembly function, then
     * creates the appropriate instruction
     */
    private void operationAsm(OperationNode op, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }
        if (op.getType() == Type.INTEGER) {
            op.setStoreReg(requestRegisters(1)[0]);
        } else if (op.getType() == Type.REAL) {
            op.setStoreReg(requestFloatRegisters(1)[0]);
        }

        if (op.getLeft() instanceof OperationNode) {
            OperationNode opLeft = (OperationNode) op.getLeft();
            operationAsm(opLeft, hashCode);
            op.setLeftReg(opLeft.getStoreReg());
        } else if (op.getLeft() instanceof ValueNode) {
            ValueNode valLeft = (ValueNode) op.getLeft();
            valueAsm(valLeft);
            op.setLeftReg(valLeft.getRegister());
        } else if (op.getLeft() instanceof RegularVariableNode) {
            RegularVariableNode varLeft = (RegularVariableNode) op.getLeft();
            varAsm(varLeft, hashCode);
            op.setLeftReg(varLeft.getRegister());
        } else if (op.getLeft() instanceof ArrayIndexNode) {
            ArrayIndexNode arrayLeft = (ArrayIndexNode) op.getLeft();
            arrayIndexAsm(arrayLeft, hashCode);
            op.setLeftReg(arrayLeft.getRegister());
        } else if (op.getLeft() instanceof CallNodeAsExpression) {
            CallNodeAsExpression callNode = (CallNodeAsExpression) op.getLeft();
            callNodeExprAsm(callNode, hashCode);
            op.setLeftReg(callNode.getRegister());
        }

        if (op.getRight() instanceof OperationNode) {
            OperationNode opRight = (OperationNode) op.getRight();
            operationAsm(opRight, hashCode);
            op.setRightReg(opRight.getStoreReg());
        } else if (op.getRight() instanceof ValueNode) {
            ValueNode valRight = (ValueNode) op.getRight();
            valueAsm(valRight);
            op.setRightReg(valRight.getRegister());
        } else if (op.getRight() instanceof RegularVariableNode) {
            RegularVariableNode varRight = (RegularVariableNode) op.getRight();
            varAsm(varRight, hashCode);
            op.setRightReg(varRight.getRegister());
        } else if (op.getRight() instanceof ArrayIndexNode) {
            ArrayIndexNode arrayRight = (ArrayIndexNode) op.getRight();
            arrayIndexAsm(arrayRight, hashCode);
            op.setRightReg(arrayRight.getRegister());
        } else if (op.getRight() instanceof CallNodeAsExpression) {
            CallNodeAsExpression callNode = (CallNodeAsExpression) op.getRight();
            callNodeExprAsm(callNode, hashCode);
            op.setRightReg(callNode.getRegister());
        }

        String opChar = op.getOperation();
        if (op.getOpType() != TokenType.relop) {
            switch (opChar) {
                case "+":
                    if (op.getType() == Type.INTEGER) {
                        theAsm += "\nadd " + op.getStoreReg() + ", " + op.getLeftReg() + ", " + op.getRightReg();
                        freeRegisters(2);
                    } else {
                        theAsm += "\nadd.s " + op.getStoreReg() + ", " + op.getLeftReg() + ", " + op.getRightReg();
                        freeFloatRegisters(2);
                    }
                    break;
                case "-":
                    if (op.getType() == Type.INTEGER) {
                        theAsm += "\nsub " + op.getStoreReg() + ", " + op.getLeftReg() + ", " + op.getRightReg();
                        freeRegisters(2);
                    } else {
                        theAsm += "\nsub.s " + op.getStoreReg() + ", " + op.getLeftReg() + ", " + op.getRightReg();
                        freeFloatRegisters(2);
                    }
                    break;
                case "/":
                    if (op.getType() == Type.INTEGER) {
                        theAsm += "\ndiv " + op.getLeftReg() + ", " + op.getRightReg();
                        theAsm += "\nmflo " + op.getStoreReg();
                        freeRegisters(2);
                    } else {
                        theAsm += "\ndiv.s " + op.getStoreReg() + ", " + op.getLeftReg() + ", " + op.getRightReg();
                        freeFloatRegisters(2);
                    }
                    break;
                case "*":
                    if (op.getType() == Type.INTEGER) {
                        theAsm += "\nmult " + op.getLeftReg() + ", " + op.getRightReg();
                        theAsm += "\nmflo " + op.getStoreReg();
                        freeRegisters(2);
                    } else {
                        theAsm += "\nmul.s " + op.getStoreReg() + ", " + op.getLeftReg() + ", " + op.getRightReg();
                        freeFloatRegisters(2);
                    }
                    break;
            }
        } else {
            switch (opChar) {
                case "<":
                    if (op.getType() == Type.INTEGER) {
                        theAsm += "\nblt " + op.getLeftReg() + ", " + op.getRightReg();
                        freeRegisters(2);
                    } else {
                        theAsm += "\nc.lt.s " + op.getLeftReg() + ", " + op.getRightReg();
                        theAsm += "\nbc1t ";
                        freeFloatRegisters(2);
                    }
                    break;
                case ">":
                    if (op.getType() == Type.INTEGER) {
                        theAsm += "\nbgt " + op.getLeftReg() + ", " + op.getRightReg();
                        freeRegisters(2);
                    } else {
                        theAsm += "\nc.lt.s " + op.getLeftReg() + ", " + op.getRightReg();
                        theAsm += "\nbc1f ";
                        freeFloatRegisters(2);
                    }
                    break;
                case "<=":
                    if (op.getType() == Type.INTEGER) {
                        theAsm += "\nble " + op.getLeftReg() + ", " + op.getRightReg();
                        freeRegisters(2);
                    } else {
                        theAsm += "\nc.le.s " + op.getLeftReg() + ", " + op.getRightReg();
                        theAsm += "\nbc1t ";
                        freeFloatRegisters(2);
                    }
                    break;
                case ">=":
                    if (op.getType() == Type.INTEGER) {
                        theAsm += "\nbge " + op.getLeftReg() + ", " + op.getRightReg();
                        freeRegisters(2);
                    } else {
                        theAsm += "\nc.le.s " + op.getLeftReg() + ", " + op.getRightReg();
                        theAsm += "\nbc1f ";
                        freeFloatRegisters(2);
                    }
                    break;
                case "=":
                    if (op.getType() == Type.INTEGER) {
                        theAsm += "\nbeq " + op.getLeftReg() + ", " + op.getRightReg();
                        freeRegisters(2);
                    } else {
                        theAsm += "\nc.eq.s " + op.getLeftReg() + ", " + op.getRightReg();
                        theAsm += "\nbc1t ";
                        freeFloatRegisters(2);
                    }
                    break;
                case "<>":
                    if (op.getType() == Type.INTEGER) {
                        theAsm += "\nbne " + op.getLeftReg() + ", " + op.getRightReg();
                        freeRegisters(2);
                    } else {
                        theAsm += "\nc.eq.s " + op.getLeftReg() + ", " + op.getRightReg();
                        theAsm += "\nbc1f ";
                        freeFloatRegisters(2);
                    }
                    break;
            }
        }
    }

    /**
     * Loads the address of the array. This would do more if array assignments
     * has been implemented in code gen.
     */
    private void arrayAsm(ArrayVariableNode array, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }
        array.setRegister(requestRegisters(1)[0]);
        theAsm += "\nla " + array.getRegister() + ", " + array.getName() + hashCode;
    }

    /**
     * Grabs the address of the first element of the array then adds the offset
     * * 4 to that address, then loads whatever value is present at that
     * address.
     */
    private void arrayIndexAsm(ArrayIndexNode arrayIndex, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode = progHash[0];
        }
        if (arrayIndex.getType() == Type.INTEGER) {
            arrayIndex.setRegister(requestRegisters(1)[0]);
            expressionAsm(arrayIndex.getOffset(), hashCode);
            theAsm += "\nla " + arrayIndex.getRegister() + ", " + arrayIndex.getArrayRef().getName() + hashCode;
            String val4 = requestRegisters(1)[0];
            theAsm += "\nli " + val4 + ", 4";
            theAsm += "\nmult " + arrayIndex.getOffset().getRegister() + ", " + val4;
            freeRegisters(1);
            theAsm += "\nmflo " + arrayIndex.getOffset().getRegister();
            theAsm += "\nadd " + arrayIndex.getRegister() + ", " + arrayIndex.getRegister() + ", " + arrayIndex.getOffset().getRegister();
            theAsm += "\nlw " + arrayIndex.getRegister() + ", 0(" + arrayIndex.getRegister() + ")";
            freeRegisters(1);
        } else if (arrayIndex.intToReal()) {
            String fReg = requestFloatRegisters(1)[0];
            String iReg = requestRegisters(1)[0];
            expressionAsm(arrayIndex.getOffset(), hashCode);
            theAsm += "\nla " + iReg + ", " + arrayIndex.getArrayRef().getName() + hashCode;
            String val4 = requestRegisters(1)[0];
            theAsm += "\nli " + val4 + ", 4";
            theAsm += "\nmult " + arrayIndex.getOffset().getRegister() + ", " + val4;
            freeRegisters(1);
            theAsm += "\nmflo " + arrayIndex.getOffset().getRegister();
            theAsm += "\nadd " + arrayIndex.getRegister() + ", " + arrayIndex.getRegister() + ", " + arrayIndex.getOffset().getRegister();
            theAsm += "\nlwc1 " + fReg + ", 0(" + iReg + ")";
            theAsm += "\ncvt.s.w " + fReg + ", " + fReg;
            //Free both the array's address register and the register utilized to indicate array offset.
            freeRegisters(2);
            arrayIndex.setRegister(fReg);
        } else {
            String fReg = requestFloatRegisters(1)[0];
            String iReg = requestRegisters(1)[0];
            expressionAsm(arrayIndex.getOffset(), hashCode);
            theAsm += "\nla " + iReg + ", " + arrayIndex.getArrayRef().getName() + hashCode;
            String val4 = requestRegisters(1)[0];
            theAsm += "\nli " + val4 + ", 4";
            theAsm += "\nmult " + arrayIndex.getOffset().getRegister() + ", " + val4;
            freeRegisters(1);
            theAsm += "\nmflo " + arrayIndex.getOffset().getRegister();
            theAsm += "\nadd " + iReg + ", " + iReg + ", " + arrayIndex.getOffset().getRegister();
            theAsm += "\nlwc1 " + fReg + ", 0(" + iReg + ")";
            freeRegisters(2);
            arrayIndex.setRegister(fReg);
        }
    }

    /**
     * Loads immediate value
     */
    private void valueAsm(ValueNode val) {
        //Request 1 register, set the value's register to that.
        if (val.getType() == Type.INTEGER) {
            val.setRegister(requestRegisters(1)[0]);
            theAsm += "\nli " + val.getRegister() + ", " + val.getAttribute();
        } else if (val.intToReal()) {
            val.setRegister(requestFloatRegisters(1)[0]);
            theAsm += "\nli.s " + val.getRegister() + ", " + val.getAttribute() + ".0";
        } else {
            String attribute = parseReal(val.getAttribute());
            val.setRegister(requestFloatRegisters(1)[0]);
            theAsm += "\nli.s " + val.getRegister() + ", " + attribute;
        }
    }

    /**
     * Changes the format of MiniPascal reals to what MIPs accepts as floats for
     * the majority, though possibly not all, cases.
     */
    private String parseReal(String real) {
        if (real.contains("E")) {
            String[] splits = real.split("E");
            float val = Float.parseFloat(splits[0]);
            int exp = Integer.parseInt(splits[1]);
            real = Float.toString((float) (val * Math.pow(10, exp)));
        }
        if (real.contains("+")) {
            real.replaceFirst("\\+", "");
        } else if (real.contains("-")) {
            real.replaceAll("-", "");
        }
        return real;
    }

    /**
     * Loads the given variable from the data section
     */
    private void varAsm(VariableNode var, String... progHash) {
        String hashCode = "";
        if (progHash.length == 1) {
            hashCode += progHash[0];
        }
        //Request 1 register, set the variable's register to that.
        if (var.getType() == Type.INTEGER) {
            var.setRegister(requestRegisters(1)[0]);
            theAsm += "\nla " + var.getRegister() + ", " + var.getName() + hashCode;
            theAsm += "\nlw " + var.getRegister() + ", 0(" + var.getRegister() + ")";
        } else if (var.intToReal()) {
            String fReg = requestFloatRegisters(1)[0];
            theAsm += "\nlwc1 " + fReg + ", " + var.getName() + hashCode;
            theAsm += "\ncvt.s.w " + fReg + ", " + fReg;
            freeRegisters(1);
            var.setRegister(fReg);
        } else {
            String iReg = requestRegisters(1)[0];
            String fReg = requestFloatRegisters(1)[0];
            theAsm += "\nla " + iReg + ", " + var.getName() + hashCode;
            theAsm += "\nlwc1 " + fReg + ", 0(" + iReg + ")";
            freeRegisters(1);
            var.setRegister(fReg);
        }
    }

    /**
     * Function to keep track of the number of occupied registers, alloting the
     * next i free registers
     */
    private String[] requestRegisters(int i) {
        String[] returnArray = new String[i];
        for (int j = 0; j < i; j++) {
            if (activeVariables < 24) {
                returnArray[j] = registers[registerPointer];
                registerPointer++;
                activeVariables++;
            } else if (activeVariables % 24 == 0) {
                registerPointer = 0;
                returnArray[j] = registers[registerPointer];
                theAsm += "\naddi $sp, $sp, -4";
                theAsm += "\nsw " + registers[registerPointer] + ", 0($sp)";
                registerPointer++;
                activeVariables++;
            } else {
                returnArray[j] = registers[registerPointer];
                theAsm += "\naddi $sp, $sp, -4";
                theAsm += "\nsw " + registers[registerPointer] + ", 0($sp)";
                registerPointer++;
                activeVariables++;
            }
        }
        return returnArray;
    }

    /**
     * Tracks the number of occupied registers by removing the records for the
     * previous i registers.
     */
    /**
     * Does not empty the registers, merely allows the software to utilize those
     * registers.
     */
    private void freeRegisters(int i) {
        for (int j = 0; j < i; j++) {
            if (activeVariables % 24 == 0 && activeVariables > 24) {
                theAsm += "\nlw " + registers[registerPointer] + ", 0($sp)";
                theAsm += "\naddi $sp, $sp, 4";
                registerPointer = 23;
                activeVariables--;
            } else if (activeVariables > 24) {
                theAsm += "\nlw " + registers[registerPointer] + ", 0($sp)";
                theAsm += "\naddi $sp, $sp, 4";
                registerPointer--;
                activeVariables--;
            } else {
                if (registerPointer != 0) {
                    registerPointer--;
                }
                if (activeVariables != 0) {
                    activeVariables--;
                }
            }
        }
    }

    /**
     * Request for floats
     */
    private String[] requestFloatRegisters(int i) {
        String[] returnArray = new String[i];
        for (int j = 0; j < i; j++) {
            if (activeFloats < 32) {
                returnArray[j] = floatRegisters[floatPointer];
                floatPointer++;
                activeFloats++;
            } else if (activeFloats % 32 == 0) {
                floatPointer = 0;
                returnArray[j] = floatRegisters[floatPointer];
                theAsm += "\naddi $sp, $sp, -4";
                theAsm += "\nswc1 " + floatRegisters[floatPointer] + ", 0($sp)";
                floatPointer++;
                activeFloats++;
            } else {
                returnArray[j] = floatRegisters[floatPointer];
                theAsm += "\naddi $sp, $sp, -4";
                theAsm += "\nswc1 " + floatRegisters[floatPointer] + ", 0($sp)";
                floatPointer++;
                activeFloats++;
            }
        }
        return returnArray;
    }

    /**
     * Freedom of floats
     */
    private void freeFloatRegisters(int i) {
        for (int j = 0; j < i; j++) {
            if (activeVariables % 32 == 0 && activeVariables > 32) {
                theAsm += "\nlw " + floatRegisters[floatPointer] + ", 0($sp)";
                theAsm += "\naddi $sp, $sp, 4";
                floatPointer = 31;
                activeFloats--;
            } else if (activeVariables > 32) {
                theAsm += "\nlw " + floatRegisters[floatPointer] + ", 0($sp)";
                theAsm += "\naddi $sp, $sp, 4";
                floatPointer--;
                activeFloats--;
            } else {
                if (floatPointer != 0) {
                    floatPointer--;
                }
                if (activeFloats != 0) {
                    activeFloats--;
                }
            }
        }
    }
}
