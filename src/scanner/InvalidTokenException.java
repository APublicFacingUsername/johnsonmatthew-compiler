/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package scanner;

/**
 * This is an exception raised when a set of symbols not in the grammar is
 * encountered.
 *
 * @author Matt
 */
public class InvalidTokenException extends Exception {

    private String message;
    private int lineNumber;
    private Token errorSource;

    /**
     * Constructor that takes a string message to be displayed to the user
     *
     * @param message Gives the user a description of what happened.
     */
    public InvalidTokenException(String message) {
        this.message = message;
    }

    /**
     * Constructor that takes a string message and the line number the error
     * occurred on, to be displayed to the user
     *
     * @param message Gives the user a description of what happened.
     * @param lineNumber Gives the user the line number on which the error
     * occurred.
     */
    public InvalidTokenException(String message, int lineNumber) {
        this.message = message;
        this.lineNumber = lineNumber;
    }

    /**
     * Constructor that takes a string message, the line number the error
     * occurred on, and the token that was the source of the error. These are to
     * be displayed to the user.
     *
     * @param message Gives the user a description of what happened.
     * @param lineNumber Gives the user the line number on which the error
     * occurred.
     * @param errorSource The token that caused the error.
     */
    public InvalidTokenException(String message, int lineNumber, Token errorSource) {
        this.message = message;
        this.lineNumber = lineNumber;
        this.errorSource = errorSource;
    }

    @Override
    /**
     * A string representation of the error that returns the information
     * provided.
     *
     * @return A String representation of the exception
     */
    public String toString() {
        if (errorSource != null) {
            return "InvalidTokenException: The token that caused the error was " + errorSource.toString() + "\nException occured on line " + Integer.toString(lineNumber) + "\n" + this.message;
        } else if (lineNumber != 0) {
            return "InvalidTokenException: Exception occured on line " + Integer.toString(lineNumber) + "\n" + this.message;
        } else {
            return "InvalidTokenException: " + this.message;
        }
    }

    /**
     * Gets the token that was the source of the error
     *
     * @return the token that was the source of the error.
     */
    public Token getErrorSource() {
        return this.errorSource;
    }
}
