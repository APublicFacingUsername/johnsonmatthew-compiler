/*
 *This is software designed by Matthew D Johnson of Augsburg College
 *It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 *For more information, contact johns116@augsburg.edu
 */
package scanner;

import java.util.HashMap;

/**
 * This is a HashMap that allows for the quick lookup of a lexeme to determine
 * if it is valid.
 *
 * @author Matt
 */
public class LookupTable extends HashMap<String, TokenType> {

    /**
     * Basic constructor that populates the the hashmap with the different types
     * of tokens that we expect to find in the course of scanning the input
     * file. This lookup table is used to examine a lexeme and determine the
     * token type that corresponds to it.
     */
    public LookupTable() {
        super.put(";", TokenType.SYM_semicolon);
        super.put(".", TokenType.SYM_period);
        super.put(",", TokenType.SYM_comma);
        super.put(":", TokenType.SYM_colon);
        super.put("[", TokenType.SYM_lbracket);
        super.put("]", TokenType.SYM_rbracket);
        super.put("(", TokenType.SYM_lparens);
        super.put(")", TokenType.SYM_rparens);
        super.put("=", TokenType.relop);
        super.put("<>", TokenType.relop);
        super.put("<=", TokenType.relop);
        super.put(">=", TokenType.relop);
        super.put("<", TokenType.relop);
        super.put(">", TokenType.relop);
        super.put("+", TokenType.addop);
        super.put("-", TokenType.addop);
        super.put("*", TokenType.mulop);
        super.put("/", TokenType.mulop);
        super.put(":=", TokenType.assignop);
        super.put("var", TokenType.KW_var);
        super.put("while", TokenType.KW_while);
        super.put("array", TokenType.KW_array);
        super.put("begin", TokenType.KW_begin);
        super.put("do", TokenType.KW_do);
        super.put("else", TokenType.KW_else);
        super.put("end", TokenType.KW_end);
        super.put("function", TokenType.KW_function);
        super.put("if", TokenType.KW_if);
        super.put("integer", TokenType.KW_integer);
        super.put("not", TokenType.KW_not);
        super.put("of", TokenType.KW_of);
        super.put("procedure", TokenType.KW_procedure);
        super.put("program", TokenType.KW_program);
        super.put("real", TokenType.KW_real);
        super.put("then", TokenType.KW_then);
        super.put("or", TokenType.addop);
        super.put("div", TokenType.mulop);
        super.put("mod", TokenType.mulop);
        super.put("and", TokenType.mulop);
        super.put("readint", TokenType.KW_readint);
        super.put("readfloat", TokenType.KW_readfloat);
        super.put("write", TokenType.KW_write);
    }
}
