/*
 *This is software designed by Matthew D Johnson of Augsburg College
 *It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 *For more information, contact johns116@augsburg.edu
 */
package scanner;

/**
 * This is a class to represent a token in the MiniPascal language designated in
 * the SDD. Instances of this object bind a lexeme and its token type together
 * so that we might be able to identify a token by either.
 *
 * @author Matt
 */
public class Token {

    private String lexeme;
    private TokenType ourTokenType;

    /**
     * Constructor for Token object.
     *
     * @param inputLexeme The String value being tokenized. Corresponds to a
     * private String variable.
     * @param inputType The TokenType as taken from our enum. Corresponds to a
     * private TokenType variable.
     */
    public Token(String inputLexeme, TokenType inputType) {
        this.lexeme = inputLexeme;
        this.ourTokenType = inputType;
    }

    /**
     * Get the lexeme that is associated with this token
     *
     * @return A String representation of the token
     */
    public String getLex() {
        return this.lexeme;
    }

    /**
     * Get the type of token that is associated with this token.
     *
     * @return A value from the TokenType enum denoting the type of token
     */
    public TokenType getType() {
        return this.ourTokenType;
    }

    /**
     * Return a string representation of the token.
     *
     * @return A string containing both the lexeme and a string denoting the
     * TokenType
     */
    @Override
    public String toString() {
        return this.lexeme + "," + this.ourTokenType.name();
    }
}
