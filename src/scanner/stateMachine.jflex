/*
 *This is software designed by Matthew D Johnson of Augsburg College
 *It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 *For more information, contact johns116@augsburg.edu
 */
package scanner;

import java.util.HashMap;

%%
/* Options */
/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
/* !!! Please note that JFlex does not contain an option to have exceptions caught by its   !!! */
/* !!! parsing method, here named as nextToken(). You will need to remove the default throw !!! */
/* !!! declaration for the exception java.io.IOException from the nextToken() function and  !!! */
/* !!! implement a generic try/catch block for this error encapsulating the full contents   !!! */
/* !!! of nextToken(). Failure to do so will cause compile errors of the java code.         !!! */
/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

%class		Scanner   /* Names the produced java file */
%function	nextToken /* Renames the yylex() function */
%type		Token     /* Defines the return type of the scanning function */
%public
%eofval{
  return null;
%eofval}
%line
%unicode

%yylexthrow{
InvalidTokenException
%yylexthrow}

/* Code implanted inside the class file */
/* User Code */
%{
	/* Hashmap representation of valid keywords to be used for looking up type after parsing */
	public LookupTable ourLookupTable = new LookupTable();
        /* Method to allow us to get the line number the scanner is on */
        public int getLine(){
            return yyline;
        }
%}

/* Macros */

/* Sorry for the following regex, may god have mercy on my soul */
keyword_or_id		= [A-Za-z][A-Za-z0-9]*
num					= {digits}{optional_fraction}?{optional_exponent}?
symbols				= ([;,\.:\[\]\(\)\+-=<>\*/] | <> | <= | >= | :=)
whitespace			= [ \n\r\t]
comments			= \{([^\}] | {whitespace})*\}		/* a left brace followed by any character besides a right brace including new lines, ending with right brace */
digit				= [0-9]
digits				= {digit}+
optional_fraction	= [\.][0-9]+
optional_exponent	= [E][\+-]?[0-9]+
other				= .


%%
/* Lexical Rules */

{comments} {
	/* Do nothing with comments */
}

{keyword_or_id} {
	/* Return token with keyword or id type */
	TokenType lexemeType = ourLookupTable.get(yytext());
	if(lexemeType != null){
		return new Token(yytext(), lexemeType);
	} else {
		return new Token(yytext(), TokenType.id);
	}
}

{num} {
	/* Return token with num type */
	return new Token(yytext(), TokenType.num);
}

{symbols} {
	/* Return token with symbol type */
	return new Token(yytext(), ourLookupTable.get(yytext()));
}

{whitespace} {
	/* Do nothing with whitespace */
}

{other} {
	/* Vomit all over the user */
	throw new InvalidTokenException("Illegal character '" + yytext() + "'", yyline + 1);
}
