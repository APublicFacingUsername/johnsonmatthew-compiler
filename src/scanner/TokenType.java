/*
 *This is software designed by Matthew D Johnson of Augsburg College
 *It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 *For more information, contact johns116@augsburg.edu
 */
package scanner;

/**
 * This is a Java enum that enumerates the types of tokens that are present in
 * the grammar of our MiniPascal language.
 *
 * @author Matt
 */
public enum TokenType {
    /**
     * The list of token types that our language is to have.
     */
    KW_var, KW_while, KW_array, KW_begin, KW_do, KW_else, KW_end, KW_function, KW_if, KW_integer,
    KW_not, KW_of, KW_procedure, KW_program, KW_real, KW_then, SYM_semicolon, SYM_period, SYM_comma,
    KW_write, KW_readint, KW_readfloat, SYM_colon, SYM_lbracket, SYM_rbracket, SYM_lparens, SYM_rparens, id, num, relop,
    addop, mulop, assignop;
}
