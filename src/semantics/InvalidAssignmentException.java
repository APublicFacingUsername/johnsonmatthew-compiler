/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package semantics;

import parser.syntaxtree.AssignmentStatementNode;

/**
 * An exception used to indicate that an assignment statement is invalid.
 *
 * @author Matt
 */
public class InvalidAssignmentException extends Exception {

    private String message;
    private AssignmentStatementNode node;

    /**
     * Constructor that takes a string message to be displayed to the user.
     *
     * @param message the message to give to the user.
     */
    public InvalidAssignmentException(String message) {
        this.message = message;
        this.node = null;
    }

    /**
     * COnstructor that takes the assignment node that caused the error, to be
     * displayed to the user.
     *
     * @param node the assignment node that caused the error.
     */
    public InvalidAssignmentException(AssignmentStatementNode node) {
        this.message = "";
        this.node = node;
    }

    /**
     * Constructor that takes an assignment node and a string message, both to
     * be displayed to the user.
     *
     * @param node the assignment node that caused the error.
     * @param message the message to give to the user.
     */
    public InvalidAssignmentException(AssignmentStatementNode node, String message) {
        this.message = message;
        this.node = node;
    }

    /**
     * Set the node that caused the error.
     *
     * @param node The assignment node that caused the error.
     */
    public void setNode(AssignmentStatementNode node) {
        this.node = node;
    }

    /**
     * A string representation of this error, drawing on the different
     * information used in its construction.
     *
     * @return a string representation of this error.
     */
    @Override
    public String toString() {
        String returnString = "InvalidAssignmentException: ";
        if (node != null) {
            returnString += "The attempted assignment was to the variable: " + node.getLValue().getName() + "\n";
        }
        returnString += message;
        return returnString;
    }
}
