/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringReader;
import org.junit.Test;
import static org.junit.Assert.*;
import parser.syntaxtree.*;
import scanner.InvalidTokenException;
import scanner.Scanner;

/**
 * Parser test suite to ensure that the parser reads and fails files correctly.
 *
 * @author Matt
 */
public class ParserTest {

    public ParserTest() {
    }

    /**
     * Test of program method, of class Parser, using file simplest.pas. The
     * intent of this test is to determine that the parser can read a valid
     * pascal file.
     */
    @Test
    public void testProgram_Simplest() throws Exception {
        String filePath = "MiniPascalSamples/simplest.pas";
        System.out.println("program - using file '" + filePath + "'");
        BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            ProgramNode rootNode = instance.program();
            SymbolTable symbols = instance.getSymbolTable();
            semantics.SemanticAnalyzer.createAssignments(rootNode, symbols);
            semantics.SemanticAnalyzer.consistentAssignments(rootNode, symbols);
            String symbolTable = "Scope: foo\n";
            String syntaxTree = "Program: foo\n"
                    + "|-- Declarations\n"
                    + "|-- SubProgramDeclarations\n"
                    + "|-- Compound Statement\n";
            assertEquals(syntaxTree, rootNode.indentedToString(0, symbols));
            assertEquals(symbolTable, symbols.indentedToString());
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected Token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid Token");
        }
    }

    @Test
    /**
     * Test of program method, of class Parser, using file simple.pas. The
     * intent of this test is to determine that the parser can read a valid
     * pascal file.
     */
    public void testProgram_Simple() throws Exception {
        String filePath = "MiniPascalSamples/simple.pas";
        System.out.println("program - using file '" + filePath + "'");
        BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            ProgramNode rootNode = instance.program();
            SymbolTable symbols = instance.getSymbolTable();
            semantics.SemanticAnalyzer.createAssignments(rootNode, symbols);
            semantics.SemanticAnalyzer.consistentAssignments(rootNode, symbols);
            String syntaxTree = "Program: foo\n"
                    + "|-- Declarations\n"
                    + "|-- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- SubProgramDeclarations\n"
                    + "|-- Compound Statement\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- Value: \"4\" ofType: INTEGER\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- Value: \"5\" ofType: INTEGER\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- Operation: \"+\" ofType: INTEGER\n"
                    + "|-- --- --- --- LVal:\n"
                    + "|-- --- --- --- Operation: \"*\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- Value: \"3\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- RVal:\n"
                    + "|-- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- If\n"
                    + "|-- --- --- Operation: \"<\"\n"
                    + "|-- --- --- --- LVal:\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- RVal:\n"
                    + "|-- --- --- --- Value: \"13\" ofType: INTEGER\n"
                    + "|-- --- Then\n"
                    + "|-- --- --- Assignment\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Value: \"13\" ofType: INTEGER\n"
                    + "|-- --- Else\n"
                    + "|-- --- --- Assignment\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Value: \"26\" ofType: INTEGER\n"
                    + "|-- --- Write Command \n"
                    + "|-- --- --- Expression: fo\n";
            String symbolTable = "Scope: foo\n"
                    + "|-- INTEGER			fi\n"
                    + "|-- INTEGER			fee\n"
                    + "|-- INTEGER			fo\n"
                    + "|-- INTEGER			fum\n";
            assertEquals(syntaxTree, rootNode.indentedToString(0, symbols));
            assertEquals(symbolTable, symbols.indentedToString());
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected Token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid Token");
        }
    }

    /**
     * Test of program method, of class Parser, using file
     * simpleWithSubPrograms.pas. The intent of this test is to determine that
     * the parser can read a valid pascal file.
     */
    @Test
    public void testProgram_simpleWithSubPrograms() throws Exception {
        String filePath = "MiniPascalSamples/simpleWithSubPrograms.pas";
        System.out.println("program - using file '" + filePath + "'");
        BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            ProgramNode rootNode = instance.program();
            SymbolTable symbols = instance.getSymbolTable();
            semantics.SemanticAnalyzer.createAssignments(rootNode, symbols);
            semantics.SemanticAnalyzer.consistentAssignments(rootNode, symbols);
            String symbolTable = "Scope: foo\n"
                    + "|-- INTEGER			fi\n"
                    + "|-- ID_PROCEDURE	giantsYo\n"
                    + "|-- INTEGER			fee\n"
                    + "|-- INTEGER			fo\n"
                    + "|-- INTEGER			fum\n"
                    + "|-- Scope: giantsYo\n"
                    + "|-- --- INTEGER			fi\n"
                    + "|-- --- ID_PROCEDURE	moreGiantsYo\n"
                    + "|-- --- INTEGER			fee\n"
                    + "|-- --- INTEGER			fo\n"
                    + "|-- --- INTEGER			fum\n"
                    + "|-- --- Scope: moreGiantsYo\n"
                    + "|-- --- --- INTEGER			fi\n"
                    + "|-- --- --- ID_PROCEDURE	evenMoreGiantsYo\n"
                    + "|-- --- --- INTEGER			fee\n"
                    + "|-- --- --- INTEGER			fo\n"
                    + "|-- --- --- INTEGER			fum\n"
                    + "|-- --- --- Scope: evenMoreGiantsYo\n"
                    + "|-- --- --- --- INTEGER			fi\n"
                    + "|-- --- --- --- INTEGER			fee\n"
                    + "|-- --- --- --- INTEGER			fo\n"
                    + "|-- --- --- --- INTEGER			fum\n";
            String syntaxTree = "Program: foo\n"
                    + "|-- Declarations\n"
                    + "|-- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- SubProgramDeclarations\n"
                    + "|-- --- Procedure: giantsYo\n"
                    + "|-- --- --- Arguments\n"
                    + "|-- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- Declarations\n"
                    + "|-- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- SubProgramDeclarations\n"
                    + "|-- --- --- --- Procedure: moreGiantsYo\n"
                    + "|-- --- --- --- --- Arguments\n"
                    + "|-- --- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- Declarations\n"
                    + "|-- --- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- SubProgramDeclarations\n"
                    + "|-- --- --- --- --- --- Procedure: evenMoreGiantsYo\n"
                    + "|-- --- --- --- --- --- --- Arguments\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- Declarations\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- SubProgramDeclarations\n"
                    + "|-- --- --- --- --- --- --- Compound Statement\n"
                    + "|-- --- --- --- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- Value: \"4\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- Value: \"5\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- Operation: \"+\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- --- Operation: \"*\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- --- --- Value: \"3\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- If\n"
                    + "|-- --- --- --- --- --- --- --- --- Operation: \"<\"\n"
                    + "|-- --- --- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- --- Value: \"13\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Then\n"
                    + "|-- --- --- --- --- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- --- Value: \"13\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Else\n"
                    + "|-- --- --- --- --- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- --- Value: \"26\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Write Command \n"
                    + "|-- --- --- --- --- --- --- --- --- Expression: fo\n"
                    + "|-- --- --- --- --- Compound Statement\n"
                    + "|-- --- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- Value: \"4\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- Value: \"5\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- Operation: \"+\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- Operation: \"*\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- Value: \"3\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- If\n"
                    + "|-- --- --- --- --- --- --- Operation: \"<\"\n"
                    + "|-- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- Value: \"13\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Then\n"
                    + "|-- --- --- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Value: \"13\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Else\n"
                    + "|-- --- --- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- Value: \"26\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Write Command \n"
                    + "|-- --- --- --- --- --- --- Expression: fo\n"
                    + "|-- --- --- Compound Statement\n"
                    + "|-- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- Value: \"4\" ofType: INTEGER\n"
                    + "|-- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- Value: \"5\" ofType: INTEGER\n"
                    + "|-- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- Operation: \"+\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- Operation: \"*\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- Value: \"3\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- If\n"
                    + "|-- --- --- --- --- Operation: \"<\"\n"
                    + "|-- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- Value: \"13\" ofType: INTEGER\n"
                    + "|-- --- --- --- Then\n"
                    + "|-- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Value: \"13\" ofType: INTEGER\n"
                    + "|-- --- --- --- Else\n"
                    + "|-- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- Value: \"26\" ofType: INTEGER\n"
                    + "|-- --- --- --- Write Command \n"
                    + "|-- --- --- --- --- Expression: fo\n"
                    + "|-- Compound Statement\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- Value: \"4\" ofType: INTEGER\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- Value: \"5\" ofType: INTEGER\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- Operation: \"+\" ofType: INTEGER\n"
                    + "|-- --- --- --- LVal:\n"
                    + "|-- --- --- --- Operation: \"*\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- Value: \"3\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- RVal:\n"
                    + "|-- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- If\n"
                    + "|-- --- --- Operation: \"<\"\n"
                    + "|-- --- --- --- LVal:\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- RVal:\n"
                    + "|-- --- --- --- Value: \"13\" ofType: INTEGER\n"
                    + "|-- --- Then\n"
                    + "|-- --- --- Assignment\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Value: \"13\" ofType: INTEGER\n"
                    + "|-- --- Else\n"
                    + "|-- --- --- Assignment\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Value: \"26\" ofType: INTEGER\n"
                    + "|-- --- Write Command \n"
                    + "|-- --- --- Expression: fo\n";
            assertEquals(syntaxTree, rootNode.indentedToString(0, symbols));
            assertEquals(symbolTable, symbols.indentedToString());
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected Token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid Token");
        }
    }

    /**
     * Test of program method, of class Parser, using file simplest.pas. The
     * intent of this test is to determine that the parser can read a valid
     * pascal file.
     */
    @Test
    public void testProgram_completeSample() throws Exception {
        String filePath = "MiniPascalSamples/completeSample.pas";
        System.out.println("program - using file '" + filePath + "'");
        BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            ProgramNode rootNode = instance.program();
            SymbolTable symbols = instance.getSymbolTable();
            semantics.SemanticAnalyzer.createAssignments(rootNode, symbols);
            semantics.SemanticAnalyzer.consistentAssignments(rootNode, symbols);
            String symbolTable = "Scope: foo\n"
                    + "|-- INTEGER_ARRAY	yarr\n"
                    + "|-- ID_FUNCTION		riffRaff\n"
                    + "|-- INTEGER			fi\n"
                    + "|-- REAL			thumb\n"
                    + "|-- ID_PROCEDURE	giantsYo\n"
                    + "|-- INTEGER			fee\n"
                    + "|-- INTEGER			fo\n"
                    + "|-- REAL			dumb\n"
                    + "|-- ID_FUNCTION		moreGiants\n"
                    + "|-- REAL_ARRAY		jill\n"
                    + "|-- INTEGER			fum\n"
                    + "|-- Scope: giantsYo\n"
                    + "|-- --- INTEGER			fi\n"
                    + "|-- --- INTEGER			fee\n"
                    + "|-- --- INTEGER			fo\n"
                    + "|-- --- ID_PROCEDURE	nothingDoing\n"
                    + "|-- --- INTEGER			fum\n"
                    + "|-- --- Scope: nothingDoing\n"
                    + "|-- --- --- REAL			tra\n"
                    + "|-- --- --- REAL			la\n"
                    + "|-- --- --- REAL			lu\n"
                    + "|-- Scope: moreGiants\n"
                    + "|-- --- REAL_ARRAY		plum\n"
                    + "|-- --- REAL			hum\n"
                    + "|-- --- REAL			fi\n"
                    + "|-- --- REAL			fee\n"
                    + "|-- --- REAL			fo\n"
                    + "|-- --- REAL			dumb\n"
                    + "|-- --- INTEGER			i\n"
                    + "|-- --- REAL			sum\n"
                    + "|-- --- REAL			rum\n"
                    + "|-- --- REAL			gum\n"
                    + "|-- --- REAL_ARRAY		jack\n"
                    + "|-- --- REAL			fum\n"
                    + "|-- Scope: riffRaff\n"
                    + "|-- --- INTEGER			fi\n"
                    + "|-- --- INTEGER			fee\n"
                    + "|-- --- INTEGER			fo\n"
                    + "|-- --- INTEGER			fum\n";
            String syntaxTree = "Program: foo\n"
                    + "|-- Declarations\n"
                    + "|-- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- Variable: \"dumb\" ofType: REAL\n"
                    + "|-- --- Variable: \"thumb\" ofType: REAL\n"
                    + "|-- --- Array: \"jill[0:10]\" ofType: REAL_ARRAY\n"
                    + "|-- --- Array: \"yarr[0:10]\" ofType: INTEGER_ARRAY\n"
                    + "|-- SubProgramDeclarations\n"
                    + "|-- --- Procedure: giantsYo\n"
                    + "|-- --- --- Arguments\n"
                    + "|-- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- Declarations\n"
                    + "|-- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- SubProgramDeclarations\n"
                    + "|-- --- --- --- Procedure: nothingDoing\n"
                    + "|-- --- --- --- --- Arguments\n"
                    + "|-- --- --- --- --- --- Variable: \"tra\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- Variable: \"la\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- Variable: \"lu\" ofType: REAL\n"
                    + "|-- --- --- --- --- Declarations\n"
                    + "|-- --- --- --- --- --- Variable: \"tra\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- Variable: \"la\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- Variable: \"lu\" ofType: REAL\n"
                    + "|-- --- --- --- --- SubProgramDeclarations\n"
                    + "|-- --- --- --- --- Compound Statement\n"
                    + "|-- --- --- Compound Statement\n"
                    + "|-- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- Operation: \"+\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- Operation: \"*\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- Operation: \"*\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- Function: moreGiants\n"
                    + "|-- --- --- Arguments\n"
                    + "|-- --- --- --- Variable: \"fee\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"fi\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"fum\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"rum\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"hum\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"sum\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"gum\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"dumb\" ofType: REAL\n"
                    + "|-- --- --- --- Array: \"plum[0:10]\" ofType: REAL_ARRAY\n"
                    + "|-- --- --- Returns: REAL_ARRAY[0:10]\n"
                    + "|-- --- --- ReturnExpression \n"
                    + "|-- --- --- --- Array: \"jack[0:10]\" ofType: REAL_ARRAY\n"
                    + "|-- --- --- Declarations\n"
                    + "|-- --- --- --- Variable: \"fee\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"fi\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"fum\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"rum\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"hum\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"sum\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"gum\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"dumb\" ofType: REAL\n"
                    + "|-- --- --- --- Array: \"plum[0:10]\" ofType: REAL_ARRAY\n"
                    + "|-- --- --- --- Variable: \"i\" ofType: INTEGER\n"
                    + "|-- --- --- --- Array: \"jack[0:10]\" ofType: REAL_ARRAY\n"
                    + "|-- --- --- SubProgramDeclarations\n"
                    + "|-- --- --- Compound Statement\n"
                    + "|-- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- ArrayIndex: \"jack[0]\" ofType: \"REAL\"\n"
                    + "|-- --- --- --- --- Operation: \"+\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- Operation: \"*\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- Variable: \"rum\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- Operation: \"*\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"hum\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- Operation: \"*\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- Variable: \"sum\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- Operation: \"*\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- --- Variable: \"gum\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- --- Operation: \"*\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- --- --- Variable: \"dumb\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- --- --- Variable: \"fum\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- Operation: \"+\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- Variable: \"fee\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- Operation: \"+\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fi\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- Operation: \"*\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- Variable: \"fo\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- ArrayIndex: \"jill[8]\" ofType: \"REAL\"\n"
                    + "|-- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- Variable: \"i\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- Value: \"1\" ofType: INTEGER\n"
                    + "|-- --- --- --- While Statement\n"
                    + "|-- --- --- --- --- Operation: \"<\"\n"
                    + "|-- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- Variable: \"i\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- Value: \"11\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- Compound Statement\n"
                    + "|-- --- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- --- ArrayIndex: \"jack[i]\" ofType: \"REAL\"\n"
                    + "|-- --- --- --- --- --- --- Operation: \"+\" ofType: REAL\n"
                    + "|-- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- ArrayIndex: \"jack[i - 1]\" ofType: \"REAL\"\n"
                    + "|-- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- ArrayIndex: \"plum[i]\" ofType: \"REAL\"\n"
                    + "|-- --- --- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- --- --- Variable: \"i\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- Operation: \"+\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"i\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- Value: \"1\" ofType: INTEGER\n"
                    + "|-- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- ReturnVariable: \"moreGiants\" ofType: REAL_ARRAY\n"
                    + "|-- --- --- --- --- Array: \"jack[0:10]\" ofType: REAL_ARRAY\n"
                    + "|-- --- Function: riffRaff\n"
                    + "|-- --- --- Arguments\n"
                    + "|-- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- Returns: INTEGER\n"
                    + "|-- --- --- ReturnExpression \n"
                    + "|-- --- --- --- Operation: \"+\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- Operation: \"*\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- Operation: \"*\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- Declarations\n"
                    + "|-- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- SubProgramDeclarations\n"
                    + "|-- --- --- Compound Statement\n"
                    + "|-- --- --- --- Assignment\n"
                    + "|-- --- --- --- --- ReturnVariable: \"riffRaff\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- Operation: \"+\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- Operation: \"*\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- Operation: \"*\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- Compound Statement\n"
                    + "|-- --- Read Command \n"
                    + "|-- --- --- fee:=Hard Determinism\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- Value: \"5\" ofType: INTEGER\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- Operation: \"+\" ofType: INTEGER\n"
                    + "|-- --- --- --- LVal:\n"
                    + "|-- --- --- --- Operation: \"*\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- Value: \"3\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- RVal:\n"
                    + "|-- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Variable: \"thumb\" ofType: REAL\n"
                    + "|-- --- --- Operation: \"*\" ofContextualType: REAL\n"
                    + "|-- --- --- --- LVal:\n"
                    + "|-- --- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- --- RVal:\n"
                    + "|-- --- --- --- Variable: \"fi\" ofContextualType: REAL\"\n"
                    + "|-- --- If\n"
                    + "|-- --- --- Negation \n"
                    + "|-- --- --- --- Negation \n"
                    + "|-- --- --- --- --- Negation \n"
                    + "|-- --- --- --- --- --- Negation \n"
                    + "|-- --- --- --- --- --- --- Negation \n"
                    + "|-- --- --- --- --- --- --- --- Operation: \"<\"\n"
                    + "|-- --- --- --- --- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- --- --- --- --- Value: \"13\" ofType: INTEGER\n"
                    + "|-- --- Then\n"
                    + "|-- --- --- Assignment\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Value: \"13\" ofType: INTEGER\n"
                    + "|-- --- Else\n"
                    + "|-- --- --- Assignment\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Value: \"26\" ofType: INTEGER\n"
                    + "|-- --- Procedure or Function call \n"
                    + "|-- --- --- Procedure name: giantsYo\n"
                    + "|-- --- --- With Args: \n"
                    + "|-- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- Procedure or Function call \n"
                    + "|-- --- --- Procedure name: moreGiants\n"
                    + "|-- --- --- With Args: \n"
                    + "|-- --- --- Variable: \"fee\" ofContextualType: REAL\"\n"
                    + "|-- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- Array: \"jill[0:10]\" ofType: REAL_ARRAY\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Array: \"jill[0:10]\" ofType: REAL_ARRAY\n"
                    + "|-- --- --- Function call \n"
                    + "|-- --- --- --- Function name: moreGiants\n"
                    + "|-- --- --- --- Type: REAL_ARRAY\n"
                    + "|-- --- --- --- With Args: \n"
                    + "|-- --- --- --- Operation: \"+\" ofType: REAL\n"
                    + "|-- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- Value: \"5\" ofContextualType: REAL\n"
                    + "|-- --- --- --- Variable: \"thumb\" ofType: REAL\n"
                    + "|-- --- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- --- Function call \n"
                    + "|-- --- --- --- --- Function name: riffRaff\n"
                    + "|-- --- --- --- --- Contextual type: REAL\n"
                    + "|-- --- --- --- --- With Args: \n"
                    + "|-- --- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- --- Variable: \"fo\" ofContextualType: REAL\"\n"
                    + "|-- --- --- --- Array: \"jill[0:10]\" ofType: REAL_ARRAY\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Variable: \"dumb\" ofType: REAL\n"
                    + "|-- --- --- Function call \n"
                    + "|-- --- --- --- Function name: riffRaff\n"
                    + "|-- --- --- --- Contextual type: REAL\n"
                    + "|-- --- --- --- With Args: \n"
                    + "|-- --- --- --- Variable: \"fee\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fi\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fo\" ofType: INTEGER\n"
                    + "|-- --- --- --- Variable: \"fum\" ofType: INTEGER\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Variable: \"dumb\" ofType: REAL\n"
                    + "|-- --- --- Operation: \"*\" ofType: REAL\n"
                    + "|-- --- --- --- LVal:\n"
                    + "|-- --- --- --- Operation: \"+\" ofContextualType: REAL\n"
                    + "|-- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- Value: \"13\" ofContextualType: REAL\n"
                    + "|-- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- Value: \"12\" ofContextualType: REAL\n"
                    + "|-- --- --- --- RVal:\n"
                    + "|-- --- --- --- ArrayIndex: \"jill[5]\" ofType: \"REAL\"\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Array: \"jill[0:10]\" ofType: REAL_ARRAY\n"
                    + "|-- --- --- Array: \"yarr[0:10]\" ofType: INTEGER_ARRAY\n"
                    + "|-- --- Write Command \n"
                    + "|-- --- --- Expression: dumb\n"
                    + "|-- --- Assignment\n"
                    + "|-- --- --- Variable: \"dumb\" ofType: REAL\n"
                    + "|-- --- --- Operation: \"/\" ofContextualType: REAL\n"
                    + "|-- --- --- --- LVal:\n"
                    + "|-- --- --- --- Operation: \"+\" ofContextualType: REAL\n"
                    + "|-- --- --- --- --- LVal:\n"
                    + "|-- --- --- --- --- Value: \"14\" ofContextualType: REAL\n"
                    + "|-- --- --- --- --- RVal:\n"
                    + "|-- --- --- --- --- Value: \"12\" ofContextualType: REAL\n"
                    + "|-- --- --- --- RVal:\n"
                    + "|-- --- --- --- Value: \"5\" ofContextualType: REAL\n";
            assertEquals(syntaxTree, rootNode.indentedToString(0, symbols));
            assertEquals(symbolTable, symbols.indentedToString());
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected Token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid Token");
        }
    }

    @Test
    /**
     * Test of program method, of class Parser, using file
     * invalidPascal_InvalidTokenException.pas. The intent of this test is to
     * determine that the parser can read a valid pascal file.
     */
    public void testProgram_InvalidTokenException() throws Exception {
        String filePath = "MiniPascalSamples/invalidPascal_InvalidTokenException.pas";
        System.out.println("program - using file '" + filePath + "'");
        BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            instance.program();
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected token, anticipated invalid token.");
        } catch (InvalidTokenException ex) {
            if (!inputScanner.yytext().equalsIgnoreCase("~")) {
                System.out.println(ex.toString());
                fail("Invalid token, but wrong one.");
            } else {
                return;
            }
        }
        fail("No errors caught, anticipated invalid token");
    }

    @Test
    /**
     * Test of program method, of class Parser, using file
     * invalidPascal_UnexpectedTokenException.pas. The intent of this test is to
     * determine that the parser can read a valid pascal file.
     */
    public void testProgram_UnexpectedTokenException() throws Exception {
        String filePath = "MiniPascalSamples/invalidPascal_UnexpectedTokenException.pas";
        System.out.println("program - using file '" + filePath + "'");
        BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            instance.program();
        } catch (UnexpectedTokenException ex) {
            if (!ex.getErrorSource().getLex().equalsIgnoreCase("then")) {
                System.out.println(ex.toString());
                fail("Unexpected token, but wrong one.");
            } else {
                return;
            }
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token, anticipated unexpected token");
        }
        fail("No errors caught, anticipated unexpected token");
    }

    @Test
    /**
     * Test of factor method, of class Parser, using string 'test'.
     */
    public void testFactor_Rule1() throws Exception {
        System.out.println("factor - using string 'someIDHere'");
        BufferedReader inputReader = new BufferedReader(new StringReader("someIDHere"));
        Scanner inputScanner = new Scanner(inputReader);
        SymbolTable ourTable = new SymbolTable("placeholder");
        ourTable.insertID("someIDHere", Type.INTEGER);
        Parser instance = new Parser(inputScanner, ourTable);
        try {
            instance.factor();
        } catch (UnexpectedTokenException ex) {
            if (!ex.toString().contains("Encountered EOF marker sooner than expected.")) {
                System.out.println(ex.toString());
                fail("Unexpected token");
            }
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of factor method, of class Parser, using string 'test[15+16]'.
     */
    public void testFactor_Rule2() throws Exception {
        System.out.println("factor - using string 'vartest[15+16]'");
        BufferedReader inputReader = new BufferedReader(new StringReader("test[15+16]"));
        Scanner inputScanner = new Scanner(inputReader);
        SymbolTable ourTable = new SymbolTable("placeholder");
        ourTable.insertID("test", Type.INTEGER_ARRAY);
        ourTable.setObjectReference("test", new ArrayVariableNode("test", "0", "32", parser.Type.INTEGER_ARRAY));
        Parser instance = new Parser(inputScanner, ourTable);
        try {
            instance.factor();
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of factor method, of class Parser, using string 'test(20-16)'.
     */
    public void testFactor_Rule3() throws Exception {
        System.out.println("factor - using string 'test(20-16)'");
        BufferedReader inputReader = new BufferedReader(new StringReader("test(20-16)"));
        Scanner inputScanner = new Scanner(inputReader);
        SymbolTable ourTable = new SymbolTable("placeholder");
        ourTable.insertID("test", Type.ID_FUNCTION);
        FunctionNode funcNode = new FunctionNode("test");
        ArgumentsNode argNode = new ArgumentsNode();
        argNode.addVariable(new RegularVariableNode("hellsbells", Type.INTEGER));
        funcNode.setArguments(argNode);
        ourTable.setObjectReference("test", funcNode);
        Parser instance = new Parser(inputScanner, ourTable);
        try {
            instance.factor();
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of factor method, of class Parser, using string '14.0629E-100'.
     */
    public void testFactor_Rule4() throws Exception {
        System.out.println("factor - using string '14.0629E-100'");
        BufferedReader inputReader = new BufferedReader(new StringReader("14.0629E-100"));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            instance.factor();
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of factor method, of class Parser, using string '(200+42+42)'.
     */
    public void testFactor_Rule5() throws Exception {
        System.out.println("factor - using string '(200+42+42)'");
        BufferedReader inputReader = new BufferedReader(new StringReader("(200+42+42)"));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            instance.factor();
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of factor method, of class Parser, using string 'not not not not
     * helloThere(42)'.
     */
    public void testFactor_Rule6() throws Exception {
        System.out.println("factor - using string 'not (not (not (not (helloThere(42)))))'");
        BufferedReader inputReader = new BufferedReader(new StringReader("not not (not (not (helloThere(42) > 2))))"));
        Scanner inputScanner = new Scanner(inputReader);
        SymbolTable ourTable = new SymbolTable("placeholder");
        FunctionNode funcNode = new FunctionNode("test");
        ArgumentsNode argNode = new ArgumentsNode();
        argNode.addVariable(new RegularVariableNode("hellsbells", Type.INTEGER));
        funcNode.setArguments(argNode);
        ourTable.insertID("helloThere", Type.ID_FUNCTION);
        ourTable.setObjectReference("helloThere", funcNode);
        Parser instance = new Parser(inputScanner, ourTable);
        try {
            instance.factor();
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of expression method, of class Parser, using string
     * '(42+42-42*42)/42'.
     */
    public void testExpression_Rule1() throws Exception {
        System.out.println("expression - using string '(42+42-42*42)/42'");
        BufferedReader inputReader = new BufferedReader(new StringReader("(42+42-42*42)/42"));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            instance.expression();
        } catch (UnexpectedTokenException ex) {
            if (!ex.toString().contains("Encountered EOF marker sooner than expected.")) {
                System.out.println(ex.toString());
                fail("Unexpected token");
            }
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of expression method, of class Parser, using string
     * '(42+42-42*42)/42 <= 42'.
     */
    public void testExpression_Rule2() throws Exception {
        System.out.println("expression - using string '(42+42-42*42)/42 <= 42'");
        BufferedReader inputReader = new BufferedReader(new StringReader("(42+42-42*42)/42 <= 42"));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            instance.expression();
        } catch (UnexpectedTokenException ex) {
            if (!ex.toString().contains("Encountered EOF marker sooner than expected.")) {
                System.out.println(ex.toString());
                fail("Unexpected token");
            }
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of simplePart method, of class Parser, using the string
     * '+13-13+13+42'.
     */
    public void testSimplePart_Rule1() throws Exception {
        System.out.println("simplePart - using the string '+13-13+13+42'");
        BufferedReader inputReader = new BufferedReader(new StringReader("+13-13+13+42"));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            instance.simplePart();
        } catch (UnexpectedTokenException ex) {
            if (!ex.toString().contains("Encountered EOF marker sooner than expected.")) {
                System.out.println(ex.toString());
                fail("Unexpected token");
            }
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of simplePart method, of class Parser, using the empty string.
     */
    public void testSimplePart_Rule2() throws Exception {
        System.out.println("simplePart - using the empty string");
        BufferedReader inputReader = new BufferedReader(new StringReader(""));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            instance.simplePart();
        } catch (UnexpectedTokenException ex) {
            if (!ex.toString().contains("Encountered EOF marker sooner than expected.")) {
                System.out.println(ex.toString());
                fail("Unexpected token");
            }
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of simpleExpression method, of class Parser, using the string
     * 'testID*testID*testID*testID+testID-testID'.
     */
    public void testSimpleExpression_Rule1() throws Exception {
        System.out.println("simpleExpression - using the string 'testID*testID*testID*testID+testID-testID'");
        BufferedReader inputReader = new BufferedReader(new StringReader("testID*testID*testID*testID+testID-testID"));
        Scanner inputScanner = new Scanner(inputReader);
        SymbolTable ourTable = new SymbolTable("placeholder");
        ourTable.insertID("testID", Type.REAL);
        Parser instance = new Parser(inputScanner, ourTable);
        try {
            instance.simpleExpression();
        } catch (UnexpectedTokenException ex) {
            if (!ex.toString().contains("Encountered EOF marker sooner than expected.")) {
                System.out.println(ex.toString());
                fail("Unexpected token");
            }
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of simpleExpression method, of class Parser, using the string
     * '+testID*testID*testID*testID+testID-testID'.
     */
    public void testSimpleExpression_Rule2() throws Exception {
        System.out.println("simpleExpression - using the string '+testID*testID*testID*testID+testID-testID'");
        BufferedReader inputReader = new BufferedReader(new StringReader("+testID*testID*testID*testID+testID-testID"));
        Scanner inputScanner = new Scanner(inputReader);
        SymbolTable ourTable = new SymbolTable("placeholder");
        ourTable.insertID("testID", Type.REAL);
        Parser instance = new Parser(inputScanner, ourTable);
        try {
            instance.simpleExpression();
        } catch (UnexpectedTokenException ex) {
            if (!ex.toString().contains("Encountered EOF marker sooner than expected.")) {
                System.out.println(ex.toString());
                fail("Unexpected token");
            }
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of simpleExpression method, of class Parser, using the string 'test
     * := 42+22+100'.
     */
    public void testStatement_Rule1() throws Exception {
        System.out.println("statement - using the string 'test := 42+22+100'");
        BufferedReader inputReader = new BufferedReader(new StringReader("test := 42+22+100"));
        Scanner inputScanner = new Scanner(inputReader);
        SymbolTable ourTable = new SymbolTable("placeholder");
        ourTable.insertID("test", Type.INTEGER);
        ourTable.setObjectReference("test", new RegularVariableNode("test", Type.INTEGER));
        Parser instance = new Parser(inputScanner, ourTable);
        try {
            instance.statement();
        } catch (UnexpectedTokenException ex) {
            if (!ex.toString().contains("Encountered EOF marker sooner than expected.")) {
                System.out.println(ex.toString());
                //ex.printStackTrace();
                fail("Unexpected token");
            }
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of statement method, of class Parser, using the string
     * 'test[testing123]'.
     */
    public void testStatement_Rule2() throws Exception {
        System.out.println("statement - using the string 'test[testing123] := 55'");
        BufferedReader inputReader = new BufferedReader(new StringReader("test[testing123] := 55"));
        Scanner inputScanner = new Scanner(inputReader);
        SymbolTable ourTable = new SymbolTable("placeholder");
        ourTable.insertID("test", Type.INTEGER_ARRAY);
        ourTable.insertID("testing123", Type.INTEGER);
        ourTable.setObjectReference("test", new ArrayVariableNode("test", "0", "32", parser.Type.INTEGER_ARRAY));
        Parser instance = new Parser(inputScanner, ourTable);
        try {
            instance.statement();
        } catch (UnexpectedTokenException ex) {
            if (!ex.toString().contains("Encountered EOF marker sooner than expected.")) {
                System.out.println(ex.toString());
                fail("Unexpected token");
            }
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of statement method, of class Parser, using the string
     * 'test(testing123)'.
     */
    public void testStatement_Rule3() throws Exception {
        System.out.println("statement - using the string 'test(testing123)'");
        BufferedReader inputReader = new BufferedReader(new StringReader("test(testing123)"));
        Scanner inputScanner = new Scanner(inputReader);
        SymbolTable ourTable = new SymbolTable("placeholder");
        ourTable.insertID("test", Type.ID_PROCEDURE);
        ProcedureNode procNode = new ProcedureNode("test");
        ArgumentsNode argNode = new ArgumentsNode();
        argNode.addVariable(new RegularVariableNode("hellsbells", Type.INTEGER));
        procNode.setArguments(argNode);
        ourTable.setObjectReference("test", procNode);
        ourTable.insertID("testing123", Type.INTEGER);
        ourTable.setObjectReference("testing123", new RegularVariableNode("testing123", Type.INTEGER));
        Parser instance = new Parser(inputScanner, ourTable);
        try {
            instance.statement();
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of statement method, of class Parser, using the string 'if fo < 13
     * then fo := 13 else fo := 26 ;]'.
     */
    public void testStatement_Rule4() throws Exception {
        System.out.println("statement - using the string 'if fo < 13\\n"
                + "    then\\n"
                + "      fo := 13\\n"
                + "    else\\n"
                + "      fo := 26\\n"
                + "  ;'");
        BufferedReader inputReader = new BufferedReader(new StringReader("if fo < 13\n"
                + "    then\n"
                + "      fo := 13\n"
                + "    else\n"
                + "      fo := 26\n"
                + "  ;"));
        Scanner inputScanner = new Scanner(inputReader);
        SymbolTable ourTable = new SymbolTable("placeholder");
        ourTable.insertID("fo", Type.INTEGER);
        ourTable.setObjectReference("fo", new RegularVariableNode("fo", Type.INTEGER));
        Parser instance = new Parser(inputScanner, ourTable);
        try {
            instance.statement();
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of statement method, of class Parser, using the string ' while fo <
     * 30 do fo := fo + 1 ;'.
     */
    public void testStatement_Rule5() throws Exception {
        System.out.println("statement - using the string '  while fo < 30 do\\n"
                + "	fo := fo + 1\\n"
                + "  ;'");
        BufferedReader inputReader = new BufferedReader(new StringReader("  while fo < 30 do\n"
                + "	fo := fo + 1\n"
                + "  ;"));
        Scanner inputScanner = new Scanner(inputReader);
        SymbolTable ourTable = new SymbolTable("placeholder");
        ourTable.insertID("fo", Type.INTEGER);
        ourTable.setObjectReference("fo", new RegularVariableNode("fo", Type.INTEGER));
        Parser instance = new Parser(inputScanner, ourTable);
        try {
            instance.statement();
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of statement method, of class Parser, using the string
     * 'read(someidhere)'
     */
    public void testStatement_Rule6() throws Exception {
        System.out.println("statement - using the string 'read(someidhere)'");
        BufferedReader inputReader = new BufferedReader(new StringReader("read(someidhere)"));
        Scanner inputScanner = new Scanner(inputReader);
        SymbolTable ourTable = new SymbolTable("placeholder");
        ourTable.insertID("someidhere", Type.INTEGER);
        ourTable.setObjectReference("someidhere", new RegularVariableNode("someidhere", Type.INTEGER));
        Parser instance = new Parser(inputScanner, ourTable);
        try {
            instance.statement();
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }

    @Test
    /**
     * Test of statement method, of class Parser, using the string
     * 'write((42+42-42*42)/42 <= 42)'
     */
    public void testStatement_Rule7() throws Exception {
        System.out.println("statement - using the string 'write((42+42-42*42)/42 <= 42)'");
        BufferedReader inputReader = new BufferedReader(new StringReader("write((42+42-42*42)/42 <= 42)"));
        Scanner inputScanner = new Scanner(inputReader);
        Parser instance = new Parser(inputScanner);
        try {
            instance.statement();
        } catch (UnexpectedTokenException ex) {
            System.out.println(ex.toString());
            fail("Unexpected token");
        } catch (InvalidTokenException ex) {
            System.out.println(ex.toString());
            fail("Invalid token");
        }
    }
}
