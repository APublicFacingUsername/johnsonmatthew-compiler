/*
 * This is software designed by Matthew D Johnson of Augsburg College
 * It is part of a project to build a compiler for the course CSC 451, spring semester 2017
 * For more information, contact johns116@augsburg.edu
 */
package scanner;

import java.io.BufferedReader;
import java.io.FileReader;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * A test for the class Scanner in the package scanner. Tests specifically the
 * yytext() method and the nextToken() function.
 *
 * @author Matt
 */
public class ScannerTest {

    public ScannerTest() {
    }

    /**
     * Test of yytext method, of class Scanner, using file simplest.pas. The
     * intent of this test is to ensure the scanner can read a valid MiniPascal
     * file
     *
     * @throws java.io.IOException An IO Exception detected by the VM
     */
    @Test
    public void testyytext_Simplest() throws java.io.IOException {
        try {
            String filePath = "MiniPascalSamples/simplest.pas";
            System.out.println("yytext - using file '" + filePath + "'");
            BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
            Scanner instance = new Scanner(inputReader);

            String expResult = "";
            String result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();

            expResult = "program";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();

            expResult = "foo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();

            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();

            expResult = "begin";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();

            expResult = "end";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();

            expResult = ".";
            result = instance.yytext();
            assertEquals(expResult, result);

            expResult = null;
            Token tResult = instance.nextToken();
            assertEquals(expResult, tResult);

            instance.yyclose();
            instance = null;
            inputReader.close();
            inputReader = null;
        } catch (InvalidTokenException ex) {
            fail("Invalid token in unexpected location\n" + ex.toString());
        }
    }

    /**
     * Test of the yytext method, of class Scanner, using file simple.pas. The
     * intent of this test is to ensure the scanner can read a valid MiniPascal
     * file of slight complexity.
     *
     * @throws java.io.IOException An IO Exception detected by the VM
     */
    @Test
    public void testyytext_Simple() throws java.io.IOException {
        try {
            String filePath = "MiniPascalSamples/simple.pas";
            System.out.println("yytext - using file '" + filePath + "'");
            BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
            Scanner instance = new Scanner(inputReader);
            String expResult = "";
            String result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "program";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "foo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "var";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fee";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ",";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fi";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ",";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ",";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fum";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "integer";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "begin";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fee";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "4";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fi";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "5";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "3";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "*";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fee";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "+";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fi";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "if";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "<";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "13";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "then";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "13";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "else";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "26";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "write";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "(";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ")";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "end";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ".";
            result = instance.yytext();
            assertEquals(expResult, result);
            expResult = null;
            Token tResult = instance.nextToken();
            assertEquals(expResult, tResult);
            instance.yyclose();
            instance = null;
            inputReader.close();
            inputReader = null;
        } catch (InvalidTokenException ex) {
            fail("Invalid token in unexpected location\n" + ex.toString());
        }
    }

    /**
     * Test of the yytext method, of class Scanner, using file
     * invalidPascal_InvalidTokenException.pas. The intent of this test is to
     * ensure yytext functions correctly when an invalid token is found.
     *
     * @throws java.io.IOException An IO Exception detected by the VM
     */
    @Test
    public void testyytext_InvalidTokenException() throws java.io.IOException {
        try {
            String filePath = "MiniPascalSamples/invalidPascal_InvalidTokenException.pas";
            System.out.println("yytext - using file '" + filePath + "'");
            BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
            Scanner instance = new Scanner(inputReader);
            String expResult = "";
            String result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "program";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "foo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "var";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fee";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ",";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fi";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ",";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ",";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fum";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "integer";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "begin";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fee";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "4";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fi";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "5";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "3";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "*";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fee";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "+";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fi";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "if";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "<";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "13";
            result = instance.yytext();
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "~";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "`";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "!";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "@";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "#";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "$";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "%";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "^";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "&";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "_";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "|";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "\\";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "'";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "\"";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            try {
                instance.nextToken();
            } catch (InvalidTokenException ex) {
                expResult = "?";
                result = instance.yytext();
            }
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "then";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "13";
            instance.nextToken();
            expResult = "else";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "26";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "write";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "(";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "fo";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ")";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "end";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ".";
            result = instance.yytext();
            assertEquals(expResult, result);
            expResult = null;
            Token tResult = instance.nextToken();
            assertEquals(expResult, tResult);
            instance.yyclose();
            instance = null;
            inputReader.close();
            inputReader = null;
        } catch (InvalidTokenException ex) {
            fail("Invalid token in unexpected location\n" + ex.toString());
        }
    }

    /**
     * Test of the yytext method, of class Scanner, using file
     * invalidPascal_AllLexemes.pas. The intent of this test is to make sure the
     * scanner picks up all valid lexemes.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testyytext_AllLexemes() throws java.io.IOException {
        try {
            String filePath = "MiniPascalSamples/invalidPascal_AllLexemes.pas";
            System.out.println("yytext - using file '" + filePath + "'");
            BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
            Scanner instance = new Scanner(inputReader);
            String expResult = "";
            String result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "<>";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "<";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "<=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ">=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ">";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "+";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "-";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "or";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "*";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "/";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "and";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "mod";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "div";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":=";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ";";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ".";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ",";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ":";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "[";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "]";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "(";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = ")";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "array";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "begin";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "do";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "else";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "end";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "function";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "if";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "integer";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "not";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "of";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "procedure";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "program";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "real";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "then";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "var";
            result = instance.yytext();
            assertEquals(expResult, result);
            instance.nextToken();
            expResult = "while";
            result = instance.yytext();
            assertEquals(expResult, result);
            expResult = null;
            Token tResult = instance.nextToken();
            assertEquals(expResult, tResult);
            instance.yyclose();
            instance = null;
            inputReader.close();
            inputReader = null;
        } catch (InvalidTokenException ex) {
            fail("Invalid token in unexpected location\n" + ex.toString());
        }
    }

    /**
     * Test of nextToken method, of class Scanner, using file simplest.pas. The
     * intent of this test is to ensure the scanner can read a valid MiniPascal
     * file
     *
     * @throws java.io.IOException An IO exception detected by the VM
     */
    @Test
    public void testNextToken_Simplest() throws java.io.IOException {
        try {
            String filePath = "MiniPascalSamples/simplest.pas";
            System.out.println("nextToken - using file '" + filePath + "'");
            BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
            Scanner instance = new Scanner(inputReader);
            Token expResult = new Token("program", TokenType.KW_program);
            Token result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("foo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("begin", TokenType.KW_begin);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("end", TokenType.KW_end);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(".", TokenType.SYM_period);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = null;
            result = instance.nextToken();
            assertEquals(expResult, result);
            instance.yyclose();
            instance = null;
            inputReader.close();
            inputReader = null;
        } catch (InvalidTokenException ex) {
            fail("Invalid token in unexpected location\n" + ex.toString());
        }
    }

    /**
     * Test of nextToken method, of class Scanner, using file simple.pas. The
     * intent of this test is to ensure the scanner can read a valid MiniPascal
     * file of slight complexity.
     *
     * @throws java.io.IOException An IO exception detected by the VM
     */
    @Test
    public void testNextToken_Simple() throws java.io.IOException {
        try {
            String filePath = "MiniPascalSamples/simple.pas";
            System.out.println("nextToken - using file '" + filePath + "'");
            BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
            Scanner instance = new Scanner(inputReader);
            Token expResult = new Token("program", TokenType.KW_program);
            Token result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("foo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("var", TokenType.KW_var);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fee", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(",", TokenType.SYM_comma);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fi", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(",", TokenType.SYM_comma);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(",", TokenType.SYM_comma);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fum", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":", TokenType.SYM_colon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("integer", TokenType.KW_integer);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("begin", TokenType.KW_begin);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fee", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":=", TokenType.assignop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("4", TokenType.num);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fi", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":=", TokenType.assignop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("5", TokenType.num);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":=", TokenType.assignop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("3", TokenType.num);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("*", TokenType.mulop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fee", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("+", TokenType.addop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fi", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("if", TokenType.KW_if);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("<", TokenType.relop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("13", TokenType.num);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("then", TokenType.KW_then);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":=", TokenType.assignop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("13", TokenType.num);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("else", TokenType.KW_else);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":=", TokenType.assignop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("26", TokenType.num);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("write", TokenType.KW_write);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("(", TokenType.SYM_lparens);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(")", TokenType.SYM_rparens);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("end", TokenType.KW_end);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(".", TokenType.SYM_period);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = null;
            result = instance.nextToken();
            assertEquals(expResult, result);
            instance.yyclose();
            instance = null;
            inputReader.close();
            inputReader = null;
        } catch (InvalidTokenException ex) {
            fail("Invalid token in unexpected location\n" + ex.toString());
        }
    }

    /**
     * Test of nextToken method, of class Scanner, using file
     * invalidPascal_InvalidTokenException.pas. The intent of this test is to
     * ensure nextToken functions correctly when an invalid token is found.
     *
     * @throws java.io.IOException An IO exception detected by the VM
     */
    @Test
    public void testNextToken_InvalidTokenException() throws java.io.IOException {
        try {
            boolean bCaughtException = false;
            String filePath = "MiniPascalSamples/invalidPascal_InvalidTokenException.pas";
            System.out.println("nextToken - using file '" + filePath + "'");
            BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
            Scanner instance = new Scanner(inputReader);
            Token expResult = new Token("program", TokenType.KW_program);
            Token result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("foo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("var", TokenType.KW_var);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fee", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(",", TokenType.SYM_comma);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fi", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(",", TokenType.SYM_comma);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(",", TokenType.SYM_comma);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fum", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":", TokenType.SYM_colon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("integer", TokenType.KW_integer);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("begin", TokenType.KW_begin);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fee", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":=", TokenType.assignop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("4", TokenType.num);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fi", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":=", TokenType.assignop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("5", TokenType.num);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":=", TokenType.assignop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("3", TokenType.num);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("*", TokenType.mulop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fee", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("+", TokenType.addop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fi", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("if", TokenType.KW_if);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("<", TokenType.relop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("13", TokenType.num);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                assertEquals(expResult.getLex(), result.getLex());
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            try {
                result = instance.nextToken();
            } catch (InvalidTokenException ex) {
                bCaughtException = true;
            }
            if (bCaughtException == false) {
                fail("Did not catch invalid character");
            }
            bCaughtException = false;
            expResult = new Token("then", TokenType.KW_then);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":=", TokenType.assignop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("13", TokenType.num);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("else", TokenType.KW_else);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":=", TokenType.assignop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("26", TokenType.num);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("write", TokenType.KW_write);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("(", TokenType.SYM_lparens);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("fo", TokenType.id);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(")", TokenType.SYM_rparens);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("end", TokenType.KW_end);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(".", TokenType.SYM_period);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = null;
            result = instance.nextToken();
            assertEquals(expResult, result);
            instance.yyclose();
            instance = null;
            inputReader.close();
            inputReader = null;
        } catch (InvalidTokenException ex) {
            fail("Invalid token in unexpected location\n" + ex.toString());
        }
    }

    /**
     * Test of nextToken method, of class Scanner, using file
     * invalidPascal_AllLexemes.pas. The intent of this test is to make sure the
     * scanner picks up all valid lexemes.
     *
     * @throws java.io.IOException An IO exception detected by the VM
     */
    @Test
    public void testNextToken_AllLexemes() throws java.io.IOException {
        try {
            String filePath = "MiniPascalSamples/invalidPascal_AllLexemes.pas";
            System.out.println("nextToken - using file '" + filePath + "'");
            BufferedReader inputReader = new BufferedReader(new FileReader(filePath));
            Scanner instance = new Scanner(inputReader);
            Token expResult = new Token("=", TokenType.relop);
            Token result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("<>", TokenType.relop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("<", TokenType.relop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("<=", TokenType.relop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(">=", TokenType.relop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(">", TokenType.relop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("+", TokenType.addop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("-", TokenType.addop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("or", TokenType.addop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("*", TokenType.mulop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("/", TokenType.mulop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("and", TokenType.mulop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("mod", TokenType.mulop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("div", TokenType.mulop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":=", TokenType.assignop);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(";", TokenType.SYM_semicolon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(".", TokenType.SYM_period);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(",", TokenType.SYM_comma);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(":", TokenType.SYM_colon);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("[", TokenType.SYM_lbracket);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("]", TokenType.SYM_rbracket);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("(", TokenType.SYM_lparens);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token(")", TokenType.SYM_rparens);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("array", TokenType.KW_array);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("begin", TokenType.KW_begin);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("do", TokenType.KW_do);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("else", TokenType.KW_else);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("end", TokenType.KW_end);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("function", TokenType.KW_function);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("if", TokenType.KW_if);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("integer", TokenType.KW_integer);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("not", TokenType.KW_not);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("of", TokenType.KW_of);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("procedure", TokenType.KW_procedure);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("program", TokenType.KW_program);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("real", TokenType.KW_real);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("then", TokenType.KW_then);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("var", TokenType.KW_var);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = new Token("while", TokenType.KW_while);
            result = instance.nextToken();
            assertEquals(expResult.getLex(), result.getLex());
            assertEquals(expResult.getType(), result.getType());
            expResult = null;
            result = instance.nextToken();
            assertEquals(expResult, result);
            instance.yyclose();
            instance = null;
            inputReader.close();
            inputReader = null;
        } catch (InvalidTokenException ex) {
            fail("Invalid token in unexpected location\n" + ex.toString());
        }
    }
}
