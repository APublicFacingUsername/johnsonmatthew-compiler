program foo;
var fee, fi, fo, fum: integer;
var dumb, thumb: real;
var jill : array[0:10] of real;
var yarr : array[0:10] of integer;
procedure giantsYo (fee, fi, fo, fum : integer);
	{test nested subprograms}
	procedure nothingDoing(tra, la, lu : real);
		begin
		end
		;
	begin
	  fum := (fee * fo * fi) + fum
	end
	;
function moreGiants (fee, fi, fo, fum, rum, hum, sum, gum, dumb : real;
						plum : array[0:10] of real) : array[0:10] of real;
	var i : integer;
	var jack : array[0:10] of real;
	begin
	  {test assignment of array indicies}
	  jack[0] := (rum * hum * sum * gum * dumb * fum) + fee + fi + fo * jack[8];
	  i := 1;
	  {test while statements}
	  while (i < 11)
	  do
	    begin
			jack[i] :=  jack[i + 1] + plum[i];
			i := i + 1
		end
		;
      {test return values}
	  moreGiants := jack
	end
	;
function riffRaff (fee, fi, fo, fum : integer) : integer;
	begin
		riffRaff := fee * fi * fo + fum
	end
	;
begin
  readint(fee);
  fi := 5;
  fo := 3 * fee + fi;
  thumb := fo * fi;
  {test if statements}
  if not not (not not not(fo < 13))
    then
      fo := 13
    else
      fo := 26
  ;
  {statement to test procedure and function calls}
  giantsYo(fee, fi, fo, fum);
  moreGiants(fee, fo, fo, fo, fo, fo, fo, fo, fo, jill);
  {statement to test return values from functions and array assignments}
  jill := moreGiants(fo + 5, thumb, fo, fo, riffRaff(fee, fi, fo, fum), fo, fo, fo, fo, jill);
  dumb := riffRaff(fee, fi, fo, fum);
  {some statements to test casting}
  dumb := (13 + 12) * jill[5];
  jill := yarr;
  write( dumb);
  dumb := (14 + 12) / 5
end
.