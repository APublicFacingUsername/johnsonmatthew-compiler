<!DOCTYPE html>

<body style="max-width:1080px">

<head>
<title>MJohnsonCompiler</title>
<h1>Compiler Project</h1>
<p>
Author: Matthew Johnson
<br>
Version: 0.1
<br>
Description: This software is the core project for the course CSC 451 - Compilers at Augsburg College, Spring Semester 2017
<br>
Info: This software is implemented in this repository using <span style="font-weight:bold">java</span>
</p>

<body>
<h2>1. Overview</h2>
<div class="overview-margin" style="margin:20px">
<p>
This software is intended to compile the high level language of MiniPascal to the assembly language of MIPS.
<br>The MiniPascal language is specified in the Documentation folder of this repository, specifically in the file named <a href="file:./MiniPascal-Grammar.pdf">MiniPascal-Grammar.pdf</a>.
</p>
<p>
This software is to be constructed in several steps.
</p>
<div class="paragraph-margin" style="margin:20px">
<p>
The first of these steps is the creation of the Scanner package. The Scanner package will tokenize the contents of a file, changing individual "words" from the file into a token that we can work with.
</p>
<p>
The second is the creation of the Parser package. The Parser will determine if the scanned file is valid according to our MiniPascal grammar.
</p>
<p>
The third is the semantic analysis, which will provide type assignments that are missing and check that type assignments are consistent given their context.
</p>
<p>
The fourth and final step is the code generation, in which we will actually output our parsed and now compiled data to an assembly file.
</p>
</div>
</div>
<h2>2. Specifications</h2>
<div class="specifications-margin" style="margin:20px">
<h3>A. Scanner Package</h3>
<div class="paragraph-margin" style="margin:20px">
<p>
The scanner package is to contain several objects. The most notable of these objects is the Scanner object itself.
This object will be specified using the language JFlex, specifically the version of JFlex designated as version 1.6.1.
The JFlex can be parsed and transformed into Java code, which will then be a Java implementation of a state machine that corresponds to our <a href="file:./MiniPascal-Grammar.pdf">MiniPascal grammar</a>.
</p>
<p>
The following is a graphical depiction of the state machine that our JFlex implements:
</p>
<img src="MJohnson-StateMachine.png" width="1040">
<p>
For instructions regarding the parsing of JFlex into Java, please see the JFlex file included in this repository.
</p>
<p>
This Scanner object created will draw on several resources. The first of these is the Token object.
The Token object is one that contains both a String to represent the lexeme for that token, and a TokenType variable to represent what type of token that lexeme corresponds to. 
Via the Token object, the Scanner will also draw on the TokenType enum.
This enum is a listing of all the different types of tokens that we can create in our MiniPascal language. We will use this when creating Token objects.
When the Scanner object's method named "nextToken()" will utilize the LookupTable resource.
This resource is an extension of HashMap<String, TokenType> and will allow us to lookup what value in our TokenType enum corresponds to the lexeme we have found.
When we have established what TokenType to assign to the lexeme, we will then create a Token object using the lexeme and TokenType as inputs to the constructor.
Should we happen to find a lexeme that does not conform to the rules stated in our MiniPascal grammar, we will throw a custom exception.
This exception is named InvalidTokenException and will have a constructors for just a message, and for a message as well as the line number of the exception.
This exception will also have a toString method which will return either the message or the message and the line number where the exception was encountered.
It should be noted that this exception is thrown up the method stack to whatever has called on the scanner.Scanner object.
</div>
<h3>B. Parser Package</h3>
<div class="paragraph-margin" style="margin:20px">
<p>
The parser package is to contain several objects. The most notable of these objects is the Parser object itself.
We will implement this object by following the grammar as listed in our documentation file <a href="file:./MiniPascal-Grammar.pdf">MiniPascal grammar</a>. 
This implementation is to have a method named "match()" which we will utilize to determine if a terminal symbol has been found in its expected position. These matches are to be done using the TokenType enum.
The implementation will operate by using match() on terminal symbols of the grammar (bold in our documentation file) and calling methods that represent non terminal symbols of the grammar.
Each non terminal symbol, or production, will have its own method except in the case of the productions listed in the grammar as "variable" and "procedure_statement". This exception is to be able to handle the ambiguous references caused by these two productions.
At the appropriate points in the process flow of the interlinked methods we will insert into the symbol table the type of ID that we have come across. This is to be done in the "program", "identifier_list", and "subprogram_head" methods.
</p>
<h4>Symbol table</h4>
<div class="paragraph-margin" style="margin:20px">
<p>
The symbol table is to be implemented as a stack of HashMaps. These HashMaps should have keys that are String representing the lexeme of an ID and a value of a data structure named Info.
The Info data structure should have several values enclosed in it, the first being the lexeme, the second being a value of a private enum named "Kind".
The Kind enum is to have 4 types: "ID_PROGAM", "ID_FUNCTION", "ID_VARIABLE", and "ID_PROCESS".
The symbol table should have the functionality to push and pop HashMaps from the stack. This pushing and popping represents the change in scope that occurs when we enter into new functions and processes in a MiniPascal program.
The symbol table should have the functionality to insert (only ever into the top most HashMap of the stack) values based on their Kind; this may be done either by having a method that accepts only a String as an argument (meaning 4 methods would be necessary), or by having a method that accepts both a String and a Kind (as of the enum).
The symbol table should also have the functionality to check if an ID is present in the local (top of the stack) or global (bottom of the stack) scopes. This may also be implemented with a String argument and with or without a Kind argument.
</p>
</div>
<h4>Syntax tree</h4>
<div class="paragraph-margin" style="margin:20px">
<p>
The syntax tree package is to be a subpackage of the main parser package. It will contain a variety of different objects, each of which is intended to represent some portion of the code that has been passed into the parser.
These objects are to be referred to as Nodes, each Node forming a part of the tree structure that we will use to do semantic analysis. 
These Nodes will contain references either to terminal symbols as in the grammar or non-terminal symbols as in the grammar. In the case of terminal symbols, the lexemes of those symbols will be stored as Strings. In the case of non-terminal symbols, the symbols will be stored as another Node object.
</p>
<p>
See the below image that describes both the object heirarchies as well as the UML specificiation of each object.
Please note that the objects SyntaxTreeNode, StatementNode, and ExpressionNode are all abstract classes.
</p>
</div>
</div>
<img src="MJohnson-SyntaxTree.png" width="2080">
<h3>C. Semantic Analysis</h3>
<div class="paragraph-margin" style="margin:20px">
<p>
The Semantic Analysis is to be a stand alone class that contains several functions, contained in a eponymous package. There may be intermediate functions that carry out portions of the main functions of the Semantic Analyzer that are not listed here.
There are three core functions to the Sematic Analysis package. The first function is to find any expressions or statements that ought to have a type but don't. When these expressions or statements are encoutnered they are to be assigned an appropriate type based on the context in which they exist.
The second function is to check that each expression and statement node has appropriate type assignments given the context in which they exist.
The third function is to check that array bounds are not broken by references to the array, and also to check that the number and type of arguments provided to a function or procedure are consistent with the arguments expected by the function or procedure.
</p>
<p>
These two functions are to be carried out by a recursive descent through the different elements of a syntax tree, with a ProgramNode at the root of the syntax tree being analyzed.
Each element of a given node should be analyzed accordingly, for example the ProgramNode that consititutes the root of the syntax tree should have its subprogram declarations checked along with its compound statement. The declarations are ignored as type here would already be knowne.
Another example would be to check each element of an operation, both its left and right operands, and ensuring that if there are any real values present that the integers are cast to real.
</p>
</div>
<h3>D. Code Generation</h3>
<div class="paragraph-margin" style="margin:20px">
<p>
The code generation package is to be a stand alone class that contains several functions, namely functions that generate the assembly and get the assembly.
The function that generates the assembly is to be also based on a recursive descent across the different nodes of a syntax tree, with a ProgramNode as the root of the syntax tree (this is the output provided by the parser).
Each node should have its structure propperly reflected with a set of assembly instructions. There ought also be two attributes of the class that are added to as the recursion takes place; one string attribute for the data section of the assembly and one string for the text section of the assembly.
</p>
</div>
</div>
<h2>3. Changelog of SDD</h2>
<p><b>170110</b> - Template for the SDD with basic information about the project.
<br><b>170117</b> - Commit to include JavaDoc and JUnit testing, also changed the Scanner's behavior when an invalid lexeme is found (now throws an error instead of printing to stdout).
<br><b>170117</b> - Commit to update the SDD as well as the change logs.
<br><b>170119</b> - Commit to complete the test cases for scanner.Scanner.
<br><b>170204</b> - Commit to implement the recognition portion of the Parser package as well as its JavaDoc and its JUnit testing.
<br><b>170211</b> - Commit to implement the Symbol Table portion of the Parser package as well as its JavaDoc.
<br><b>170218</b> - Commit to implement a Compiler Main function.
<br><b>170308</b> - Commit to add parser.syntaxtree package, include syntaxtree return types in the parser.Parser class. Addition of command line args for tree and symbol table outputs.
<br><b>170501</b> - Various commits including both the semantic analysis package and the code generation package, summing to the final product. These commits were not made all at once, please see git repository for details. For access, email johns116@augsburg.edu.
<br>
</p>
</body>