Software description:
A compiler that takes a file that contains a MiniPascal program and compiles the file to MIPS assembly.
Inputs are taken relative to the current directory, and output is provided to an "Outputs" folder that is generated
in the current directory at runtime. Software dependency is the Java Runtime Environment.

MiniPascal:
The MiniPascal language is a subset of the Pascal programming language, including support for integers, reals, and
arrays of each. It also includes support for functions with return types and procedures without return types.
For more details regarding the language specification see the accompanying pdf, MiniPascal-Grammar.pdf
For anyone who might have prior knowledge of the grammar of this language, please check the grammar file again.

MIPS assembly:
Please refer to any of the wonderful online resources available, or if you really want you may refer to Berkeley's
"Green Sheet" pdf present with this software distribution.

Operation:
The program is to be called from the command line using the java Virtual Machine as its runtime environment.
The correct usage is: 'java -jar JohnsonMatthew-Compiler.jar <options> <filepath>
Options should not be concatenated.\n"
The options are:
-h or /?	help information, details of options and correct argument order\n"
-ta 		produce a copy of the symbol table as output\n"
-tr 		produce a copy of the syntax tree as output\n"
-asm 		produce a copy of MIPs assmebly as output\n");

Bugs:
Please report any bugs to johns116@augsburg.edu